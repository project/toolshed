<?php

namespace Drupal\toolshed\Annotation;

use Drupal\Component\Annotation\Plugin;
use Drupal\Core\Annotation\Translation;

/**
 * Defines the Toolshed third party plugins for entity configurations.
 *
 * Plugin Namespace: Plugin\Toolshed\ThirdPartyConfig.
 *
 * @see \Drupal\Component\Annotation\Plugin
 * @see \Drupal\toolshed\Plugin\ThirdPartyConfigInterface
 * @see \Drupal\toolshed\Plugin\ThirdPartyConfigPluginManager
 *
 * @ingroup toolshed_third_party_config_plugins
 *
 * @Annotation
 */
class ToolshedThirdPartyConfig extends Plugin {

  /**
   * The globally unique plugin ID / machine name.
   *
   * @var string
   */
  public string $id;

  /**
   * The human friendly name for admin configuration forms.
   *
   * @var \Drupal\Core\Annotation\Translation|null
   *
   * @ingroup plugin_translatable
   */
  public ?Translation $label = NULL;

  /**
   * The label to use as the third party settings element.
   *
   * @var string|null
   */
  public ?string $name = NULL;

  /**
   * The weight to apply to the third party settings in the entity form.
   *
   * A default is added to the plugin definition by the plugin manager.
   *
   * @var int
   */
  public int $weight = 0;

  /**
   * An array of the ConfigEntityInterfaces ID that this plugin applies to.
   *
   * @var string[]
   */
  public array $entity_types = [];

}
