<?php

namespace Drupal\toolshed\Strategy;

use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * Factory that injects the service container when creating strategy instances.
 */
class ContainerStrategyFactory implements StrategyFactoryInterface {

  /**
   * The dependency injection container to use with strategy constructors.
   *
   * @var \Symfony\Component\DependencyInjection\ContainerInterface
   */
  protected ContainerInterface $container;

  /**
   * Creates a new instance of the ContainerStrategyFactory class.
   *
   * @param \Symfony\Component\DependencyInjection\ContainerInterface $container.
   */
  public function __construct(?ContainerInterface $container = NULL) {
    if (!isset($container)) {
      @trigger_error(__CLASS__ . ' constructor without passing the dependency container, is deprecated in 2.0.0-beta15 and will be removed by 2.0.0-rc1. Use the "strategy.factory.container" service instead.', E_USER_NOTICE);
      $container = \Drupal::getContainer();
    }
    $this->container = $container;
  }

  /**
   * {@inheritdoc}
   */
  public function create(string $id, StrategyDefinitionInterface $definition): StrategyInterface {
    $class = $definition->getClass();

    if (is_subclass_of($class, ContainerInjectionStrategyInterface::class)) {
      /** @var class-string<\Drupal\toolshed\Strategy\ContainerInjectionStrategyInterface> $class */
      return $class::create($this->container, $id, $definition);
    }

    return new $class($id, $definition);
  }

}
