<?php

namespace Drupal\toolshed\Strategy;

/**
 * The base class for strategy definitions.
 */
class StrategyDefinitionBase implements StrategyDefinitionInterface {

  /**
   * The identifier for this strategy.
   *
   * @var string
   */
  protected string $id;

  /**
   * The label to use for this strategy.
   *
   * @var \Stringable|string
   */
  protected \Stringable|string $label;

  /**
   * The provider of the strategy definition.
   *
   * @var string
   */
  protected string $provider;

  /**
   * The provider type (module or theme).
   *
   * @var string
   */
  protected string $providerType;

  /**
   * The fully namespaced class for the strategy.
   *
   * @var string
   */
  protected string $class;

  /**
   * Creates a new instance of the StrategyDefinition class.
   *
   * @param string $id
   *   The strategy identifier.
   * @param mixed[] $definition
   *   The strategy definition found from the discovery.
   */
  public function __construct(string $id, array $definition) {
    $this->id = $id;
    $this->class = $definition['class'];
    $this->provider = $definition['provider'];
    $this->providerType = $definition['provider_type'] ?? 'module';
    $this->label = $definition['label'] ?? $id;
  }

  /**
   * {@inheritdoc}
   */
  public function id(): string {
    return $this->id;
  }

  /**
   * {@inheritdoc}
   */
  public function getLabel(): \Stringable|string {
    return $this->label;
  }

  /**
   * {@inheritdoc}
   */
  public function getClass(): string {
    return $this->class;
  }

  /**
   * {@inheritdoc}
   */
  public function setClass(string $class): void {
    $this->class = $class;
  }

  /**
   * {@inheritdoc}
   */
  public function getProvider(): string {
    return $this->provider;
  }

  /**
   * {@inheritdoc}
   */
  public function getProviderType(): string {
    return $this->providerType;
  }

}
