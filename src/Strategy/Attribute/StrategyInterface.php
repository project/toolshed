<?php

namespace Drupal\toolshed\Strategy\Attribute;

use Drupal\toolshed\Strategy\StrategyDefinitionInterface;

/**
 * Interface for strategy discovery attributes.
 */
interface StrategyInterface {

  /**
   * Get the strategy identifier.
   *
   * @return string
   *   The strategy indentifier.
   */
  public function id(): string;

  /**
   * Get the strategy deriver class if one is available.
   *
   * @return class-string|null
   *   Get the deriver class to use to enumerate strategy definitions.
   */
  public function getDeriver(): ?string;

  /**
   * Get the strategy class.
   *
   * @return class-string<\Drupal\toolshed\Strategy\StrategyInterface>
   *   The class to use to instantiate instances of this strategy from.
   */
  public function getClass(): string;

  /**
   * Sets the strategy implementation class.
   *
   * @param string $class
   *   Set the class name to use to instantiate this strategy with.
   *
   * @return self
   *   Return this instance for method chaining.
   */
  public function setClass(string $class): self;

  /**
   * Get the provider extension that provides this strategy.
   *
   * @return string
   *   Gets the name of the extension that provides this strategy.
   */
  public function getProvider(): string;

  /**
   * The extension which provides this stratgey.
   *
   * @param string $provider
   *   The name of the provider to set as the provider of this strategy.
   *
   * @return self
   *   Return this instance for method chaining.
   */
  public function setProvider(string $provider): self;

  /**
   * Gets the StrategyDefinition for this discovery attribute.
   *
   * @return array|\Drupal\toolshed\Strategy\StrategyDefinitionInterface
   *   The strategy definition as either an array or the strategy definition
   *   instance variable.
   */
  #[\ReturnTypeWillChange]
  public function get(): array|StrategyDefinitionInterface;

}
