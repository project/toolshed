<?php

namespace Drupal\toolshed\Strategy\Attribute;

use Drupal\toolshed\Strategy\StrategyDefinitionInterface;

/**
 * Base class for implementing strategy discovery attributes.
 */
#[\Attribute(\Attribute::TARGET_CLASS)]
class Strategy implements StrategyInterface {

  /**
   * The strategy implementation class.
   *
   * @var class-string<\Drupal\toolshed\Strategy\StrategyInterface>
   */
  protected string $class;

  /**
   * The strategy provider extension.
   *
   * @var string
   */
  protected string $provider;

  /**
   * Creates a new instance of the Strategy discovery attribute.
   *
   * @param string $id
   *   The strategy identifier.
   * @param class-string|string|null $deriver
   *   The strategy deriver class or service name.
   */
  public function __construct(
    public readonly string $id,
    public readonly ?string $deriver = NULL,
  ) {}

  /**
   * {@inheritdoc}
   */
  public function id(): string {
    return $this->id;
  }

  /**
   * {@inheritdoc}
   */
  public function getDeriver(): ?string {
    return $this->deriver;
  }

  /**
   * {@inheritdoc}
   */
  public function getClass(): string {
    return $this->class;
  }

  /**
   * {@inheritdoc}
   */
  public function setClass(string $class): self {
    $this->class = $class;
    return $this;
  }

  /**
   * {@inheritdoc}
   */
  public function getProvider(): string {
    return $this->provider;
  }

  /**
   * {@inheritdoc}
   */
  public function setProvider(string $provider): self {
    $this->provider = $provider;
    return $this;
  }

  /**
   * {@inheritdoc}
   */
  public function get(): array|StrategyDefinitionInterface {
    return array_filter(get_object_vars($this) + [
      'class' => $this->getClass(),
      'provider' => $this->getProvider(),
    ], function ($value, $key) {
      return !($value === NULL && ($key === 'deriver' || $key === 'provider'));
    }, ARRAY_FILTER_USE_BOTH);
  }

}
