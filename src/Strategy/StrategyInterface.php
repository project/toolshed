<?php

namespace Drupal\toolshed\Strategy;

/**
 * Base interface for strategy implementations.
 */
interface StrategyInterface {

  /**
   * Get the strategy identifier.
   *
   * @return string
   *   The strategy identifier.
   */
  public function id(): string;

  /**
   * Get the strategy provider's machine name.
   *
   * @return string
   *   The machine name of the extension that defined this strategy.
   */
  public function getProvider(): string;

  /**
   * Gets the defining provider's extension type ("module" or "theme").
   *
   * @return string
   *   The type of provider that defines this strategy.
   */
  public function getProviderType(): string;

  /**
   * Gets the strategy definition.
   *
   * @return \Drupal\toolshed\Strategy\StrategyDefinitionInterface
   *   The strategy definition.
   */
  public function getDefinition(): StrategyDefinitionInterface;

}
