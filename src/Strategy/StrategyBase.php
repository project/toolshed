<?php

namespace Drupal\toolshed\Strategy;

/**
 * The base class for Strategy implementations.
 *
 * The base class provides helps to expose the strategy definition properties
 * and helps to ease the implementation of the StrategyInterface.
 */
abstract class StrategyBase implements StrategyInterface {

  /**
   * The Strategy identifier.
   *
   * @var string
   */
  protected string $id;

  /**
   * The definition for this strategy.
   *
   * @var \Drupal\toolshed\Strategy\StrategyDefinitionInterface
   */
  protected StrategyDefinitionInterface $definition;

  /**
   * Creates a new strategy instance.
   *
   * @param string $id
   *   The strategy identifier.
   * @param \Drupal\toolshed\Strategy\StrategyDefinitionInterface $definition
   *   The strategy definition.
   */
  public function __construct(string $id, StrategyDefinitionInterface $definition) {
    $this->id = $id;
    $this->definition = $definition;
  }

  /**
   * {@inheritdoc}
   */
  public function id(): string {
    return $this->id;
  }

  /**
   * {@inheritdoc}
   */
  public function getProvider(): string {
    return $this->definition->getProvider();
  }

  /**
   * {@inheritdoc}
   */
  public function getProviderType(): string {
    return $this->definition->getProviderType();
  }

  /**
   * {@inheritdoc}
   */
  public function getDefinition(): StrategyDefinitionInterface {
    return $this->definition;
  }

}
