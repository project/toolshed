<?php

namespace Drupal\toolshed\Strategy\Exception;

/**
 * Exception for when a strategy definition is complete or invalid.
 */
class InvalidDefinitionException extends StrategyException {
}
