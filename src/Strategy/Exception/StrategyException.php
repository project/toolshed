<?php

namespace Drupal\toolshed\Strategy\Exception;

/**
 * Base class for strategy exceptions.
 */
class StrategyException extends \Exception {
}
