<?php

namespace Drupal\toolshed\Strategy\Exception;

/**
 * Exception for when a requested strategy definition is unavailable.
 *
 * This exception can be thrown also if the strategy is not found for the
 * current context. An example might be, the strategy is not defined for the
 * active theme, even though it might be defined for a different enabled theme.
 */
class StrategyNotFoundException extends StrategyException {

  /**
   * Creates a new instance of the StrategyNotFoundException class.
   *
   * @param string $strategy_id
   *   The strategy identifier.
   * @param int $code
   *   The exception code.
   * @param \Throwable|null $prev
   *   A previous throwable or exception if chaining exceptions.
   */
  public function __construct(string $strategy_id, int $code = 0, ?\Throwable $prev = NULL) {
    $message = \sprintf('The strategy "%s" is not found.', $strategy_id);

    parent::__construct($message, $code, $prev);
  }

}
