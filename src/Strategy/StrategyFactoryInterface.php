<?php

namespace Drupal\toolshed\Strategy;

/**
 * A factory interface for creating strategy instances from definitions.
 */
interface StrategyFactoryInterface {

  /**
   * Creates a new instance of a strategy from the provided definition.
   *
   * @param string $id
   *   The strategy identifier.
   * @param \Drupal\toolshed\Strategy\StrategyDefinitionInterface $definition
   *   The strategy definition to create the instance from.
   *
   * @return \Drupal\toolshed\Strategy\StrategyInterface
   *   A new created instance of the strategy built from the definition.
   */
  public function create(string $id, StrategyDefinitionInterface $definition): StrategyInterface;

}
