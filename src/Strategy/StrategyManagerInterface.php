<?php

namespace Drupal\toolshed\Strategy;

/**
 * The base strategy manager interface.
 *
 * Strategies are similar to the plugin API, but are designed to have a single
 * instances (singletons). This means that strategies do not have per instance
 * configurations, or have non-global states.
 *
 * Strategies are also capable of having theme overriden strategy definitions,
 * which are available when the defining theme is active by using the
 * \Drupal\toolshed\Strategy\ThemeAwareStrategyManager as the strategy manager.
 */
interface StrategyManagerInterface {

  /**
   * The name to use when discovering the strategy.
   *
   * The YAML discovery will search for files in modules and themes with the
   * following format "$provider.$name.yml"
   *
   * @return string
   *   The name for the definition files to load block options from.
   */
  public function getDiscoveryName(): string;

  /**
   * Clears any cached definitions and instances.
   *
   * This clears the defintion and instance caches.
   */
  public function clearCachedDefinitions(): void;

  /**
   * Does the strategy definition exist for the requested context.
   *
   * @param string $id
   *   The strategy identifier to find a definitions for.
   * @param mixed[] $context
   *   A list of contexts to find a matching strategy definitions for.
   *
   * @return bool
   *   If the definition exists for the requested context.
   */
  public function hasDefinition(string $id, array $context = []): bool;

  /**
   * Get a strategy definition matching the requested identifier and context.
   *
   * @param string $id
   *   The strategy identifier to get a matching definition for.
   * @param mixed[] $context
   *   A list of contexts to find a matching strategy for. The context can be
   *   empty, which means to get the definition for the default context.
   *
   * @return \Drupal\toolshed\Strategy\StrategyDefinitionInterface
   *   The matching strategy definition for the provided $context.
   *
   * @throws \Drupal\toolshed\Strategy\Exception\StrategyNotFoundException
   *   Thrown if no matching strategy definition can be found.
   */
  #[\ReturnTypeWillChange]
  public function getDefinition(string $id, array $context = []): StrategyDefinitionInterface;

  /**
   * Get the strategy definitions for the requested $context.
   *
   * @param mixed[] $context
   *   A list of contexts to find a matching strategy definitions for.
   *
   * @return \Drupal\toolshed\Strategy\StrategyDefinitionInterface[]
   *   The strategy definitions for requested context.
   */
  public function getDefinitions(array $context = []): array;

  /**
   * Get a strategy instance matching the request identifier and context.
   *
   * @param string $id
   *   The strategy identifier to get the strategy implementation for.
   * @param mixed[] $context
   *   A list of contexts to find a matching strategy instance for.
   *
   * @return \Drupal\toolshed\Strategy\StrategyInterface
   *   The strategy implemenation matching the request identifier and context.
   */
  #[\ReturnTypeWillChange]
  public function getInstance(string $id, array $context = []): StrategyInterface;

}
