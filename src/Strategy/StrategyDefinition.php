<?php

namespace Drupal\toolshed\Strategy;

/**
 * The default the strategy definition for generic use.
 *
 * In most cases you may want to define a specific definition class when your
 * definition uses additional properties, instead of using the magic PHP method
 * to expose the definition properties.
 */
#[\AllowDynamicProperties]
class StrategyDefinition extends StrategyDefinitionBase implements StrategyDefinitionInterface {

  /**
   * The strategy definition data.
   *
   * @var mixed[]
   */
  protected array $data;

  /**
   * Creates a new instance of the StrategyDefinition class.
   *
   * @param string $id
   *   The strategy identifier.
   * @param mixed[] $definition
   *   The strategy definition found from the discovery.
   */
  public function __construct(string $id, array $definition) {
    parent::__construct($id, $definition);

    $this->data = $definition;
  }

  /**
   * PHP magic method to check if strategy definition data is set.
   *
   * @param string $key
   *   The property name to check if it has been defined.
   *
   * @return bool
   *   TRUE if and only if the data property is properly set.
   */
  public function __isset($key): bool {
    return isset($this->data[$key]);
  }

  /**
   * PHP magic method to get other strategy definition data.
   *
   * @param string $key
   *   The property name to fetch the definition data value for.
   *
   * @return mixed|null
   *   The value matching the $key if there is a property value. NULL if there
   *   is no value for a matching property.
   */
  public function __get(string $key): mixed {
    return $this->data[$key] ?? NULL;
  }

}
