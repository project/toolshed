<?php

namespace Drupal\toolshed\Strategy;

/**
 * The interface required for implementing a strategy definition class.
 */
interface StrategyDefinitionInterface {

  /**
   * Gets the strategy identifier.
   *
   * @return string
   *   The strategy identifier.
   */
  public function id(): string;

  /**
   * Gets the label for the strategy.
   *
   * @return \Stringable|string
   *   The label for this strategy.
   */
  public function getLabel(): \Stringable|string;

  /**
   * Get the fully namespaced class name for the strategy implementation.
   *
   * @return class-string<\Drupal\toolshed\Strategy\StrategyInterface>
   *   The fully namespaced class name for the strategy implementation.
   */
  public function getClass(): string;

  /**
   * Gets the provider type ("module" or "theme").
   *
   * @return string
   *   The provider type of the strategy's provider.
   */
  public function getProviderType(): string;

  /**
   * Get the providing extension's machine name.
   *
   * @return string
   *   The providing extension's machine name.
   */
  public function getProvider(): string;

}
