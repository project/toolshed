<?php

namespace Drupal\toolshed\Strategy;

use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * Interface for dependency injection createable strategy objects.
 *
 * @see \Drupal\toolshed\Strategy\ContainerStrategyFactory
 */
interface ContainerInjectionStrategyInterface extends StrategyInterface {

  /**
   * Creates an new instance of this strategy class.
   *
   * @param \Symfony\Component\DependencyInjection\ContainerInterface $container
   *   The dependency injection service container.
   * @param string $id
   *   The identifier for this strategy.
   * @param \Drupal\toolshed\Strategy\StrategyDefinitionInterface $definition
   *   The strategy definition to create the strategy instance for.
   *
   * @return \Drupal\toolshed\Strategy\StrategyInterface
   *   The constructed strategy object.
   */
  public static function create(ContainerInterface $container, string $id, StrategyDefinitionInterface $definition): StrategyInterface;

}
