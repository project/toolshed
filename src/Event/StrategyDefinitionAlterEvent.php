<?php

namespace Drupal\toolshed\Event;

use Drupal\Component\EventDispatcher\Event;

/**
 * The default event object for modifying strategy definitions.
 *
 * This event can be dispatched for either module or theme base discovery of
 * strategy definitions. The strategy manager is responsible providing the
 * definition alter event name.
 *
 * @see \Drupal\toolshed\Strategy\StrategyManager::findDefinitions()
 * @see \Drupal\toolshed\Strategy\ThemeStrategyDiscoveryTrait::findThemeDefinitions()
 */
class StrategyDefinitionAlterEvent extends Event {

  /**
   * The provider type these definitions are for.
   *
   * This value should be either "module" or "theme" depending on if these
   * definitions were provided by modules or themes.
   *
   * @var string
   */
  protected string $providerType;

  /**
   * Array of strategy definitions (array format), grouped by provider.
   *
   * @var mixed[]
   */
  protected array $definitions;

  /**
   * Creates a new instance of the StrategyDefinitionAlterEvent class.
   *
   * @param string $provider_type
   *   The provider type of these definitions ("module" or "theme").
   * @param array $definitions_by_provider
   *   A reference to the strategy definitions to alter.
   */
  public function __construct(string $provider_type, array &$definitions_by_provider) {
    $this->providerType = $provider_type;
    $this->definitions = &$definitions_by_provider;
  }

  /**
   * Get the provider type for these definitions.
   *
   * @return string
   *   Provider type for the definitions being altered ("module" or "theme").
   */
  public function getProviderType(): string {
    return $this->providerType;
  }

  /**
   * Get all strategy definitions being altered.
   *
   * The definitions are in array format and are grouped by provider.
   *
   * @return mixed[]
   *   The array of all definitions (array format), grouped by the provider for
   *   the strategy definitions being altered.
   */
  public function &getDefinitions(): array {
    return $this->definitions;
  }

  /**
   * Get all definitions provided by a provider (module or theme).
   *
   * @param string $provider_name
   *   The machine name of the provider to fetch the definitions for.
   *
   * @return mixed[]
   *   Gets definitions for the requested provided, keyed by their strategy ID.
   *
   * @throws \InvalidArgumentException
   *   Thrown when provider does not have any strategy definitions or the
   *   provider does not exist.
   */
  public function &getProviderDefinitions(string $provider_name): array {
    if (!isset($this->definitions[$provider_name])) {
      $err = sprintf('No strategy definitions are available for provider name: "%s"', $provider_name);
      throw new \InvalidArgumentException($err);
    }

    return $this->definitions[$provider_name];
  }

}
