<?php

namespace Drupal\toolshed;

use Drupal\Core\DependencyInjection\ContainerBuilder;
use Drupal\Core\DependencyInjection\ServiceProviderInterface;
use Drupal\toolshed\DependencyInjection\Compiler\StrategyManagerCompilerPass;

/**
 * Service provider class for altering the dependency injection container.
 *
 * This service provider registers a compiler pass to add the strategy managers
 * to the cached strategy definition purger.
 *
 * This ensures that definitions are refreshed when a full cache rebuild is
 * requested.
 *
 * @see \Drupal\toolshed\Compiler\StrategyManagerCompilerPass::process()
 */
class ToolshedServiceProvider implements ServiceProviderInterface {

  /**
   * {@inheritdoc}
   */
  public function register(ContainerBuilder $container): void {
    $container->addCompilerPass(new StrategyManagerCompilerPass());
  }

}
