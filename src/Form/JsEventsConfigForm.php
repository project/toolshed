<?php

namespace Drupal\toolshed\Form;

use Drupal\Core\Asset\LibraryDiscoveryInterface;
use Drupal\Core\Cache\CacheCollectorInterface;
use Drupal\Core\DependencyInjection\ContainerInjectionInterface;
use Drupal\Core\Form\ConfigFormBase;
use Drupal\Core\Form\FormStateInterface;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * Create configuration form for Toolshed JS events.
 */
class JsEventsConfigForm extends ConfigFormBase implements ContainerInjectionInterface {

  /**
   * The Drupal service that manages resolves references to libraries.
   *
   * @var \Drupal\Core\Asset\LibraryDiscoveryInterface
   */
  protected LibraryDiscoveryInterface $libraryCollector;

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container): static {
    $instance = parent::create($container);
    $instance->setLibraryDiscovery($container->get('library.discovery'));

    return $instance;
  }

  /**
   * Set the library discovery service for cache clearing.
   *
   * @param \Drupal\Core\Asset\LibraryDiscoveryInterface&\Drupal\Core\Cache\CacheCollectorInterface $library_collector
   *   Set the library discovery service for the config form.
   *
   * @return self
   *   Return the instance for method chaining.
   */
  public function setLibraryDiscovery(LibraryDiscoveryInterface $library_collector): self {
    $this->libraryCollector = $library_collector;
    return $this;
  }

  /**
   * {@inheritdoc}
   */
  public function getFormId(): string {
    return 'toolshed_js_events_config';
  }

  /**
   * {@inheritdoc}
   */
  public function getEditableConfigNames(): array {
    return ['toolshed.assets.config'];
  }

  /**
   * {@inheritdoc}
   */
  public function buildForm(array $form, FormStateInterface $form_state): array {
    $config = $this->configFactory->get('toolshed.assets.config');

    $form['dev_assets'] = [
      '#type' => 'checkbox',
      '#title' => $this->t('Use development version of assets.'),
      '#default_value' => $config->get('dev_assets') ?? FALSE,
    ];

    // Configuration for debounce with JavaScript events (e.g. resize, scroll).
    $debounce = $config->get('debounce');
    $form['debounce'] = [
      '#type' => 'fieldset',
      '#title' => $this->t('Debounce settings'),
      '#tree' => TRUE,
      '#description' => $this->t(
        'Debounce is a method to throttle actions that maybe expensive by
        adding a delay before triggering them. Expensive events like
        reacting to window resizes can be deferred until the time after
        the last resize event exceeds the specified timeout delay.'
      ),

      'enabled' => [
        '#type' => 'checkbox',
        '#title' => $this->t('Debouce enabled'),
        '#default_value' => $debounce['enabled'] ?? TRUE,
      ],
      'delay' => [
        '#type' => 'number',
        '#title' => $this->t('Delay in milliseconds'),
        '#field_suffix' => 'ms',
        '#size' => 10,
        '#min' => 0,
        '#default_value' => $debounce['delay'] ?? 200,
      ],
    ];

    $accordConfig = $config->get('accordions');
    $form['accordions'] = [
      '#type' => 'details',
      '#title' => $this->t('Accordions Behavior Settings'),
      '#tree' => TRUE,
      '#collapsible' => TRUE,
      '#description' => $this->t(
        'These options are for working with the <em>toolshed/behavior.accordions</em>
        library and allow your site to specify the selectors to use when activating
        accordions behaviors on HTML elements.'
      ),

      'itemSelector' => [
        '#type' => 'textfield',
        '#title' => $this->t('Selector for accordion items'),
        '#default_value' => $accordConfig['itemSelector'] ?? NULL,
        '#description' => $this->t('The CSS selector to use when searching for accordion items.'),
      ],
      'toggleSelector' => [
        '#type' => 'textfield',
        '#title' => $this->t('Selector for the toggle control'),
        '#default_value' => $accordConfig['toggleSelector'] ?? NULL,
        '#description' => $this->t('The CSS selector to identify the control when clicked which expands and collapses the accordion.'),
      ],
      'bodySelector' => [
        '#type' => 'textfield',
        '#title' => $this->t('Selector for collapsible portion of the item'),
        '#default_value' => $accordConfig['bodySelector'] ?? NULL,
        '#description' => $this->t('The portion of the accordion item that collapses.'),
      ],
    ];

    $form['actions'] = [
      '#type' => 'actions',

      'submit' => [
        '#type' => 'submit',
        '#value' => $this->t('Save'),
      ],
      'cancel' => [
        '#type' => 'link',
        '#url' => NULL,
        '#attributes' => [
          'class' => ['button', 'button--cancel'],
        ],
      ],
    ];

    return $form;
  }

  /**
   * {@inheritdoc}
   */
  public function submitForm(array &$form, FormStateInterface $form_state): void {
    $values = $form_state->getValues();
    $config = $this->config('toolshed.assets.config');

    $config->set('dev_assets', $values['dev_assets']);

    $config->set('debounce', [
      'enabled' => $values['debounce']['enabled'],
      'delay' => $values['debounce']['delay'],
    ]);

    $config->set('accordions', [
      'itemSelector' => $values['accordions']['itemSelector'],
      'toggleSelector' => $values['accordions']['toggleSelector'],
      'bodySelector' => $values['accordions']['bodySelector'],
    ]);

    // Save the changes to the configuration.
    $config->save();

    // Clear cache so that the updated settings are applied.
    if ($this->libraryCollector instanceof CacheCollectorInterface) {
      $this->libraryCollector->clear();
    }
    elseif (method_exists($this->libraryCollector, 'clearCachedDefinition')) {
      // This allows us to keep Drupal 10.x support, but can be removed when
      // Drupal 11 or Drupal 12 are the minimum supported version.
      $this->libraryCollector->clearCachedDefinitions();
    }
  }

}
