<?php

namespace Drupal\toolshed\Attribute;

use Drupal\Component\Plugin\Attribute\Plugin;

/**
 * Attribute for the third-party entity configuration plugins.
 */
#[\Attribute(\Attribute::TARGET_CLASS)]
class ToolshedThirdPartyConfig extends Plugin {

  /**
   * Creates a new instance of the ToolshedThirdPartyConfig plugin attribute.
   *
   * @param string $id
   *   The globally unique plugin ID / machine name.
   * @param \Stringable|string|null $label
   *   The human friendly name for admin configuration forms.
   * @param string|null $name
   *   The label to use as the third party settings element.
   * @param int $weight
   *   The weight to apply to the third party settings in the entity form.
   * @param string[] $entity_types
   *   An array of the ConfigEntityInterfaces ID that this plugin applies to.
   */
  public function __construct(
    public readonly string $id,
    public readonly \Stringable|string|null $label = NULL,
    public readonly ?string $name = NULL,
    public readonly int $weight = 0,
    public readonly array $entity_types = [],
  ) {}

}
