<?php

namespace Drupal\toolshed\DependencyInjection\Compiler;

use Drupal\toolshed\Strategy\StrategyManagerInterface;
use Symfony\Component\DependencyInjection\Compiler\CompilerPassInterface;
use Symfony\Component\DependencyInjection\ContainerBuilder;
use Symfony\Component\DependencyInjection\Reference;

/**
 * The service container compiler pass to setup the definition cache clearing.
 */
class StrategyManagerCompilerPass implements CompilerPassInterface {

  /**
   * {@inheritdoc}
   */
  public function process(ContainerBuilder $container): void {
    $cachePurger = $container->getDefinition('strategy.cache_clearer');

    foreach ($container->getDefinitions() as $serviceId => $definition) {
      if (strpos($serviceId, 'strategy.manager.') !== 0) {
        continue;
      }

      if (is_subclass_of($definition->getClass(), StrategyManagerInterface::class)) {
        $cachePurger->addMethodCall('addStrategyManager', [new Reference($serviceId)]);
      }
    }
  }

}
