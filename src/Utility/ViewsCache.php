<?php

namespace Drupal\toolshed\Utility;

use Drupal\Core\Cache\CacheableMetadata;

/**
 * Helper to alter the cache metadata of views output, rows, styles or displays.
 */
class ViewsCache {

  /**
   * Disable caching for each row in the views results.
   *
   * Depending on the view, sometimes the actual row items are nested and
   * need to be searched for recursively. This function searches for rows
   * and applies a cache max-age to items.
   *
   * @param array $rows
   *   Views rows array to search for renderable rows.
   */
  public static function disableRowCache(array &$rows): void {
    if (empty($rows)) {
      return;
    }

    if (!empty($rows['#rows'])) {
      static::disableRowCache($rows['#rows']);
    }
    elseif (!empty($rows['#row'])) {
      $cacheMeta = CacheableMetadata::createFromRenderArray($rows);
      $cacheMeta->mergeCacheMaxAge(0);
      $cacheMeta->applyTo($rows);
    }
    else {
      foreach ($rows as $key => &$row) {
        if (is_int($key)) {
          static::disableRowCache($row);
        }
      }
    }
  }

}
