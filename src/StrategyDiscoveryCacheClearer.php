<?php

namespace Drupal\toolshed;

use Drupal\toolshed\Strategy\StrategyManagerInterface;

/**
 * The strategy discovery cache clearer.
 */
class StrategyDiscoveryCacheClearer {

  /**
   * The strategy managers to flush caches for.
   *
   * @var \Drupal\toolshed\Strategy\StrategyManagerInterface[]
   */
  protected array $strategyManagers = [];

  /**
   * Adds a strategy manager to have the discovery cache cleared.
   *
   * @param \Drupal\toolshed\Strategy\StrategyManagerInterface $strategy_manager
   *   The strategy manager to add for definition cache clears.
   */
  public function addStrategyManager(StrategyManagerInterface $strategy_manager): void {
    $this->strategyManagers[] = $strategy_manager;
  }

  /**
   * Clears all the registered cache for discovered strategy definitions.
   */
  public function clearCachedDefinitions(): void {
    foreach ($this->strategyManagers as $manager) {
      $manager->clearCachedDefinitions();
    }
  }

}
