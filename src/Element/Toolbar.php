<?php

namespace Drupal\toolshed\Element;

use Drupal\Core\Render\Element;
use Drupal\Core\Render\Element\RenderElementBase;

/**
 * Create a render element which houses toolbar buttons and panes.
 *
 * Generates a toolbar with docking options, and contains buttons. Controls the
 * layout for the buttons and panels it contains.
 *
 * @RenderElement("toolshed_toolbar")
 */
class Toolbar extends RenderElementBase {

  /**
   * {@inheritdoc}
   */
  public function getInfo(): array {
    return [
      '#pre_render' => [static::class . '::preRenderToolbar'],
    ];
  }

  /**
   * Format and prepare the toolbar for rendering.
   *
   * @param array $element
   *   Renderable toolbar panes and button information.
   *
   * @return array
   *   Processed and renderable elements of the toolbar. The toolbar buttons
   *   and panes organized together.
   */
  public static function preRenderToolbar(array $element): array {
    $element['#attached']['library'][] = 'toolshed/Toolbar';

    foreach (Element::children($element['panes']) as $child) {
      $pane = &$element['panes'][$child];
      $pane += [
        '#type' => 'details',
        '#title' => 'selection',
        '#collapsible' => TRUE,
        '#collapsed' => TRUE,
      ];

      if (empty($pane['#attributes']['class'])) {
        $pane['#attributes']['class'] = [];
      }

      $pane['#attributes']['class'][] = 'toolbar-pane';
    }

    return $element;
  }

}
