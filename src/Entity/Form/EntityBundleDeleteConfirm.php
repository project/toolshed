<?php

namespace Drupal\toolshed\Entity\Form;

use Drupal\Core\Form\FormStateInterface;
use Drupal\toolshed\Entity\EntityBundleBase;

/**
 * Delete confirmation for removing an entity bundle.
 */
class EntityBundleDeleteConfirm extends EntityDeleteConfirm {

  /**
   * {@inheritdoc}
   */
  public function buildForm(array $form, FormStateInterface $form_state): array {
    $entity = $this->entity;

    if ($entity instanceof EntityBundleBase && $entity->hasData()) {
      $form['#title'] = $this->getQuestion();
      $form['description'] = [
        '#markup' => $this->t('The @entity_type %bundle has content. You cannot remove this @entity_type until all the content using it is removed.', [
          '%bundle' => $entity->label() ?? '',
          '@entity_type' => $entity->getEntityType()->getLabel() ?? '',
        ]),
      ];
    }
    else {
      $form = parent::buildForm($form, $form_state);
    }

    return $form;
  }

}
