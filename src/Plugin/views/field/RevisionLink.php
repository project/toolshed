<?php

namespace Drupal\toolshed\Plugin\views\field;

use Drupal\views\Plugin\views\field\EntityLink;

/**
 * Field handler to present a link to a specific entity revision.
 *
 * Should only be added to entities that support revisions and have a link
 * template defined for <em>revision</em>.
 *
 * @ingroup views_field_handlers
 *
 * @ViewsField("toolshed_revision_link")
 */
class RevisionLink extends EntityLink {

  /**
   * {@inheritdoc}
   */
  protected function getEntityLinkTemplate(): string {
    return 'revision';
  }

  /**
   * {@inheritdoc}
   */
  protected function getDefaultLabel(): \Stringable|string {
    return $this->t('view revision');
  }

}
