<?php

namespace Drupal\toolshed\Plugin\views\field;

use Drupal\views\Plugin\views\field\EntityLink;
use Drupal\views\ResultRow;

/**
 * Field handler to present a link to delete an entity revision.
 *
 * Should only be added to entities that support revisions and have a link
 * template defined for <em>revision-delete</em>.
 *
 * @ingroup views_field_handlers
 *
 * @ViewsField("toolshed_revision_delete_link")
 */
class RevisionDeleteLink extends EntityLink {

  /**
   * {@inheritdoc}
   */
  protected function getEntityLinkTemplate(): string {
    return 'revision-delete';
  }

  /**
   * {@inheritdoc}
   */
  protected function renderLink(ResultRow $row): string {
    $this->options['alter']['query'] = $this->getDestinationArray();
    return parent::renderLink($row);
  }

  /**
   * {@inheritdoc}
   */
  protected function getDefaultLabel(): \Stringable|string {
    return $this->t('delete revision');
  }

}
