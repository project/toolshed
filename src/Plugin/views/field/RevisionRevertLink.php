<?php

namespace Drupal\toolshed\Plugin\views\field;

use Drupal\views\Plugin\views\field\EntityLink;
use Drupal\views\ResultRow;

/**
 * Field handler to present a link to revert an entity revision.
 *
 * Should only be added to entities that support revisions and have a link
 * template defined for <em>revision-revert</em>.
 *
 * @ingroup views_field_handlers
 *
 * @ViewsField("toolshed_revision_revert_link")
 */
class RevisionRevertLink extends EntityLink {

  /**
   * {@inheritdoc}
   */
  protected function getEntityLinkTemplate(): string {
    return 'revision-revert';
  }

  /**
   * {@inheritdoc}
   */
  protected function renderLink(ResultRow $row): string {
    $this->options['alter']['query'] = $this->getDestinationArray();
    return parent::renderLink($row);
  }

  /**
   * {@inheritdoc}
   */
  protected function getDefaultLabel(): \Stringable|string {
    return $this->t('revert revision');
  }

}
