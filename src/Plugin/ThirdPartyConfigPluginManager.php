<?php

namespace Drupal\toolshed\Plugin;

use Drupal\Core\Cache\CacheBackendInterface;
use Drupal\Core\Extension\ModuleHandlerInterface;
use Drupal\Core\Plugin\DefaultPluginManager;

/**
 * Default plugin manager for managing config entity ThirdPartyConfig plugins.
 *
 * @see \Drupal\toolshed\Annotation\ThirdPartyConfig
 * @see \Drupal\toolshed\Plugin\ThirdPartyConfigInterface
 * @see plugin_api
 */
class ThirdPartyConfigPluginManager extends DefaultPluginManager {

  /**
   * Constructs a ThirdPartyConfigPluginManager instance.
   *
   * @param \Traversable $namespaces
   *   An object that implements \Traversable which contains the root paths
   *   keyed by the corresponding namespace to look for plugin implementations.
   * @param \Drupal\Core\Cache\CacheBackendInterface $cache_backend
   *   Cache backend instance to use.
   * @param \Drupal\Core\Extension\ModuleHandlerInterface $module_handler
   *   The module handler to invoke the alter hook with.
   */
  public function __construct(\Traversable $namespaces, CacheBackendInterface $cache_backend, ModuleHandlerInterface $module_handler) {
    parent::__construct(
      'Plugin/Toolshed/ThirdPartyConfig',
      $namespaces,
      $module_handler,
      'Drupal\toolshed\Plugin\ThirdPartyConfigInterface',
      'Drupal\toolshed\Attribute\ToolshedThirdPartyConfig',
      'Drupal\toolshed\Annotation\ToolshedThirdPartyConfig'
    );

    $this->alterInfo('toolshed_third_party_config');
    $this->setCacheBackend($cache_backend, 'toolshed_third_party_config');
  }

  /**
   * Get an array of plugin definitions that apply to the passed in $entity.
   *
   * @param string $entity_type
   *   Machine name of the entity type to fetch support third party
   *   configurations for.
   *
   * @return \Drupal\toolshed\Plugin\ThirdPartyConfigInterface[]
   *   An array of plugin definitions keyed by the plugin ID.
   */
  public function getPluginsByEntityType($entity_type): array {
    $plugins = [];

    // @todo Implement caching of the plugin ID to entity type mapping.
    foreach ($this->getDefinitions() as $pluginDef) {
      if (in_array($entity_type, $pluginDef['entity_types'])) {
        $plugins[$pluginDef['id']] = $this->createInstance($pluginDef['id']);
      }
    }

    return $plugins;
  }

}
