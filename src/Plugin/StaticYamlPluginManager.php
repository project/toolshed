<?php

namespace Drupal\toolshed\Plugin;

@trigger_error('\Drupal\toolshed\Plugin\StaticYamlPluginManager is deprecated in toolshed:2.0.0-alpha1 and is removed from toolshed:2.0.0-rc1. Instead implement the \Drupal\toolshed\StrategyManagerInterface. See https://www.drupal.org/project/iconset/issues/1', E_USER_DEPRECATED);

use Drupal\Component\Plugin\Exception\PluginException;
use Drupal\Component\Plugin\PluginInspectionInterface;

/**
 * Create a plugin manager that maintains single instances of plugins.
 *
 * Most YAML discovered plugins are built from the exact same configuration
 * and definitions. These when these plugins are usually providing a service or
 * definition which are all meant to be single instances, this plugin
 * manager can be utilized to always retrieve and return the single instance.
 *
 * These definitions should have no external configurations, and only require
 * the $plugin_definition (YAML discovery) to get created.
 *
 * @deprecated in toolshed:2.0.0-alpha1 and is removed from toolshed:2.0.0-rc1.
 * Instead use a strategy manager which implements the
 * \Drupal\toolshed\Strategy\StrategyManagerInterface.
 *
 * @see https://www.drupal.org/project/iconset/issues/1
 */
class StaticYamlPluginManager extends YamlPluginManager {

  /**
   * All loaded instances based on plugin ID.
   *
   * @var \Drupal\Component\Plugin\PluginInspectionInterface[]
   */
  protected static array $instances = [];

  /**
   * Initialize new instances of the plugin.
   *
   * Static plugin instances may have some special initializations that only
   * need to be applied during creation. This allows subclassed plugin managers
   * to implement initiation using manager services.
   *
   * @param \Drupal\Component\Plugin\PluginInspectionInterface $plugin
   *   The newly created plugin instance to initialize.
   */
  protected function initInstance(PluginInspectionInterface $plugin): void {
    // By default there are no additional initialization steps needed.
  }

  /**
   * {@inheritdoc}
   */
  public function createInstance($plugin_id, array $configuration = []): object {
    if (!isset(static::$instances[$plugin_id])) {
      $plugin = parent::createInstance($plugin_id);
      $this->initInstance($plugin);
      static::$instances[$plugin_id] = $plugin;
    }

    return static::$instances[$plugin_id];
  }

  /**
   * {@inheritdoc}
   */
  public function getInstance(array $options): object|false {
    if (!empty($options['id'])) {
      try {
        return $this->createInstance($options['id']);
      }
      catch (PluginException $e) {
        return FALSE;
      }
    }

    return FALSE;
  }

}
