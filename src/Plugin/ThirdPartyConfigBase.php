<?php

namespace Drupal\toolshed\Plugin;

use Drupal\Core\Config\Entity\ConfigEntityInterface;
use Drupal\Core\Entity\EntityInterface;
use Drupal\Core\Form\FormStateInterface;
use Drupal\Core\Plugin\PluginBase;

/**
 * Base implementation for a ThirdPartyConfig plugin.
 */
abstract class ThirdPartyConfigBase extends PluginBase implements ThirdPartyConfigInterface {

  /**
   * {@inheritdoc}
   */
  public function isApplicable(ConfigEntityInterface $entity, $op): bool {
    return !in_array($op, ['delete', 'delete_multiple_confirm']);
  }

  /**
   * Get the machine name of the third party settings provider (module name).
   *
   * @return string
   *   The machine name of the third party settings provider.
   */
  public function getProvider(): string {
    return $this->pluginDefinition['provider'];
  }

  /**
   * Get the label to use for these third party settings.
   *
   * @return \Stringable|string
   *   Label to use as the third party settings form.
   */
  public function getLabel(): \Stringable|string {
    return $this->pluginDefinition['label'];
  }

  /**
   * Get the default third party settings values.
   *
   * @return array
   *   The default values to use for the third party settings.
   */
  public function getDefaultSettings(): array {
    return [];
  }

  /**
   * Create the settings form elements for setting the third party settings.
   *
   * @param \Drupal\Core\Config\Entity\ConfigEntityInterface $entity
   *   The config entity for which to generate the third party setting for.
   * @param array $settings
   *   The current settings values.
   * @param \Drupal\Core\Form\FormStateInterface $form_state
   *   The complete form state.
   *
   * @return array
   *   The form elements for configuring the third party settings.
   */
  abstract protected function formElements(ConfigEntityInterface $entity, array $settings, FormStateInterface $form_state): array;

  /**
   * {@inheritdoc}
   */
  public function addThirdPartyConfig(ConfigEntityInterface $entity, array &$form, FormStateInterface $form_state): void {
    $provider = $this->getProvider();
    $settings = $entity->getThirdPartySettings($provider) ?? [];
    $settings += $this->getDefaultSettings();

    $form['third_party'][$provider] = $this->formElements($entity, $settings, $form_state);
    $form['third_party'][$provider] += [
      '#type' => 'details',
      '#title' => $this->getLabel(),
      '#group' => 'additional_settings',
      '#tree' => TRUE,
      '#weight' => 20,
    ];

    $form['#entity_builders'][] = [$this, 'buildEntity'];
  }

  /**
   * Allows form values to be processed and reorganized for saving.
   *
   * Allows third party settings subclasses to process values before they are
   * saved. This would most commonly be used when the values directly from the
   * form elements need to be processed, translated or cleaned.
   *
   * @param \Drupal\Core\Config\Entity\ConfigEntityInterface $entity
   *   The entity being formatted.
   * @param array $values
   *   The raw values to process.
   *
   * @return array
   *   The values rearrange and processed for saving to the config entity.
   */
  protected function massageValues(ConfigEntityInterface $entity, array $values): array {
    return $values;
  }

  /**
   * Entity builder callback to format and apply the settings updates.
   *
   * @param string $entity_type_id
   *   The entity type ID of the entity being updated.
   * @param \Drupal\Core\Entity\EntityInterface $entity
   *   The entity being updated.
   * @param array $form
   *   The entity form elements.
   * @param \Drupal\Core\Form\FormStateInterface $form_state
   *   The complete entity form state.
   */
  public function buildEntity($entity_type_id, EntityInterface $entity, array $form, FormStateInterface $form_state): void {
    if ($entity instanceof ConfigEntityInterface) {
      $provider = $this->getProvider();

      $element = &$form['third_party'][$provider];
      $values = $form_state->getValue($element['#parents']);
      $values = $this->massageValues($entity, $values);

      // Drupal\Core\Config\Entity\ThirdPartySettingsInterface does not have a
      // method to set all the settings at once. Instead we need to clear out
      // any old settings, and apply the new values one at a time.
      $current = $entity->getThirdPartySettings($provider);
      foreach (array_diff_key($current, $values) as $key => $value) {
        $entity->unsetThirdPartySetting($provider, $key);
      }

      // Apply the third party settings values back to the config entity.
      foreach ($values as $key => $value) {
        $entity->setThirdPartySetting($provider, $key, $value);
      }
    }
  }

}
