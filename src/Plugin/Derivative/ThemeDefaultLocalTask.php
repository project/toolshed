<?php

namespace Drupal\toolshed\Plugin\Derivative;

use Drupal\system\Plugin\Derivative\ThemeLocalTask;

/**
 * Theme local task deriver but sets the default theme the default task.
 *
 * Essentially the same as the system ThemeLocalTask deriver, except that
 * doesn't set the default theme as the default local task because it has
 * a global settings form it uses as the default.
 */
class ThemeDefaultLocalTask extends ThemeLocalTask {

  /**
   * {@inheritdoc}
   */
  public function getDerivativeDefinitions($base_plugin_definition): array {
    parent::getDerivativeDefinitions($base_plugin_definition);

    $defaultTheme = $this->themeHandler->getDefault();
    if (isset($this->derivatives[$defaultTheme])) {
      $defaultTask = &$this->derivatives[$defaultTheme];
      $defaultTask['route_name'] = $defaultTask['parent_id'];
      $defaultTask['weight'] = -100;
      unset($defaultTask['route_parameters']);
    }

    return $this->derivatives;
  }

}
