<?php

namespace Drupal\toolshed\Plugin;

use Drupal\Component\Plugin\PluginInspectionInterface;
use Drupal\Core\Config\Entity\ConfigEntityInterface;
use Drupal\Core\Form\FormStateInterface;

/**
 * Interface for ThirdPartyConfig plugins.
 *
 * Third party configuration plugins are for Config Entities, which all
 * implement the \Drupal\Core\Config\Entity\ThirdPartySettingsInterface. The
 * plugins are discovered from the ThirdPartyConfig annotation, which has an
 * "entity_types" property, that defines the Config Entity types that this
 * plugin works with.
 *
 * The plugins provide a consistent way to provide configuration settings form
 * elements for third party settings in entity forms.
 *
 * @see \Drupal\toolshed\Annotation\ThirdPartyConfig
 * @see plugin_api
 */
interface ThirdPartyConfigInterface extends PluginInspectionInterface {

  /**
   * Determine if a specific instance of a ConfigEntity applies to this plugin.
   *
   * The isApplicable() method allows for finer grained settings that may or
   * may not be relevant to a configuration entity type.
   *
   * @param \Drupal\Core\Config\Entity\ConfigEntityInterface $entity
   *   The configuration entity to test if settings plugin settings apply.
   * @param string $op
   *   The name of the operation being performed on the entity.
   *
   * @return bool
   *   Returns TRUE if this third party form settings apply to the provided
   *   $entity parameter. FALSE if the settings do not.
   */
  public function isApplicable(ConfigEntityInterface $entity, $op): bool;

  /**
   * Get the default third party settings configured by this plugin.
   *
   * @return array
   *   Default values to apply to the third party settings from this plugin.
   */
  public function getDefaultSettings(): array;

  /**
   * Add the third party settings to an entity form.
   *
   * @param \Drupal\Core\Config\Entity\ConfigEntityInterface $entity
   *   The entity being formatted.
   * @param array $form
   *   Reference to the form structure.
   * @param \Drupal\Core\Form\FormStateInterface $form_state
   *   The complete form state.
   */
  public function addThirdPartyConfig(ConfigEntityInterface $entity, array &$form, FormStateInterface $form_state): void;

}
