"use strict";

(({
  Toolshed: ts
}) => {
  /**
   * A Pane is floating DIV with manageable content.
   *
   * It is designed to be the base of dialogs, pop-ups or other elements
   * that attach to other content.
   */
  class Pane extends ts.Element {
    static defaultOptions() {
      return {
        classes: []
      };
    }

    /**
     * Create a new instance of the a display pane.
     *
     * @param {Object} options
     *   The configuration options for how to handle pane behaviors.
     */
    constructor(options = {}) {
      super('div', null, document.body);
      this.opts = {
        ...Pane.defaultOptions(),
        ...options
      };
      this.content = new ts.Element('div', {
        class: 'pane__content'
      }, this);
      this.addClass(this.opts.classes.concat('ts-pane', 'pane'));
      this.setStyles({
        display: 'none',
        position: 'absolute',
        zIndex: this.opts.zIndex
      });
      this.append = !this.opts.onAddItem ? this.appendItem : item => {
        const el = this.opts.onAddItem(item);
        this.appendItem(el || item);
      };
      this.remove = !this.opts.onRemoveItem ? this.removeItem : item => {
        const el = this.opts.onRemoveItem(item);
        this.removeItem(el || item);
      };
    }

    /**
     * Adds and element to the start of the content container.
     *
     * @param {Node} item
     *   Item to add to the start of the pane content.
     */
    prependItem(item) {
      this.content.prependChild(item);
      return this;
    }

    /**
     * Add an element to the content container.
     *
     * @param {Node} item
     *   The node to add to the content container.
     */
    appendItem(item) {
      this.content.appendChild(item);
      return this;
    }

    /**
     * Remove an element from the content container.
     *
     * @param {Node} item
     *   The node to remove from the content container.
     */
    removeItem(item) {
      this.content.removeChild(item);
      return this;
    }

    /**
     * Clear the content container of all nodes and elements.
     */
    clear() {
      this.content.empty();
      return this;
    }

    /**
     * Is the suggestion window open?
     *
     * @return {Boolean}
     *   TRUE if the suggestions window is open, otherwise FALSE.
     */
    isVisible() {
      return this.style.display !== 'none';
    }

    /**
     * Position the pane before displaying it.
     */
    // eslint-disable-next-line class-methods-use-this
    positionPane() {
      // TODO: position based on pane options.
    }

    /**
     * Position and expose the suggestions window if it is not already open.
     *
     * @return {bool}
     *   Always returns true, to indicate that the dialog is open.
     */
    open() {
      if (!this.isVisible()) {
        this.style.display = '';
        this.positionPane();
      }
      return true;
    }

    /**
     * Close the content pane.
     *
     * @return {bool}
     *   Always returns false, to indicate that the pane is closed.
     */
    close() {
      this.style.display = 'none';
      return false;
    }

    /**
     * Toggle to open and close status of the pane.
     *
     * @return {bool}
     *   Returns true if result is pane is open, and false if it is closed.
     */
    toggle() {
      return this.isVisible() ? this.close() : this.open();
    }

    /**
     * Teardown and clean-up of Pane contents.
     */
    destroy() {
      this.clear();
      super.destroy(true);
    }
  }

  /**
   * A pane, that attaches itself to another element in the document.
   *
   * AttachedPanes are a good base for pop-ups, toolbars or other elements
   * that display content and anchor themselves to other screen elements.
   */
  class AttachedPane extends Pane {
    /**
     * Create a new instance of an AttachedPane.
     *
     * @param {DOMElement} attachTo
     *   The element containing the pane content.
     * @param {Object} options
     *   The configuration options for how to handle pane behaviors.
     */
    constructor(attachTo, options = {}) {
      super(options);
      this.attachTo = attachTo;
    }

    /**
     * @override
     */
    positionPane() {
      const rect = this.attachTo.getBoundingClientRect();
      const scroll = {
        x: document.documentElement.scrollLeft || document.body.scrollLeft,
        y: document.documentElement.scrollTop || document.body.scrollTop
      };
      this.style.top = rect.top > this.el.clientHeight ? `${scroll.y - this.el.clientHeight + rect.bottom}px` : `${scroll.y + rect.top}px`;
      this.style.left = `${scroll.x + rect.right}px`;

      // Calculate the z-index of the attached button, this becomes important,
      // when dealing with form elements in pop-up dialogs. Only calculate this
      // if not already explicitly set by the options.
      if (!this.opts.zIndex && !this.style.zIndex) {
        let cur = this.attachTo;
        let zIndex = 10;
        while (cur) {
          const z = (cur.style || {}).zIndex || window.getComputedStyle(cur).getPropertyValue('z-index');
          if (Number.isInteger(z) || /\d+/.test(z)) {
            const tz = (ts.isString(z) ? parseInt(z, 10) : z) + 10;
            if (tz > zIndex) zIndex = tz;
          }
          cur = cur.parentElement;
        }
        this.style.zIndex = zIndex;
      }
    }
  }
  ts.Pane = Pane;
  ts.AttachedPane = AttachedPane;
})(Drupal);
//# sourceMappingURL=data:application/json;charset=utf8;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoid2lkZ2V0cy9QYW5lLmpzIiwibmFtZXMiOlsiVG9vbHNoZWQiLCJ0cyIsIlBhbmUiLCJFbGVtZW50IiwiZGVmYXVsdE9wdGlvbnMiLCJjbGFzc2VzIiwiY29uc3RydWN0b3IiLCJvcHRpb25zIiwiZG9jdW1lbnQiLCJib2R5Iiwib3B0cyIsImNvbnRlbnQiLCJjbGFzcyIsImFkZENsYXNzIiwiY29uY2F0Iiwic2V0U3R5bGVzIiwiZGlzcGxheSIsInBvc2l0aW9uIiwiekluZGV4IiwiYXBwZW5kIiwib25BZGRJdGVtIiwiYXBwZW5kSXRlbSIsIml0ZW0iLCJlbCIsInJlbW92ZSIsIm9uUmVtb3ZlSXRlbSIsInJlbW92ZUl0ZW0iLCJwcmVwZW5kSXRlbSIsInByZXBlbmRDaGlsZCIsImFwcGVuZENoaWxkIiwicmVtb3ZlQ2hpbGQiLCJjbGVhciIsImVtcHR5IiwiaXNWaXNpYmxlIiwic3R5bGUiLCJwb3NpdGlvblBhbmUiLCJvcGVuIiwiY2xvc2UiLCJ0b2dnbGUiLCJkZXN0cm95IiwiQXR0YWNoZWRQYW5lIiwiYXR0YWNoVG8iLCJyZWN0IiwiZ2V0Qm91bmRpbmdDbGllbnRSZWN0Iiwic2Nyb2xsIiwieCIsImRvY3VtZW50RWxlbWVudCIsInNjcm9sbExlZnQiLCJ5Iiwic2Nyb2xsVG9wIiwidG9wIiwiY2xpZW50SGVpZ2h0IiwiYm90dG9tIiwibGVmdCIsInJpZ2h0IiwiY3VyIiwieiIsIndpbmRvdyIsImdldENvbXB1dGVkU3R5bGUiLCJnZXRQcm9wZXJ0eVZhbHVlIiwiTnVtYmVyIiwiaXNJbnRlZ2VyIiwidGVzdCIsInR6IiwiaXNTdHJpbmciLCJwYXJzZUludCIsInBhcmVudEVsZW1lbnQiLCJEcnVwYWwiXSwic291cmNlcyI6WyJ3aWRnZXRzL1BhbmUuZXM2LmpzIl0sInNvdXJjZXNDb250ZW50IjpbIigoeyBUb29sc2hlZDogdHMgfSkgPT4ge1xuICAvKipcbiAgICogQSBQYW5lIGlzIGZsb2F0aW5nIERJViB3aXRoIG1hbmFnZWFibGUgY29udGVudC5cbiAgICpcbiAgICogSXQgaXMgZGVzaWduZWQgdG8gYmUgdGhlIGJhc2Ugb2YgZGlhbG9ncywgcG9wLXVwcyBvciBvdGhlciBlbGVtZW50c1xuICAgKiB0aGF0IGF0dGFjaCB0byBvdGhlciBjb250ZW50LlxuICAgKi9cbiAgY2xhc3MgUGFuZSBleHRlbmRzIHRzLkVsZW1lbnQge1xuICAgIHN0YXRpYyBkZWZhdWx0T3B0aW9ucygpIHtcbiAgICAgIHJldHVybiB7XG4gICAgICAgIGNsYXNzZXM6IFtdLFxuICAgICAgfTtcbiAgICB9XG5cbiAgICAvKipcbiAgICAgKiBDcmVhdGUgYSBuZXcgaW5zdGFuY2Ugb2YgdGhlIGEgZGlzcGxheSBwYW5lLlxuICAgICAqXG4gICAgICogQHBhcmFtIHtPYmplY3R9IG9wdGlvbnNcbiAgICAgKiAgIFRoZSBjb25maWd1cmF0aW9uIG9wdGlvbnMgZm9yIGhvdyB0byBoYW5kbGUgcGFuZSBiZWhhdmlvcnMuXG4gICAgICovXG4gICAgY29uc3RydWN0b3Iob3B0aW9ucyA9IHt9KSB7XG4gICAgICBzdXBlcignZGl2JywgbnVsbCwgZG9jdW1lbnQuYm9keSk7XG5cbiAgICAgIHRoaXMub3B0cyA9IHtcbiAgICAgICAgLi4uUGFuZS5kZWZhdWx0T3B0aW9ucygpLFxuICAgICAgICAuLi5vcHRpb25zLFxuICAgICAgfTtcblxuICAgICAgdGhpcy5jb250ZW50ID0gbmV3IHRzLkVsZW1lbnQoJ2RpdicsIHsgY2xhc3M6ICdwYW5lX19jb250ZW50JyB9LCB0aGlzKTtcblxuICAgICAgdGhpcy5hZGRDbGFzcyh0aGlzLm9wdHMuY2xhc3Nlcy5jb25jYXQoJ3RzLXBhbmUnLCAncGFuZScpKTtcbiAgICAgIHRoaXMuc2V0U3R5bGVzKHtcbiAgICAgICAgZGlzcGxheTogJ25vbmUnLFxuICAgICAgICBwb3NpdGlvbjogJ2Fic29sdXRlJyxcbiAgICAgICAgekluZGV4OiB0aGlzLm9wdHMuekluZGV4LFxuICAgICAgfSk7XG5cbiAgICAgIHRoaXMuYXBwZW5kID0gIXRoaXMub3B0cy5vbkFkZEl0ZW0gPyB0aGlzLmFwcGVuZEl0ZW0gOiAoaXRlbSkgPT4ge1xuICAgICAgICBjb25zdCBlbCA9IHRoaXMub3B0cy5vbkFkZEl0ZW0oaXRlbSk7XG4gICAgICAgIHRoaXMuYXBwZW5kSXRlbShlbCB8fCBpdGVtKTtcbiAgICAgIH07XG5cbiAgICAgIHRoaXMucmVtb3ZlID0gIXRoaXMub3B0cy5vblJlbW92ZUl0ZW0gPyB0aGlzLnJlbW92ZUl0ZW0gOiAoaXRlbSkgPT4ge1xuICAgICAgICBjb25zdCBlbCA9IHRoaXMub3B0cy5vblJlbW92ZUl0ZW0oaXRlbSk7XG4gICAgICAgIHRoaXMucmVtb3ZlSXRlbShlbCB8fCBpdGVtKTtcbiAgICAgIH07XG4gICAgfVxuXG4gICAgLyoqXG4gICAgICogQWRkcyBhbmQgZWxlbWVudCB0byB0aGUgc3RhcnQgb2YgdGhlIGNvbnRlbnQgY29udGFpbmVyLlxuICAgICAqXG4gICAgICogQHBhcmFtIHtOb2RlfSBpdGVtXG4gICAgICogICBJdGVtIHRvIGFkZCB0byB0aGUgc3RhcnQgb2YgdGhlIHBhbmUgY29udGVudC5cbiAgICAgKi9cbiAgICBwcmVwZW5kSXRlbShpdGVtKSB7XG4gICAgICB0aGlzLmNvbnRlbnQucHJlcGVuZENoaWxkKGl0ZW0pO1xuICAgICAgcmV0dXJuIHRoaXM7XG4gICAgfVxuXG4gICAgLyoqXG4gICAgICogQWRkIGFuIGVsZW1lbnQgdG8gdGhlIGNvbnRlbnQgY29udGFpbmVyLlxuICAgICAqXG4gICAgICogQHBhcmFtIHtOb2RlfSBpdGVtXG4gICAgICogICBUaGUgbm9kZSB0byBhZGQgdG8gdGhlIGNvbnRlbnQgY29udGFpbmVyLlxuICAgICAqL1xuICAgIGFwcGVuZEl0ZW0oaXRlbSkge1xuICAgICAgdGhpcy5jb250ZW50LmFwcGVuZENoaWxkKGl0ZW0pO1xuICAgICAgcmV0dXJuIHRoaXM7XG4gICAgfVxuXG4gICAgLyoqXG4gICAgICogUmVtb3ZlIGFuIGVsZW1lbnQgZnJvbSB0aGUgY29udGVudCBjb250YWluZXIuXG4gICAgICpcbiAgICAgKiBAcGFyYW0ge05vZGV9IGl0ZW1cbiAgICAgKiAgIFRoZSBub2RlIHRvIHJlbW92ZSBmcm9tIHRoZSBjb250ZW50IGNvbnRhaW5lci5cbiAgICAgKi9cbiAgICByZW1vdmVJdGVtKGl0ZW0pIHtcbiAgICAgIHRoaXMuY29udGVudC5yZW1vdmVDaGlsZChpdGVtKTtcbiAgICAgIHJldHVybiB0aGlzO1xuICAgIH1cblxuICAgIC8qKlxuICAgICAqIENsZWFyIHRoZSBjb250ZW50IGNvbnRhaW5lciBvZiBhbGwgbm9kZXMgYW5kIGVsZW1lbnRzLlxuICAgICAqL1xuICAgIGNsZWFyKCkge1xuICAgICAgdGhpcy5jb250ZW50LmVtcHR5KCk7XG4gICAgICByZXR1cm4gdGhpcztcbiAgICB9XG5cbiAgICAvKipcbiAgICAgKiBJcyB0aGUgc3VnZ2VzdGlvbiB3aW5kb3cgb3Blbj9cbiAgICAgKlxuICAgICAqIEByZXR1cm4ge0Jvb2xlYW59XG4gICAgICogICBUUlVFIGlmIHRoZSBzdWdnZXN0aW9ucyB3aW5kb3cgaXMgb3Blbiwgb3RoZXJ3aXNlIEZBTFNFLlxuICAgICAqL1xuICAgIGlzVmlzaWJsZSgpIHtcbiAgICAgIHJldHVybiB0aGlzLnN0eWxlLmRpc3BsYXkgIT09ICdub25lJztcbiAgICB9XG5cbiAgICAvKipcbiAgICAgKiBQb3NpdGlvbiB0aGUgcGFuZSBiZWZvcmUgZGlzcGxheWluZyBpdC5cbiAgICAgKi9cbiAgICAvLyBlc2xpbnQtZGlzYWJsZS1uZXh0LWxpbmUgY2xhc3MtbWV0aG9kcy11c2UtdGhpc1xuICAgIHBvc2l0aW9uUGFuZSgpIHtcbiAgICAgIC8vIFRPRE86IHBvc2l0aW9uIGJhc2VkIG9uIHBhbmUgb3B0aW9ucy5cbiAgICB9XG5cbiAgICAvKipcbiAgICAgKiBQb3NpdGlvbiBhbmQgZXhwb3NlIHRoZSBzdWdnZXN0aW9ucyB3aW5kb3cgaWYgaXQgaXMgbm90IGFscmVhZHkgb3Blbi5cbiAgICAgKlxuICAgICAqIEByZXR1cm4ge2Jvb2x9XG4gICAgICogICBBbHdheXMgcmV0dXJucyB0cnVlLCB0byBpbmRpY2F0ZSB0aGF0IHRoZSBkaWFsb2cgaXMgb3Blbi5cbiAgICAgKi9cbiAgICBvcGVuKCkge1xuICAgICAgaWYgKCF0aGlzLmlzVmlzaWJsZSgpKSB7XG4gICAgICAgIHRoaXMuc3R5bGUuZGlzcGxheSA9ICcnO1xuXG4gICAgICAgIHRoaXMucG9zaXRpb25QYW5lKCk7XG4gICAgICB9XG4gICAgICByZXR1cm4gdHJ1ZTtcbiAgICB9XG5cbiAgICAvKipcbiAgICAgKiBDbG9zZSB0aGUgY29udGVudCBwYW5lLlxuICAgICAqXG4gICAgICogQHJldHVybiB7Ym9vbH1cbiAgICAgKiAgIEFsd2F5cyByZXR1cm5zIGZhbHNlLCB0byBpbmRpY2F0ZSB0aGF0IHRoZSBwYW5lIGlzIGNsb3NlZC5cbiAgICAgKi9cbiAgICBjbG9zZSgpIHtcbiAgICAgIHRoaXMuc3R5bGUuZGlzcGxheSA9ICdub25lJztcbiAgICAgIHJldHVybiBmYWxzZTtcbiAgICB9XG5cbiAgICAvKipcbiAgICAgKiBUb2dnbGUgdG8gb3BlbiBhbmQgY2xvc2Ugc3RhdHVzIG9mIHRoZSBwYW5lLlxuICAgICAqXG4gICAgICogQHJldHVybiB7Ym9vbH1cbiAgICAgKiAgIFJldHVybnMgdHJ1ZSBpZiByZXN1bHQgaXMgcGFuZSBpcyBvcGVuLCBhbmQgZmFsc2UgaWYgaXQgaXMgY2xvc2VkLlxuICAgICAqL1xuICAgIHRvZ2dsZSgpIHtcbiAgICAgIHJldHVybiB0aGlzLmlzVmlzaWJsZSgpID8gdGhpcy5jbG9zZSgpIDogdGhpcy5vcGVuKCk7XG4gICAgfVxuXG4gICAgLyoqXG4gICAgICogVGVhcmRvd24gYW5kIGNsZWFuLXVwIG9mIFBhbmUgY29udGVudHMuXG4gICAgICovXG4gICAgZGVzdHJveSgpIHtcbiAgICAgIHRoaXMuY2xlYXIoKTtcbiAgICAgIHN1cGVyLmRlc3Ryb3kodHJ1ZSk7XG4gICAgfVxuICB9XG5cbiAgLyoqXG4gICAqIEEgcGFuZSwgdGhhdCBhdHRhY2hlcyBpdHNlbGYgdG8gYW5vdGhlciBlbGVtZW50IGluIHRoZSBkb2N1bWVudC5cbiAgICpcbiAgICogQXR0YWNoZWRQYW5lcyBhcmUgYSBnb29kIGJhc2UgZm9yIHBvcC11cHMsIHRvb2xiYXJzIG9yIG90aGVyIGVsZW1lbnRzXG4gICAqIHRoYXQgZGlzcGxheSBjb250ZW50IGFuZCBhbmNob3IgdGhlbXNlbHZlcyB0byBvdGhlciBzY3JlZW4gZWxlbWVudHMuXG4gICAqL1xuICBjbGFzcyBBdHRhY2hlZFBhbmUgZXh0ZW5kcyBQYW5lIHtcbiAgICAvKipcbiAgICAgKiBDcmVhdGUgYSBuZXcgaW5zdGFuY2Ugb2YgYW4gQXR0YWNoZWRQYW5lLlxuICAgICAqXG4gICAgICogQHBhcmFtIHtET01FbGVtZW50fSBhdHRhY2hUb1xuICAgICAqICAgVGhlIGVsZW1lbnQgY29udGFpbmluZyB0aGUgcGFuZSBjb250ZW50LlxuICAgICAqIEBwYXJhbSB7T2JqZWN0fSBvcHRpb25zXG4gICAgICogICBUaGUgY29uZmlndXJhdGlvbiBvcHRpb25zIGZvciBob3cgdG8gaGFuZGxlIHBhbmUgYmVoYXZpb3JzLlxuICAgICAqL1xuICAgIGNvbnN0cnVjdG9yKGF0dGFjaFRvLCBvcHRpb25zID0ge30pIHtcbiAgICAgIHN1cGVyKG9wdGlvbnMpO1xuXG4gICAgICB0aGlzLmF0dGFjaFRvID0gYXR0YWNoVG87XG4gICAgfVxuXG4gICAgLyoqXG4gICAgICogQG92ZXJyaWRlXG4gICAgICovXG4gICAgcG9zaXRpb25QYW5lKCkge1xuICAgICAgY29uc3QgcmVjdCA9IHRoaXMuYXR0YWNoVG8uZ2V0Qm91bmRpbmdDbGllbnRSZWN0KCk7XG4gICAgICBjb25zdCBzY3JvbGwgPSB7XG4gICAgICAgIHg6IGRvY3VtZW50LmRvY3VtZW50RWxlbWVudC5zY3JvbGxMZWZ0IHx8IGRvY3VtZW50LmJvZHkuc2Nyb2xsTGVmdCxcbiAgICAgICAgeTogZG9jdW1lbnQuZG9jdW1lbnRFbGVtZW50LnNjcm9sbFRvcCB8fCBkb2N1bWVudC5ib2R5LnNjcm9sbFRvcCxcbiAgICAgIH07XG5cbiAgICAgIHRoaXMuc3R5bGUudG9wID0gKHJlY3QudG9wID4gdGhpcy5lbC5jbGllbnRIZWlnaHQpXG4gICAgICAgID8gYCR7KHNjcm9sbC55IC0gdGhpcy5lbC5jbGllbnRIZWlnaHQpICsgcmVjdC5ib3R0b219cHhgXG4gICAgICAgIDogYCR7c2Nyb2xsLnkgKyByZWN0LnRvcH1weGA7XG5cbiAgICAgIHRoaXMuc3R5bGUubGVmdCA9IGAke3Njcm9sbC54ICsgcmVjdC5yaWdodH1weGA7XG5cbiAgICAgIC8vIENhbGN1bGF0ZSB0aGUgei1pbmRleCBvZiB0aGUgYXR0YWNoZWQgYnV0dG9uLCB0aGlzIGJlY29tZXMgaW1wb3J0YW50LFxuICAgICAgLy8gd2hlbiBkZWFsaW5nIHdpdGggZm9ybSBlbGVtZW50cyBpbiBwb3AtdXAgZGlhbG9ncy4gT25seSBjYWxjdWxhdGUgdGhpc1xuICAgICAgLy8gaWYgbm90IGFscmVhZHkgZXhwbGljaXRseSBzZXQgYnkgdGhlIG9wdGlvbnMuXG4gICAgICBpZiAoIXRoaXMub3B0cy56SW5kZXggJiYgIXRoaXMuc3R5bGUuekluZGV4KSB7XG4gICAgICAgIGxldCBjdXIgPSB0aGlzLmF0dGFjaFRvO1xuICAgICAgICBsZXQgekluZGV4ID0gMTA7XG5cbiAgICAgICAgd2hpbGUgKGN1cikge1xuICAgICAgICAgIGNvbnN0IHogPSAoY3VyLnN0eWxlIHx8IHt9KS56SW5kZXggfHwgd2luZG93LmdldENvbXB1dGVkU3R5bGUoY3VyKS5nZXRQcm9wZXJ0eVZhbHVlKCd6LWluZGV4Jyk7XG5cbiAgICAgICAgICBpZiAoTnVtYmVyLmlzSW50ZWdlcih6KSB8fCAvXFxkKy8udGVzdCh6KSkge1xuICAgICAgICAgICAgY29uc3QgdHogPSAodHMuaXNTdHJpbmcoeikgPyBwYXJzZUludCh6LCAxMCkgOiB6KSArIDEwO1xuICAgICAgICAgICAgaWYgKHR6ID4gekluZGV4KSB6SW5kZXggPSB0ejtcbiAgICAgICAgICB9XG5cbiAgICAgICAgICBjdXIgPSBjdXIucGFyZW50RWxlbWVudDtcbiAgICAgICAgfVxuXG4gICAgICAgIHRoaXMuc3R5bGUuekluZGV4ID0gekluZGV4O1xuICAgICAgfVxuICAgIH1cbiAgfVxuXG4gIHRzLlBhbmUgPSBQYW5lO1xuICB0cy5BdHRhY2hlZFBhbmUgPSBBdHRhY2hlZFBhbmU7XG59KShEcnVwYWwpO1xuIl0sIm1hcHBpbmdzIjoiOztBQUFBLENBQUMsQ0FBQztFQUFFQSxRQUFRLEVBQUVDO0FBQUcsQ0FBQyxLQUFLO0VBQ3JCO0FBQ0Y7QUFDQTtBQUNBO0FBQ0E7QUFDQTtFQUNFLE1BQU1DLElBQUksU0FBU0QsRUFBRSxDQUFDRSxPQUFPLENBQUM7SUFDNUIsT0FBT0MsY0FBYyxHQUFHO01BQ3RCLE9BQU87UUFDTEMsT0FBTyxFQUFFO01BQ1gsQ0FBQztJQUNIOztJQUVBO0FBQ0o7QUFDQTtBQUNBO0FBQ0E7QUFDQTtJQUNJQyxXQUFXLENBQUNDLE9BQU8sR0FBRyxDQUFDLENBQUMsRUFBRTtNQUN4QixLQUFLLENBQUMsS0FBSyxFQUFFLElBQUksRUFBRUMsUUFBUSxDQUFDQyxJQUFJLENBQUM7TUFFakMsSUFBSSxDQUFDQyxJQUFJLEdBQUc7UUFDVixHQUFHUixJQUFJLENBQUNFLGNBQWMsRUFBRTtRQUN4QixHQUFHRztNQUNMLENBQUM7TUFFRCxJQUFJLENBQUNJLE9BQU8sR0FBRyxJQUFJVixFQUFFLENBQUNFLE9BQU8sQ0FBQyxLQUFLLEVBQUU7UUFBRVMsS0FBSyxFQUFFO01BQWdCLENBQUMsRUFBRSxJQUFJLENBQUM7TUFFdEUsSUFBSSxDQUFDQyxRQUFRLENBQUMsSUFBSSxDQUFDSCxJQUFJLENBQUNMLE9BQU8sQ0FBQ1MsTUFBTSxDQUFDLFNBQVMsRUFBRSxNQUFNLENBQUMsQ0FBQztNQUMxRCxJQUFJLENBQUNDLFNBQVMsQ0FBQztRQUNiQyxPQUFPLEVBQUUsTUFBTTtRQUNmQyxRQUFRLEVBQUUsVUFBVTtRQUNwQkMsTUFBTSxFQUFFLElBQUksQ0FBQ1IsSUFBSSxDQUFDUTtNQUNwQixDQUFDLENBQUM7TUFFRixJQUFJLENBQUNDLE1BQU0sR0FBRyxDQUFDLElBQUksQ0FBQ1QsSUFBSSxDQUFDVSxTQUFTLEdBQUcsSUFBSSxDQUFDQyxVQUFVLEdBQUlDLElBQUksSUFBSztRQUMvRCxNQUFNQyxFQUFFLEdBQUcsSUFBSSxDQUFDYixJQUFJLENBQUNVLFNBQVMsQ0FBQ0UsSUFBSSxDQUFDO1FBQ3BDLElBQUksQ0FBQ0QsVUFBVSxDQUFDRSxFQUFFLElBQUlELElBQUksQ0FBQztNQUM3QixDQUFDO01BRUQsSUFBSSxDQUFDRSxNQUFNLEdBQUcsQ0FBQyxJQUFJLENBQUNkLElBQUksQ0FBQ2UsWUFBWSxHQUFHLElBQUksQ0FBQ0MsVUFBVSxHQUFJSixJQUFJLElBQUs7UUFDbEUsTUFBTUMsRUFBRSxHQUFHLElBQUksQ0FBQ2IsSUFBSSxDQUFDZSxZQUFZLENBQUNILElBQUksQ0FBQztRQUN2QyxJQUFJLENBQUNJLFVBQVUsQ0FBQ0gsRUFBRSxJQUFJRCxJQUFJLENBQUM7TUFDN0IsQ0FBQztJQUNIOztJQUVBO0FBQ0o7QUFDQTtBQUNBO0FBQ0E7QUFDQTtJQUNJSyxXQUFXLENBQUNMLElBQUksRUFBRTtNQUNoQixJQUFJLENBQUNYLE9BQU8sQ0FBQ2lCLFlBQVksQ0FBQ04sSUFBSSxDQUFDO01BQy9CLE9BQU8sSUFBSTtJQUNiOztJQUVBO0FBQ0o7QUFDQTtBQUNBO0FBQ0E7QUFDQTtJQUNJRCxVQUFVLENBQUNDLElBQUksRUFBRTtNQUNmLElBQUksQ0FBQ1gsT0FBTyxDQUFDa0IsV0FBVyxDQUFDUCxJQUFJLENBQUM7TUFDOUIsT0FBTyxJQUFJO0lBQ2I7O0lBRUE7QUFDSjtBQUNBO0FBQ0E7QUFDQTtBQUNBO0lBQ0lJLFVBQVUsQ0FBQ0osSUFBSSxFQUFFO01BQ2YsSUFBSSxDQUFDWCxPQUFPLENBQUNtQixXQUFXLENBQUNSLElBQUksQ0FBQztNQUM5QixPQUFPLElBQUk7SUFDYjs7SUFFQTtBQUNKO0FBQ0E7SUFDSVMsS0FBSyxHQUFHO01BQ04sSUFBSSxDQUFDcEIsT0FBTyxDQUFDcUIsS0FBSyxFQUFFO01BQ3BCLE9BQU8sSUFBSTtJQUNiOztJQUVBO0FBQ0o7QUFDQTtBQUNBO0FBQ0E7QUFDQTtJQUNJQyxTQUFTLEdBQUc7TUFDVixPQUFPLElBQUksQ0FBQ0MsS0FBSyxDQUFDbEIsT0FBTyxLQUFLLE1BQU07SUFDdEM7O0lBRUE7QUFDSjtBQUNBO0lBQ0k7SUFDQW1CLFlBQVksR0FBRztNQUNiO0lBQUE7O0lBR0Y7QUFDSjtBQUNBO0FBQ0E7QUFDQTtBQUNBO0lBQ0lDLElBQUksR0FBRztNQUNMLElBQUksQ0FBQyxJQUFJLENBQUNILFNBQVMsRUFBRSxFQUFFO1FBQ3JCLElBQUksQ0FBQ0MsS0FBSyxDQUFDbEIsT0FBTyxHQUFHLEVBQUU7UUFFdkIsSUFBSSxDQUFDbUIsWUFBWSxFQUFFO01BQ3JCO01BQ0EsT0FBTyxJQUFJO0lBQ2I7O0lBRUE7QUFDSjtBQUNBO0FBQ0E7QUFDQTtBQUNBO0lBQ0lFLEtBQUssR0FBRztNQUNOLElBQUksQ0FBQ0gsS0FBSyxDQUFDbEIsT0FBTyxHQUFHLE1BQU07TUFDM0IsT0FBTyxLQUFLO0lBQ2Q7O0lBRUE7QUFDSjtBQUNBO0FBQ0E7QUFDQTtBQUNBO0lBQ0lzQixNQUFNLEdBQUc7TUFDUCxPQUFPLElBQUksQ0FBQ0wsU0FBUyxFQUFFLEdBQUcsSUFBSSxDQUFDSSxLQUFLLEVBQUUsR0FBRyxJQUFJLENBQUNELElBQUksRUFBRTtJQUN0RDs7SUFFQTtBQUNKO0FBQ0E7SUFDSUcsT0FBTyxHQUFHO01BQ1IsSUFBSSxDQUFDUixLQUFLLEVBQUU7TUFDWixLQUFLLENBQUNRLE9BQU8sQ0FBQyxJQUFJLENBQUM7SUFDckI7RUFDRjs7RUFFQTtBQUNGO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7RUFDRSxNQUFNQyxZQUFZLFNBQVN0QyxJQUFJLENBQUM7SUFDOUI7QUFDSjtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtJQUNJSSxXQUFXLENBQUNtQyxRQUFRLEVBQUVsQyxPQUFPLEdBQUcsQ0FBQyxDQUFDLEVBQUU7TUFDbEMsS0FBSyxDQUFDQSxPQUFPLENBQUM7TUFFZCxJQUFJLENBQUNrQyxRQUFRLEdBQUdBLFFBQVE7SUFDMUI7O0lBRUE7QUFDSjtBQUNBO0lBQ0lOLFlBQVksR0FBRztNQUNiLE1BQU1PLElBQUksR0FBRyxJQUFJLENBQUNELFFBQVEsQ0FBQ0UscUJBQXFCLEVBQUU7TUFDbEQsTUFBTUMsTUFBTSxHQUFHO1FBQ2JDLENBQUMsRUFBRXJDLFFBQVEsQ0FBQ3NDLGVBQWUsQ0FBQ0MsVUFBVSxJQUFJdkMsUUFBUSxDQUFDQyxJQUFJLENBQUNzQyxVQUFVO1FBQ2xFQyxDQUFDLEVBQUV4QyxRQUFRLENBQUNzQyxlQUFlLENBQUNHLFNBQVMsSUFBSXpDLFFBQVEsQ0FBQ0MsSUFBSSxDQUFDd0M7TUFDekQsQ0FBQztNQUVELElBQUksQ0FBQ2YsS0FBSyxDQUFDZ0IsR0FBRyxHQUFJUixJQUFJLENBQUNRLEdBQUcsR0FBRyxJQUFJLENBQUMzQixFQUFFLENBQUM0QixZQUFZLEdBQzVDLEdBQUdQLE1BQU0sQ0FBQ0ksQ0FBQyxHQUFHLElBQUksQ0FBQ3pCLEVBQUUsQ0FBQzRCLFlBQVksR0FBSVQsSUFBSSxDQUFDVSxNQUFPLElBQUcsR0FDckQsR0FBRVIsTUFBTSxDQUFDSSxDQUFDLEdBQUdOLElBQUksQ0FBQ1EsR0FBSSxJQUFHO01BRTlCLElBQUksQ0FBQ2hCLEtBQUssQ0FBQ21CLElBQUksR0FBSSxHQUFFVCxNQUFNLENBQUNDLENBQUMsR0FBR0gsSUFBSSxDQUFDWSxLQUFNLElBQUc7O01BRTlDO01BQ0E7TUFDQTtNQUNBLElBQUksQ0FBQyxJQUFJLENBQUM1QyxJQUFJLENBQUNRLE1BQU0sSUFBSSxDQUFDLElBQUksQ0FBQ2dCLEtBQUssQ0FBQ2hCLE1BQU0sRUFBRTtRQUMzQyxJQUFJcUMsR0FBRyxHQUFHLElBQUksQ0FBQ2QsUUFBUTtRQUN2QixJQUFJdkIsTUFBTSxHQUFHLEVBQUU7UUFFZixPQUFPcUMsR0FBRyxFQUFFO1VBQ1YsTUFBTUMsQ0FBQyxHQUFHLENBQUNELEdBQUcsQ0FBQ3JCLEtBQUssSUFBSSxDQUFDLENBQUMsRUFBRWhCLE1BQU0sSUFBSXVDLE1BQU0sQ0FBQ0MsZ0JBQWdCLENBQUNILEdBQUcsQ0FBQyxDQUFDSSxnQkFBZ0IsQ0FBQyxTQUFTLENBQUM7VUFFOUYsSUFBSUMsTUFBTSxDQUFDQyxTQUFTLENBQUNMLENBQUMsQ0FBQyxJQUFJLEtBQUssQ0FBQ00sSUFBSSxDQUFDTixDQUFDLENBQUMsRUFBRTtZQUN4QyxNQUFNTyxFQUFFLEdBQUcsQ0FBQzlELEVBQUUsQ0FBQytELFFBQVEsQ0FBQ1IsQ0FBQyxDQUFDLEdBQUdTLFFBQVEsQ0FBQ1QsQ0FBQyxFQUFFLEVBQUUsQ0FBQyxHQUFHQSxDQUFDLElBQUksRUFBRTtZQUN0RCxJQUFJTyxFQUFFLEdBQUc3QyxNQUFNLEVBQUVBLE1BQU0sR0FBRzZDLEVBQUU7VUFDOUI7VUFFQVIsR0FBRyxHQUFHQSxHQUFHLENBQUNXLGFBQWE7UUFDekI7UUFFQSxJQUFJLENBQUNoQyxLQUFLLENBQUNoQixNQUFNLEdBQUdBLE1BQU07TUFDNUI7SUFDRjtFQUNGO0VBRUFqQixFQUFFLENBQUNDLElBQUksR0FBR0EsSUFBSTtFQUNkRCxFQUFFLENBQUN1QyxZQUFZLEdBQUdBLFlBQVk7QUFDaEMsQ0FBQyxFQUFFMkIsTUFBTSxDQUFDIn0=
