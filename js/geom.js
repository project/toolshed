"use strict";

/* eslint no-bitwise: ["error", { "allow": ["|"] }] */
(({
  Toolshed: ts
}) => {
  ts.Geom = {};

  /**
   * Defines a enumerations for different edges.
   *
   * @type {Object}
   */
  class Edges {
    static get TOP() {
      return 0x01;
    }
    static get BOTTOM() {
      return 0x02;
    }
    static get LEFT() {
      return 0x10;
    }
    static get RIGHT() {
      return 0x20;
    }
  }

  /**
   * Defines a directions enumerations or mask.
   *
   * @type {Object}
   */
  class Direction {
    static get VERTICAL() {
      return Edges.TOP | Edges.BOTTOM;
    }
    static get HORIZONTAL() {
      return Edges.LEFT | Edges.RIGHT;
    }
    static get ANY() {
      return Direction.HORIZONTAL | Direction.VERTICAL;
    }
  }

  /**
   * Coordinates to decribe a rectangular region in space.
   */
  class Rect {
    /**
     * Create a new Rect instance from the bounding area of a DOM element.
     *
     * @param {Element} el
     *   DOM element to create the Rectangle from.
     *
     * @return {Rect}
     *   A new Rect instance representing the bounding box of the element's
     *   client rectangular space.
     */
    static fromElement(el) {
      const b = el.getBoundingClientRect();
      return new Rect(b.left, b.top, b.right, b.bottom);
    }

    /**
     * @param {Toolshed.Rect|int} left
     *  Either a jQuery wrapper or Rect object to build the rectangle with.
     *  If just an integer, use this value as the left edge of the rectangle.
     * @param {int} top
     *  The coordinate of the top edge of the rectangle.
     * @param {int} right
     *  The coordinate of the right edge of the rectangle.
     * @param {int} bottom
     *  The coordinate of the bottom edge of the rectangle.
     */
    constructor(left, top, right, bottom) {
      if (left instanceof ts.Geom.Rect) {
        Object.assign(this, left);
      } else {
        if (top <= bottom) {
          this.top = top;
          this.bottom = bottom;
        } else {
          this.top = bottom;
          this.bottom = top;
        }
        if (left <= right) {
          this.left = left;
          this.right = right;
        } else {
          this.left = right;
          this.right = left;
        }
      }
    }

    /**
     * Get the a point indicating the top-left corner of the rectangle.
     *
     * @return {object}
     *   Object with a left and right property that present a point in space.
     */
    getPosition() {
      return {
        left: this.left,
        top: this.top
      };
    }

    /**
     * Get the width of the rectangle.
     *
     * @return {int}
     *   The width of the rectangular area.
     */
    getWidth() {
      return this.right - this.left;
    }

    /**
     * Get the height of the rectangle.
     *
     * @return {int}
     *   The height of the rectangular area.
     */
    getHeight() {
      return this.bottom - this.top;
    }

    /**
     * Check if a point in space is inside of the rectangle.
     *
     * @param {Object} pt
     *   An object with a left and top property to represent a point in space.
     *
     * @return {bool}
     *   If the point is inside of this rectangular area.
     */
    isInRect(pt) {
      const pos = this.getPosition();
      pt.left -= pos.left;
      pt.top -= pos.top;
      return pt.left >= 0 && pt.left <= this.getWidth() && pt.top >= 0 && pt.top <= this.getHeight();
    }

    /**
     * Get the volume / area represented by this rectangle.
     *
     * @return {int}
     *   The area represented by this rectangle.
     */
    getArea() {
      return this.getWidth() * this.getHeight();
    }

    /**
     * Offset the rectangle by a X and Y offset.
     *
     * @param {int} xOffset
     *   Number of units to move in the x axis. Can be negative.
     * @param {int} yOffset
     *   Number of units to move in the y axis. Can be negative.
     */
    offset(xOffset, yOffset) {
      this.top += yOffset;
      this.bottom += yOffset;
      this.left += xOffset;
      this.right += xOffset;
    }

    /**
     * Get a Rect object representing the intersection of the two rectangles.
     *
     * @param {Rect} rect
     *   The rectangle to find and intersection of.
     *
     * @return {Rect|null}
     *   The resulting intersection if there was one, otherwise NULL.
     */
    getIntersection(rect) {
      const o1 = rect.getPosition();
      const o2 = this.getPosition();
      const x = Math.max(o1.left, o2.left);
      const y = Math.max(o1.top, o2.top);
      const r = Math.min(o1.left + rect.getWidth(), o2.left + this.getWidth());
      const b = Math.min(o1.top + rect.getHeight(), o2.top + this.getHeight());

      // Check that this point is in the rectangle.
      return x > r || y > b ? null : new Rect(x, y, r, b);
    }

    /**
     * Test if a rectangle is completely inside this rectangle.
     *
     * @param {Rect} rect
     *   Rectangle to test if it was contained inside of.
     *
     * @return {bool}
     *   True IFF the rect parameter represents and area that is completely
     *   inside this Rect instance.
     */
    contains(rect) {
      return rect.left >= this.left && rect.right <= this.right && rect.top >= this.top && rect.bottom <= this.bottom;
    }
  }

  // Export the classes to the Toolshed namespace.
  ts.Geom.Edges = Edges;
  ts.Geom.Direction = Direction;
  ts.Geom.Rect = Rect;
})(Drupal);
//# sourceMappingURL=data:application/json;charset=utf8;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoiZ2VvbS5qcyIsIm5hbWVzIjpbIlRvb2xzaGVkIiwidHMiLCJHZW9tIiwiRWRnZXMiLCJUT1AiLCJCT1RUT00iLCJMRUZUIiwiUklHSFQiLCJEaXJlY3Rpb24iLCJWRVJUSUNBTCIsIkhPUklaT05UQUwiLCJBTlkiLCJSZWN0IiwiZnJvbUVsZW1lbnQiLCJlbCIsImIiLCJnZXRCb3VuZGluZ0NsaWVudFJlY3QiLCJsZWZ0IiwidG9wIiwicmlnaHQiLCJib3R0b20iLCJjb25zdHJ1Y3RvciIsIk9iamVjdCIsImFzc2lnbiIsImdldFBvc2l0aW9uIiwiZ2V0V2lkdGgiLCJnZXRIZWlnaHQiLCJpc0luUmVjdCIsInB0IiwicG9zIiwiZ2V0QXJlYSIsIm9mZnNldCIsInhPZmZzZXQiLCJ5T2Zmc2V0IiwiZ2V0SW50ZXJzZWN0aW9uIiwicmVjdCIsIm8xIiwibzIiLCJ4IiwiTWF0aCIsIm1heCIsInkiLCJyIiwibWluIiwiY29udGFpbnMiLCJEcnVwYWwiXSwic291cmNlcyI6WyJnZW9tLmVzNi5qcyJdLCJzb3VyY2VzQ29udGVudCI6WyIvKiBlc2xpbnQgbm8tYml0d2lzZTogW1wiZXJyb3JcIiwgeyBcImFsbG93XCI6IFtcInxcIl0gfV0gKi9cbigoeyBUb29sc2hlZDogdHMgfSkgPT4ge1xuICB0cy5HZW9tID0ge307XG5cbiAgLyoqXG4gICAqIERlZmluZXMgYSBlbnVtZXJhdGlvbnMgZm9yIGRpZmZlcmVudCBlZGdlcy5cbiAgICpcbiAgICogQHR5cGUge09iamVjdH1cbiAgICovXG4gIGNsYXNzIEVkZ2VzIHtcbiAgICBzdGF0aWMgZ2V0IFRPUCgpIHtcbiAgICAgIHJldHVybiAweDAxO1xuICAgIH1cblxuICAgIHN0YXRpYyBnZXQgQk9UVE9NKCkge1xuICAgICAgcmV0dXJuIDB4MDI7XG4gICAgfVxuXG4gICAgc3RhdGljIGdldCBMRUZUKCkge1xuICAgICAgcmV0dXJuIDB4MTA7XG4gICAgfVxuXG4gICAgc3RhdGljIGdldCBSSUdIVCgpIHtcbiAgICAgIHJldHVybiAweDIwO1xuICAgIH1cbiAgfVxuXG4gIC8qKlxuICAgKiBEZWZpbmVzIGEgZGlyZWN0aW9ucyBlbnVtZXJhdGlvbnMgb3IgbWFzay5cbiAgICpcbiAgICogQHR5cGUge09iamVjdH1cbiAgICovXG4gIGNsYXNzIERpcmVjdGlvbiB7XG4gICAgc3RhdGljIGdldCBWRVJUSUNBTCgpIHtcbiAgICAgIHJldHVybiBFZGdlcy5UT1AgfCBFZGdlcy5CT1RUT007XG4gICAgfVxuXG4gICAgc3RhdGljIGdldCBIT1JJWk9OVEFMKCkge1xuICAgICAgcmV0dXJuIEVkZ2VzLkxFRlQgfCBFZGdlcy5SSUdIVDtcbiAgICB9XG5cbiAgICBzdGF0aWMgZ2V0IEFOWSgpIHtcbiAgICAgIHJldHVybiBEaXJlY3Rpb24uSE9SSVpPTlRBTCB8IERpcmVjdGlvbi5WRVJUSUNBTDtcbiAgICB9XG4gIH1cblxuICAvKipcbiAgICogQ29vcmRpbmF0ZXMgdG8gZGVjcmliZSBhIHJlY3Rhbmd1bGFyIHJlZ2lvbiBpbiBzcGFjZS5cbiAgICovXG4gIGNsYXNzIFJlY3Qge1xuICAgIC8qKlxuICAgICAqIENyZWF0ZSBhIG5ldyBSZWN0IGluc3RhbmNlIGZyb20gdGhlIGJvdW5kaW5nIGFyZWEgb2YgYSBET00gZWxlbWVudC5cbiAgICAgKlxuICAgICAqIEBwYXJhbSB7RWxlbWVudH0gZWxcbiAgICAgKiAgIERPTSBlbGVtZW50IHRvIGNyZWF0ZSB0aGUgUmVjdGFuZ2xlIGZyb20uXG4gICAgICpcbiAgICAgKiBAcmV0dXJuIHtSZWN0fVxuICAgICAqICAgQSBuZXcgUmVjdCBpbnN0YW5jZSByZXByZXNlbnRpbmcgdGhlIGJvdW5kaW5nIGJveCBvZiB0aGUgZWxlbWVudCdzXG4gICAgICogICBjbGllbnQgcmVjdGFuZ3VsYXIgc3BhY2UuXG4gICAgICovXG4gICAgc3RhdGljIGZyb21FbGVtZW50KGVsKSB7XG4gICAgICBjb25zdCBiID0gZWwuZ2V0Qm91bmRpbmdDbGllbnRSZWN0KCk7XG4gICAgICByZXR1cm4gbmV3IFJlY3QoYi5sZWZ0LCBiLnRvcCwgYi5yaWdodCwgYi5ib3R0b20pO1xuICAgIH1cblxuICAgIC8qKlxuICAgICAqIEBwYXJhbSB7VG9vbHNoZWQuUmVjdHxpbnR9IGxlZnRcbiAgICAgKiAgRWl0aGVyIGEgalF1ZXJ5IHdyYXBwZXIgb3IgUmVjdCBvYmplY3QgdG8gYnVpbGQgdGhlIHJlY3RhbmdsZSB3aXRoLlxuICAgICAqICBJZiBqdXN0IGFuIGludGVnZXIsIHVzZSB0aGlzIHZhbHVlIGFzIHRoZSBsZWZ0IGVkZ2Ugb2YgdGhlIHJlY3RhbmdsZS5cbiAgICAgKiBAcGFyYW0ge2ludH0gdG9wXG4gICAgICogIFRoZSBjb29yZGluYXRlIG9mIHRoZSB0b3AgZWRnZSBvZiB0aGUgcmVjdGFuZ2xlLlxuICAgICAqIEBwYXJhbSB7aW50fSByaWdodFxuICAgICAqICBUaGUgY29vcmRpbmF0ZSBvZiB0aGUgcmlnaHQgZWRnZSBvZiB0aGUgcmVjdGFuZ2xlLlxuICAgICAqIEBwYXJhbSB7aW50fSBib3R0b21cbiAgICAgKiAgVGhlIGNvb3JkaW5hdGUgb2YgdGhlIGJvdHRvbSBlZGdlIG9mIHRoZSByZWN0YW5nbGUuXG4gICAgICovXG4gICAgY29uc3RydWN0b3IobGVmdCwgdG9wLCByaWdodCwgYm90dG9tKSB7XG4gICAgICBpZiAobGVmdCBpbnN0YW5jZW9mIHRzLkdlb20uUmVjdCkge1xuICAgICAgICBPYmplY3QuYXNzaWduKHRoaXMsIGxlZnQpO1xuICAgICAgfVxuICAgICAgZWxzZSB7XG4gICAgICAgIGlmICh0b3AgPD0gYm90dG9tKSB7XG4gICAgICAgICAgdGhpcy50b3AgPSB0b3A7XG4gICAgICAgICAgdGhpcy5ib3R0b20gPSBib3R0b207XG4gICAgICAgIH1cbiAgICAgICAgZWxzZSB7XG4gICAgICAgICAgdGhpcy50b3AgPSBib3R0b207XG4gICAgICAgICAgdGhpcy5ib3R0b20gPSB0b3A7XG4gICAgICAgIH1cblxuICAgICAgICBpZiAobGVmdCA8PSByaWdodCkge1xuICAgICAgICAgIHRoaXMubGVmdCA9IGxlZnQ7XG4gICAgICAgICAgdGhpcy5yaWdodCA9IHJpZ2h0O1xuICAgICAgICB9XG4gICAgICAgIGVsc2Uge1xuICAgICAgICAgIHRoaXMubGVmdCA9IHJpZ2h0O1xuICAgICAgICAgIHRoaXMucmlnaHQgPSBsZWZ0O1xuICAgICAgICB9XG4gICAgICB9XG4gICAgfVxuXG4gICAgLyoqXG4gICAgICogR2V0IHRoZSBhIHBvaW50IGluZGljYXRpbmcgdGhlIHRvcC1sZWZ0IGNvcm5lciBvZiB0aGUgcmVjdGFuZ2xlLlxuICAgICAqXG4gICAgICogQHJldHVybiB7b2JqZWN0fVxuICAgICAqICAgT2JqZWN0IHdpdGggYSBsZWZ0IGFuZCByaWdodCBwcm9wZXJ0eSB0aGF0IHByZXNlbnQgYSBwb2ludCBpbiBzcGFjZS5cbiAgICAgKi9cbiAgICBnZXRQb3NpdGlvbigpIHtcbiAgICAgIHJldHVybiB7IGxlZnQ6IHRoaXMubGVmdCwgdG9wOiB0aGlzLnRvcCB9O1xuICAgIH1cblxuICAgIC8qKlxuICAgICAqIEdldCB0aGUgd2lkdGggb2YgdGhlIHJlY3RhbmdsZS5cbiAgICAgKlxuICAgICAqIEByZXR1cm4ge2ludH1cbiAgICAgKiAgIFRoZSB3aWR0aCBvZiB0aGUgcmVjdGFuZ3VsYXIgYXJlYS5cbiAgICAgKi9cbiAgICBnZXRXaWR0aCgpIHtcbiAgICAgIHJldHVybiB0aGlzLnJpZ2h0IC0gdGhpcy5sZWZ0O1xuICAgIH1cblxuICAgIC8qKlxuICAgICAqIEdldCB0aGUgaGVpZ2h0IG9mIHRoZSByZWN0YW5nbGUuXG4gICAgICpcbiAgICAgKiBAcmV0dXJuIHtpbnR9XG4gICAgICogICBUaGUgaGVpZ2h0IG9mIHRoZSByZWN0YW5ndWxhciBhcmVhLlxuICAgICAqL1xuICAgIGdldEhlaWdodCgpIHtcbiAgICAgIHJldHVybiB0aGlzLmJvdHRvbSAtIHRoaXMudG9wO1xuICAgIH1cblxuICAgIC8qKlxuICAgICAqIENoZWNrIGlmIGEgcG9pbnQgaW4gc3BhY2UgaXMgaW5zaWRlIG9mIHRoZSByZWN0YW5nbGUuXG4gICAgICpcbiAgICAgKiBAcGFyYW0ge09iamVjdH0gcHRcbiAgICAgKiAgIEFuIG9iamVjdCB3aXRoIGEgbGVmdCBhbmQgdG9wIHByb3BlcnR5IHRvIHJlcHJlc2VudCBhIHBvaW50IGluIHNwYWNlLlxuICAgICAqXG4gICAgICogQHJldHVybiB7Ym9vbH1cbiAgICAgKiAgIElmIHRoZSBwb2ludCBpcyBpbnNpZGUgb2YgdGhpcyByZWN0YW5ndWxhciBhcmVhLlxuICAgICAqL1xuICAgIGlzSW5SZWN0KHB0KSB7XG4gICAgICBjb25zdCBwb3MgPSB0aGlzLmdldFBvc2l0aW9uKCk7XG4gICAgICBwdC5sZWZ0IC09IHBvcy5sZWZ0O1xuICAgICAgcHQudG9wIC09IHBvcy50b3A7XG5cbiAgICAgIHJldHVybiAocHQubGVmdCA+PSAwICYmIHB0LmxlZnQgPD0gdGhpcy5nZXRXaWR0aCgpKVxuICAgICAgICAmJiAocHQudG9wID49IDAgJiYgcHQudG9wIDw9IHRoaXMuZ2V0SGVpZ2h0KCkpO1xuICAgIH1cblxuICAgIC8qKlxuICAgICAqIEdldCB0aGUgdm9sdW1lIC8gYXJlYSByZXByZXNlbnRlZCBieSB0aGlzIHJlY3RhbmdsZS5cbiAgICAgKlxuICAgICAqIEByZXR1cm4ge2ludH1cbiAgICAgKiAgIFRoZSBhcmVhIHJlcHJlc2VudGVkIGJ5IHRoaXMgcmVjdGFuZ2xlLlxuICAgICAqL1xuICAgIGdldEFyZWEoKSB7XG4gICAgICByZXR1cm4gdGhpcy5nZXRXaWR0aCgpICogdGhpcy5nZXRIZWlnaHQoKTtcbiAgICB9XG5cbiAgICAvKipcbiAgICAgKiBPZmZzZXQgdGhlIHJlY3RhbmdsZSBieSBhIFggYW5kIFkgb2Zmc2V0LlxuICAgICAqXG4gICAgICogQHBhcmFtIHtpbnR9IHhPZmZzZXRcbiAgICAgKiAgIE51bWJlciBvZiB1bml0cyB0byBtb3ZlIGluIHRoZSB4IGF4aXMuIENhbiBiZSBuZWdhdGl2ZS5cbiAgICAgKiBAcGFyYW0ge2ludH0geU9mZnNldFxuICAgICAqICAgTnVtYmVyIG9mIHVuaXRzIHRvIG1vdmUgaW4gdGhlIHkgYXhpcy4gQ2FuIGJlIG5lZ2F0aXZlLlxuICAgICAqL1xuICAgIG9mZnNldCh4T2Zmc2V0LCB5T2Zmc2V0KSB7XG4gICAgICB0aGlzLnRvcCArPSB5T2Zmc2V0O1xuICAgICAgdGhpcy5ib3R0b20gKz0geU9mZnNldDtcbiAgICAgIHRoaXMubGVmdCArPSB4T2Zmc2V0O1xuICAgICAgdGhpcy5yaWdodCArPSB4T2Zmc2V0O1xuICAgIH1cblxuICAgIC8qKlxuICAgICAqIEdldCBhIFJlY3Qgb2JqZWN0IHJlcHJlc2VudGluZyB0aGUgaW50ZXJzZWN0aW9uIG9mIHRoZSB0d28gcmVjdGFuZ2xlcy5cbiAgICAgKlxuICAgICAqIEBwYXJhbSB7UmVjdH0gcmVjdFxuICAgICAqICAgVGhlIHJlY3RhbmdsZSB0byBmaW5kIGFuZCBpbnRlcnNlY3Rpb24gb2YuXG4gICAgICpcbiAgICAgKiBAcmV0dXJuIHtSZWN0fG51bGx9XG4gICAgICogICBUaGUgcmVzdWx0aW5nIGludGVyc2VjdGlvbiBpZiB0aGVyZSB3YXMgb25lLCBvdGhlcndpc2UgTlVMTC5cbiAgICAgKi9cbiAgICBnZXRJbnRlcnNlY3Rpb24ocmVjdCkge1xuICAgICAgY29uc3QgbzEgPSByZWN0LmdldFBvc2l0aW9uKCk7XG4gICAgICBjb25zdCBvMiA9IHRoaXMuZ2V0UG9zaXRpb24oKTtcblxuICAgICAgY29uc3QgeCA9IE1hdGgubWF4KG8xLmxlZnQsIG8yLmxlZnQpO1xuICAgICAgY29uc3QgeSA9IE1hdGgubWF4KG8xLnRvcCwgbzIudG9wKTtcbiAgICAgIGNvbnN0IHIgPSBNYXRoLm1pbihvMS5sZWZ0ICsgcmVjdC5nZXRXaWR0aCgpLCBvMi5sZWZ0ICsgdGhpcy5nZXRXaWR0aCgpKTtcbiAgICAgIGNvbnN0IGIgPSBNYXRoLm1pbihvMS50b3AgKyByZWN0LmdldEhlaWdodCgpLCBvMi50b3AgKyB0aGlzLmdldEhlaWdodCgpKTtcblxuICAgICAgLy8gQ2hlY2sgdGhhdCB0aGlzIHBvaW50IGlzIGluIHRoZSByZWN0YW5nbGUuXG4gICAgICByZXR1cm4gKHggPiByIHx8IHkgPiBiKSA/IG51bGwgOiBuZXcgUmVjdCh4LCB5LCByLCBiKTtcbiAgICB9XG5cbiAgICAvKipcbiAgICAgKiBUZXN0IGlmIGEgcmVjdGFuZ2xlIGlzIGNvbXBsZXRlbHkgaW5zaWRlIHRoaXMgcmVjdGFuZ2xlLlxuICAgICAqXG4gICAgICogQHBhcmFtIHtSZWN0fSByZWN0XG4gICAgICogICBSZWN0YW5nbGUgdG8gdGVzdCBpZiBpdCB3YXMgY29udGFpbmVkIGluc2lkZSBvZi5cbiAgICAgKlxuICAgICAqIEByZXR1cm4ge2Jvb2x9XG4gICAgICogICBUcnVlIElGRiB0aGUgcmVjdCBwYXJhbWV0ZXIgcmVwcmVzZW50cyBhbmQgYXJlYSB0aGF0IGlzIGNvbXBsZXRlbHlcbiAgICAgKiAgIGluc2lkZSB0aGlzIFJlY3QgaW5zdGFuY2UuXG4gICAgICovXG4gICAgY29udGFpbnMocmVjdCkge1xuICAgICAgcmV0dXJuIHJlY3QubGVmdCA+PSB0aGlzLmxlZnQgJiYgcmVjdC5yaWdodCA8PSB0aGlzLnJpZ2h0XG4gICAgICAgICYmIHJlY3QudG9wID49IHRoaXMudG9wICYmIHJlY3QuYm90dG9tIDw9IHRoaXMuYm90dG9tO1xuICAgIH1cbiAgfVxuXG4gIC8vIEV4cG9ydCB0aGUgY2xhc3NlcyB0byB0aGUgVG9vbHNoZWQgbmFtZXNwYWNlLlxuICB0cy5HZW9tLkVkZ2VzID0gRWRnZXM7XG4gIHRzLkdlb20uRGlyZWN0aW9uID0gRGlyZWN0aW9uO1xuICB0cy5HZW9tLlJlY3QgPSBSZWN0O1xufSkoRHJ1cGFsKTtcbiJdLCJtYXBwaW5ncyI6Ijs7QUFBQTtBQUNBLENBQUMsQ0FBQztFQUFFQSxRQUFRLEVBQUVDO0FBQUcsQ0FBQyxLQUFLO0VBQ3JCQSxFQUFFLENBQUNDLElBQUksR0FBRyxDQUFDLENBQUM7O0VBRVo7QUFDRjtBQUNBO0FBQ0E7QUFDQTtFQUNFLE1BQU1DLEtBQUssQ0FBQztJQUNWLFdBQVdDLEdBQUcsR0FBRztNQUNmLE9BQU8sSUFBSTtJQUNiO0lBRUEsV0FBV0MsTUFBTSxHQUFHO01BQ2xCLE9BQU8sSUFBSTtJQUNiO0lBRUEsV0FBV0MsSUFBSSxHQUFHO01BQ2hCLE9BQU8sSUFBSTtJQUNiO0lBRUEsV0FBV0MsS0FBSyxHQUFHO01BQ2pCLE9BQU8sSUFBSTtJQUNiO0VBQ0Y7O0VBRUE7QUFDRjtBQUNBO0FBQ0E7QUFDQTtFQUNFLE1BQU1DLFNBQVMsQ0FBQztJQUNkLFdBQVdDLFFBQVEsR0FBRztNQUNwQixPQUFPTixLQUFLLENBQUNDLEdBQUcsR0FBR0QsS0FBSyxDQUFDRSxNQUFNO0lBQ2pDO0lBRUEsV0FBV0ssVUFBVSxHQUFHO01BQ3RCLE9BQU9QLEtBQUssQ0FBQ0csSUFBSSxHQUFHSCxLQUFLLENBQUNJLEtBQUs7SUFDakM7SUFFQSxXQUFXSSxHQUFHLEdBQUc7TUFDZixPQUFPSCxTQUFTLENBQUNFLFVBQVUsR0FBR0YsU0FBUyxDQUFDQyxRQUFRO0lBQ2xEO0VBQ0Y7O0VBRUE7QUFDRjtBQUNBO0VBQ0UsTUFBTUcsSUFBSSxDQUFDO0lBQ1Q7QUFDSjtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7SUFDSSxPQUFPQyxXQUFXLENBQUNDLEVBQUUsRUFBRTtNQUNyQixNQUFNQyxDQUFDLEdBQUdELEVBQUUsQ0FBQ0UscUJBQXFCLEVBQUU7TUFDcEMsT0FBTyxJQUFJSixJQUFJLENBQUNHLENBQUMsQ0FBQ0UsSUFBSSxFQUFFRixDQUFDLENBQUNHLEdBQUcsRUFBRUgsQ0FBQyxDQUFDSSxLQUFLLEVBQUVKLENBQUMsQ0FBQ0ssTUFBTSxDQUFDO0lBQ25EOztJQUVBO0FBQ0o7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7SUFDSUMsV0FBVyxDQUFDSixJQUFJLEVBQUVDLEdBQUcsRUFBRUMsS0FBSyxFQUFFQyxNQUFNLEVBQUU7TUFDcEMsSUFBSUgsSUFBSSxZQUFZaEIsRUFBRSxDQUFDQyxJQUFJLENBQUNVLElBQUksRUFBRTtRQUNoQ1UsTUFBTSxDQUFDQyxNQUFNLENBQUMsSUFBSSxFQUFFTixJQUFJLENBQUM7TUFDM0IsQ0FBQyxNQUNJO1FBQ0gsSUFBSUMsR0FBRyxJQUFJRSxNQUFNLEVBQUU7VUFDakIsSUFBSSxDQUFDRixHQUFHLEdBQUdBLEdBQUc7VUFDZCxJQUFJLENBQUNFLE1BQU0sR0FBR0EsTUFBTTtRQUN0QixDQUFDLE1BQ0k7VUFDSCxJQUFJLENBQUNGLEdBQUcsR0FBR0UsTUFBTTtVQUNqQixJQUFJLENBQUNBLE1BQU0sR0FBR0YsR0FBRztRQUNuQjtRQUVBLElBQUlELElBQUksSUFBSUUsS0FBSyxFQUFFO1VBQ2pCLElBQUksQ0FBQ0YsSUFBSSxHQUFHQSxJQUFJO1VBQ2hCLElBQUksQ0FBQ0UsS0FBSyxHQUFHQSxLQUFLO1FBQ3BCLENBQUMsTUFDSTtVQUNILElBQUksQ0FBQ0YsSUFBSSxHQUFHRSxLQUFLO1VBQ2pCLElBQUksQ0FBQ0EsS0FBSyxHQUFHRixJQUFJO1FBQ25CO01BQ0Y7SUFDRjs7SUFFQTtBQUNKO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7SUFDSU8sV0FBVyxHQUFHO01BQ1osT0FBTztRQUFFUCxJQUFJLEVBQUUsSUFBSSxDQUFDQSxJQUFJO1FBQUVDLEdBQUcsRUFBRSxJQUFJLENBQUNBO01BQUksQ0FBQztJQUMzQzs7SUFFQTtBQUNKO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7SUFDSU8sUUFBUSxHQUFHO01BQ1QsT0FBTyxJQUFJLENBQUNOLEtBQUssR0FBRyxJQUFJLENBQUNGLElBQUk7SUFDL0I7O0lBRUE7QUFDSjtBQUNBO0FBQ0E7QUFDQTtBQUNBO0lBQ0lTLFNBQVMsR0FBRztNQUNWLE9BQU8sSUFBSSxDQUFDTixNQUFNLEdBQUcsSUFBSSxDQUFDRixHQUFHO0lBQy9COztJQUVBO0FBQ0o7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtJQUNJUyxRQUFRLENBQUNDLEVBQUUsRUFBRTtNQUNYLE1BQU1DLEdBQUcsR0FBRyxJQUFJLENBQUNMLFdBQVcsRUFBRTtNQUM5QkksRUFBRSxDQUFDWCxJQUFJLElBQUlZLEdBQUcsQ0FBQ1osSUFBSTtNQUNuQlcsRUFBRSxDQUFDVixHQUFHLElBQUlXLEdBQUcsQ0FBQ1gsR0FBRztNQUVqQixPQUFRVSxFQUFFLENBQUNYLElBQUksSUFBSSxDQUFDLElBQUlXLEVBQUUsQ0FBQ1gsSUFBSSxJQUFJLElBQUksQ0FBQ1EsUUFBUSxFQUFFLElBQzVDRyxFQUFFLENBQUNWLEdBQUcsSUFBSSxDQUFDLElBQUlVLEVBQUUsQ0FBQ1YsR0FBRyxJQUFJLElBQUksQ0FBQ1EsU0FBUyxFQUFHO0lBQ2xEOztJQUVBO0FBQ0o7QUFDQTtBQUNBO0FBQ0E7QUFDQTtJQUNJSSxPQUFPLEdBQUc7TUFDUixPQUFPLElBQUksQ0FBQ0wsUUFBUSxFQUFFLEdBQUcsSUFBSSxDQUFDQyxTQUFTLEVBQUU7SUFDM0M7O0lBRUE7QUFDSjtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtJQUNJSyxNQUFNLENBQUNDLE9BQU8sRUFBRUMsT0FBTyxFQUFFO01BQ3ZCLElBQUksQ0FBQ2YsR0FBRyxJQUFJZSxPQUFPO01BQ25CLElBQUksQ0FBQ2IsTUFBTSxJQUFJYSxPQUFPO01BQ3RCLElBQUksQ0FBQ2hCLElBQUksSUFBSWUsT0FBTztNQUNwQixJQUFJLENBQUNiLEtBQUssSUFBSWEsT0FBTztJQUN2Qjs7SUFFQTtBQUNKO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7SUFDSUUsZUFBZSxDQUFDQyxJQUFJLEVBQUU7TUFDcEIsTUFBTUMsRUFBRSxHQUFHRCxJQUFJLENBQUNYLFdBQVcsRUFBRTtNQUM3QixNQUFNYSxFQUFFLEdBQUcsSUFBSSxDQUFDYixXQUFXLEVBQUU7TUFFN0IsTUFBTWMsQ0FBQyxHQUFHQyxJQUFJLENBQUNDLEdBQUcsQ0FBQ0osRUFBRSxDQUFDbkIsSUFBSSxFQUFFb0IsRUFBRSxDQUFDcEIsSUFBSSxDQUFDO01BQ3BDLE1BQU13QixDQUFDLEdBQUdGLElBQUksQ0FBQ0MsR0FBRyxDQUFDSixFQUFFLENBQUNsQixHQUFHLEVBQUVtQixFQUFFLENBQUNuQixHQUFHLENBQUM7TUFDbEMsTUFBTXdCLENBQUMsR0FBR0gsSUFBSSxDQUFDSSxHQUFHLENBQUNQLEVBQUUsQ0FBQ25CLElBQUksR0FBR2tCLElBQUksQ0FBQ1YsUUFBUSxFQUFFLEVBQUVZLEVBQUUsQ0FBQ3BCLElBQUksR0FBRyxJQUFJLENBQUNRLFFBQVEsRUFBRSxDQUFDO01BQ3hFLE1BQU1WLENBQUMsR0FBR3dCLElBQUksQ0FBQ0ksR0FBRyxDQUFDUCxFQUFFLENBQUNsQixHQUFHLEdBQUdpQixJQUFJLENBQUNULFNBQVMsRUFBRSxFQUFFVyxFQUFFLENBQUNuQixHQUFHLEdBQUcsSUFBSSxDQUFDUSxTQUFTLEVBQUUsQ0FBQzs7TUFFeEU7TUFDQSxPQUFRWSxDQUFDLEdBQUdJLENBQUMsSUFBSUQsQ0FBQyxHQUFHMUIsQ0FBQyxHQUFJLElBQUksR0FBRyxJQUFJSCxJQUFJLENBQUMwQixDQUFDLEVBQUVHLENBQUMsRUFBRUMsQ0FBQyxFQUFFM0IsQ0FBQyxDQUFDO0lBQ3ZEOztJQUVBO0FBQ0o7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0lBQ0k2QixRQUFRLENBQUNULElBQUksRUFBRTtNQUNiLE9BQU9BLElBQUksQ0FBQ2xCLElBQUksSUFBSSxJQUFJLENBQUNBLElBQUksSUFBSWtCLElBQUksQ0FBQ2hCLEtBQUssSUFBSSxJQUFJLENBQUNBLEtBQUssSUFDcERnQixJQUFJLENBQUNqQixHQUFHLElBQUksSUFBSSxDQUFDQSxHQUFHLElBQUlpQixJQUFJLENBQUNmLE1BQU0sSUFBSSxJQUFJLENBQUNBLE1BQU07SUFDekQ7RUFDRjs7RUFFQTtFQUNBbkIsRUFBRSxDQUFDQyxJQUFJLENBQUNDLEtBQUssR0FBR0EsS0FBSztFQUNyQkYsRUFBRSxDQUFDQyxJQUFJLENBQUNNLFNBQVMsR0FBR0EsU0FBUztFQUM3QlAsRUFBRSxDQUFDQyxJQUFJLENBQUNVLElBQUksR0FBR0EsSUFBSTtBQUNyQixDQUFDLEVBQUVpQyxNQUFNLENBQUMifQ==
