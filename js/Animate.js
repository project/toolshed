"use strict";

/**
 * Add animation methods to the Element wrapper class.
 */
(({
  Toolshed: ts
}) => {
  /**
   * Element has completed its CSS transition.
   *
   * @param {TransitionEvent} e
   *   The event object with the transition information.
   */
  function onTransitionEnd(e) {
    const prop = e.propertyName;
    if (prop === 'height' || prop === 'width') {
      const {
        style
      } = this;
      if (style[prop] === '0px') {
        style.display = 'none';
      } else {
        style.display = '';
        style.boxSizing = '';
        style[prop] = '';
      }
      style.overflow = '';
      style.transition = '';

      // Update statuses and check for any pending animations.
      this.isAnimating = false;
      const item = this.animateQueue.pop();
      if (item) {
        item[0].call(this, item[1]);
      }
    }
  }
  ;

  // Capture the current destructor to call in our override.
  const destroy = ts.Element.prototype.destroy;

  // Apply basic animation methods to the Element prototype.
  Object.assign(ts.Element.prototype, {
    destroy(detach) {
      // Terminate any in progress animations.
      if (this.isAnimating && this.el.Animation) this.el.Animation.cancel();
      if (this.animateQueue) delete this.animateQueue;

      // Call original destructor.
      destroy.call(this, detach);
    },
    /**
     * Collapse the height of the element (animate element close vertically).
     *
     * @param {float} duration A time in seconds for the transition to take place.
     */
    collapse(duration = '0.5s') {
      if (this.animating) this._queueAnimation(this.collapse, duration);else {
        // Capture the element height before starting the animation.
        const height = this.el.clientHeight;
        this._animate({
          // Browsers are not able to perform transitions on elements with
          // auto-height, because the browser doesn't have a value to transition
          // between. Set the current height, wait for the an animation frame
          // from the change to take effect and then set the transition.
          height: `${height}px`,
          boxSizing: 'border-box',
          transition: 'none'
        }, {
          height: '0px',
          paddingTop: '0px',
          paddingBottom: '0px',
          overflow: 'hidden'
        }, state => {
          this.style.transition = `height ${duration}, padding ${duration}`;
          return state;
        });
      }
    },
    /**
     * Expand the height of the element (animate element open vertically).
     *
     * @param {float} duration A time in seconds for the transition to take place.
     */
    expand(duration = '0.5s') {
      if (this.animating) this._queueAnimation(this.expand, duration);else {
        this._animate({
          display: null,
          boxSizing: 'border-box',
          height: '0px',
          paddingTop: '0px',
          paddingBottom: '0px',
          transition: `height ${duration}, padding ${duration}`,
          overflow: 'hidden'
        }, {
          height: `50px`,
          paddingTop: null,
          paddingBottom: null
        }, state => {
          state.height = `${this.el.scrollHeight || 50}px`;
          return state;
        });
      }
    },
    /**
     * Collapse the width of the element (animate element close horizontally).
     *
     * @param {float} duration A time in seconds for the transition to take place.
     */
    slideOut(duration = '0.5s') {
      if (this.animating) this._queueAnimation(this.slideOut, duration);else {
        // Capture the element height before starting the animation.
        const width = this.el.clientWidth;
        this._animate({
          // Browsers are not able to perform transitions on elements with
          // auto-width, because the browser doesn't have a value to
          // transition to. Set the current height, wait for the an animation
          // frame from the change to take effect and then set the transition.
          width: `${width}px`,
          boxSizing: 'border-box',
          transition: 'none'
        }, {
          width: '0px',
          paddingLeft: '0px',
          paddingRight: '0px',
          overflow: 'hidden'
        }, state => {
          this.style.transition = `width ${duration}, padding ${duration}`;
          return state;
        });
      }
    },
    /**
     * Expand the width of the element (animate element open horizontally).
     *
     * @param {float} duration A time in seconds for the transition to take place.
     */
    slideIn(duration = '0.5s') {
      if (this.animating) this._queueAnimation(this.slideIn, duration);else {
        this._animate({
          display: null,
          boxSizing: 'border-box',
          width: '0px',
          paddingLeft: '0px',
          paddingRight: '0px',
          transition: `width ${duration}, padding ${duration}`,
          overflow: 'hidden'
        }, {
          width: `50px`,
          paddingLeft: '',
          paddingRight: ''
        }, state => {
          state.width = `${this.el.scrollWidth || 50}px`;
          return state;
        });
      }
    },
    /**
     * Initiate an animation sequence.
     *
     * @param {Object} init
     *   Initial states and styles to apply before starting the animation frames.
     * @param {Object} end
     *   The ending states and styles which complete the transition.
     * @param {function|closure=} transition
     *   An optional interframe callback to make adjustments to the ending
     *   transition states and alterations.
     */
    _animate(init, end, transition) {
      this.isAnimating = true;
      this._initAnimation();
      this.setStyles(init);
      requestAnimationFrame(() => {
        if (transition) {
          end = transition(end);
        }
        requestAnimationFrame(() => this.setStyles(end));
      });
    },
    /**
     * Initialize the element before the first animation is run.
     */
    _initAnimation() {
      if (!this.animateQueue) {
        this.animateQueue = [];
        this.on('transitionend', onTransitionEnd.bind(this));
      }
    },
    /**
     * Queue animations if pending animations are already in progress.
     *
     * @param {function|closure} method
     *   Animation method to add to the animation queue.
     * @param {string} duration
     *   The CSS transition time in seconds (ie "0.3s").
     */
    _queueAnimation(method, duration) {
      this._initAnimation();
      this.animateQueue.push([method, duration]);
    }
  });
})(Drupal);
//# sourceMappingURL=data:application/json;charset=utf8;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoiQW5pbWF0ZS5qcyIsIm5hbWVzIjpbIlRvb2xzaGVkIiwidHMiLCJvblRyYW5zaXRpb25FbmQiLCJlIiwicHJvcCIsInByb3BlcnR5TmFtZSIsInN0eWxlIiwiZGlzcGxheSIsImJveFNpemluZyIsIm92ZXJmbG93IiwidHJhbnNpdGlvbiIsImlzQW5pbWF0aW5nIiwiaXRlbSIsImFuaW1hdGVRdWV1ZSIsInBvcCIsImNhbGwiLCJkZXN0cm95IiwiRWxlbWVudCIsInByb3RvdHlwZSIsIk9iamVjdCIsImFzc2lnbiIsImRldGFjaCIsImVsIiwiQW5pbWF0aW9uIiwiY2FuY2VsIiwiY29sbGFwc2UiLCJkdXJhdGlvbiIsImFuaW1hdGluZyIsIl9xdWV1ZUFuaW1hdGlvbiIsImhlaWdodCIsImNsaWVudEhlaWdodCIsIl9hbmltYXRlIiwicGFkZGluZ1RvcCIsInBhZGRpbmdCb3R0b20iLCJzdGF0ZSIsImV4cGFuZCIsInNjcm9sbEhlaWdodCIsInNsaWRlT3V0Iiwid2lkdGgiLCJjbGllbnRXaWR0aCIsInBhZGRpbmdMZWZ0IiwicGFkZGluZ1JpZ2h0Iiwic2xpZGVJbiIsInNjcm9sbFdpZHRoIiwiaW5pdCIsImVuZCIsIl9pbml0QW5pbWF0aW9uIiwic2V0U3R5bGVzIiwicmVxdWVzdEFuaW1hdGlvbkZyYW1lIiwib24iLCJiaW5kIiwibWV0aG9kIiwicHVzaCIsIkRydXBhbCJdLCJzb3VyY2VzIjpbIkFuaW1hdGUuZXM2LmpzIl0sInNvdXJjZXNDb250ZW50IjpbIi8qKlxuICogQWRkIGFuaW1hdGlvbiBtZXRob2RzIHRvIHRoZSBFbGVtZW50IHdyYXBwZXIgY2xhc3MuXG4gKi9cbigoeyBUb29sc2hlZDogdHMgfSkgPT4ge1xuICAvKipcbiAgICogRWxlbWVudCBoYXMgY29tcGxldGVkIGl0cyBDU1MgdHJhbnNpdGlvbi5cbiAgICpcbiAgICogQHBhcmFtIHtUcmFuc2l0aW9uRXZlbnR9IGVcbiAgICogICBUaGUgZXZlbnQgb2JqZWN0IHdpdGggdGhlIHRyYW5zaXRpb24gaW5mb3JtYXRpb24uXG4gICAqL1xuICBmdW5jdGlvbiBvblRyYW5zaXRpb25FbmQoZSkge1xuICAgIGNvbnN0IHByb3AgPSBlLnByb3BlcnR5TmFtZTtcbiAgICBpZiAocHJvcCA9PT0gJ2hlaWdodCcgfHwgcHJvcCA9PT0gJ3dpZHRoJykge1xuICAgICAgY29uc3QgeyBzdHlsZSB9ID0gdGhpcztcblxuICAgICAgaWYgKHN0eWxlW3Byb3BdID09PSAnMHB4Jykge1xuICAgICAgICBzdHlsZS5kaXNwbGF5ID0gJ25vbmUnO1xuICAgICAgfVxuICAgICAgZWxzZSB7XG4gICAgICAgIHN0eWxlLmRpc3BsYXkgPSAnJztcbiAgICAgICAgc3R5bGUuYm94U2l6aW5nID0gJyc7XG4gICAgICAgIHN0eWxlW3Byb3BdID0gJyc7XG4gICAgICB9XG5cbiAgICAgIHN0eWxlLm92ZXJmbG93ID0gJyc7XG4gICAgICBzdHlsZS50cmFuc2l0aW9uID0gJyc7XG5cbiAgICAgIC8vIFVwZGF0ZSBzdGF0dXNlcyBhbmQgY2hlY2sgZm9yIGFueSBwZW5kaW5nIGFuaW1hdGlvbnMuXG4gICAgICB0aGlzLmlzQW5pbWF0aW5nID0gZmFsc2U7XG4gICAgICBjb25zdCBpdGVtID0gdGhpcy5hbmltYXRlUXVldWUucG9wKCk7XG4gICAgICBpZiAoaXRlbSkge1xuICAgICAgICBpdGVtWzBdLmNhbGwodGhpcywgaXRlbVsxXSk7XG4gICAgICB9XG4gICAgfVxuICB9O1xuXG4gIC8vIENhcHR1cmUgdGhlIGN1cnJlbnQgZGVzdHJ1Y3RvciB0byBjYWxsIGluIG91ciBvdmVycmlkZS5cbiAgY29uc3QgZGVzdHJveSA9IHRzLkVsZW1lbnQucHJvdG90eXBlLmRlc3Ryb3k7XG5cbiAgLy8gQXBwbHkgYmFzaWMgYW5pbWF0aW9uIG1ldGhvZHMgdG8gdGhlIEVsZW1lbnQgcHJvdG90eXBlLlxuICBPYmplY3QuYXNzaWduKHRzLkVsZW1lbnQucHJvdG90eXBlLCB7XG4gICAgZGVzdHJveShkZXRhY2gpIHtcbiAgICAgIC8vIFRlcm1pbmF0ZSBhbnkgaW4gcHJvZ3Jlc3MgYW5pbWF0aW9ucy5cbiAgICAgIGlmICh0aGlzLmlzQW5pbWF0aW5nICYmIHRoaXMuZWwuQW5pbWF0aW9uKSB0aGlzLmVsLkFuaW1hdGlvbi5jYW5jZWwoKTtcbiAgICAgIGlmICh0aGlzLmFuaW1hdGVRdWV1ZSkgZGVsZXRlIHRoaXMuYW5pbWF0ZVF1ZXVlO1xuXG4gICAgICAvLyBDYWxsIG9yaWdpbmFsIGRlc3RydWN0b3IuXG4gICAgICBkZXN0cm95LmNhbGwodGhpcywgZGV0YWNoKTtcbiAgICB9LFxuXG4gICAgLyoqXG4gICAgICogQ29sbGFwc2UgdGhlIGhlaWdodCBvZiB0aGUgZWxlbWVudCAoYW5pbWF0ZSBlbGVtZW50IGNsb3NlIHZlcnRpY2FsbHkpLlxuICAgICAqXG4gICAgICogQHBhcmFtIHtmbG9hdH0gZHVyYXRpb24gQSB0aW1lIGluIHNlY29uZHMgZm9yIHRoZSB0cmFuc2l0aW9uIHRvIHRha2UgcGxhY2UuXG4gICAgICovXG4gICAgY29sbGFwc2UoZHVyYXRpb24gPSAnMC41cycpIHtcbiAgICAgIGlmICh0aGlzLmFuaW1hdGluZykgdGhpcy5fcXVldWVBbmltYXRpb24odGhpcy5jb2xsYXBzZSwgZHVyYXRpb24pO1xuICAgICAgZWxzZSB7XG4gICAgICAgIC8vIENhcHR1cmUgdGhlIGVsZW1lbnQgaGVpZ2h0IGJlZm9yZSBzdGFydGluZyB0aGUgYW5pbWF0aW9uLlxuICAgICAgICBjb25zdCBoZWlnaHQgPSB0aGlzLmVsLmNsaWVudEhlaWdodDtcbiAgICAgICAgdGhpcy5fYW5pbWF0ZShcbiAgICAgICAgICB7XG4gICAgICAgICAgICAvLyBCcm93c2VycyBhcmUgbm90IGFibGUgdG8gcGVyZm9ybSB0cmFuc2l0aW9ucyBvbiBlbGVtZW50cyB3aXRoXG4gICAgICAgICAgICAvLyBhdXRvLWhlaWdodCwgYmVjYXVzZSB0aGUgYnJvd3NlciBkb2Vzbid0IGhhdmUgYSB2YWx1ZSB0byB0cmFuc2l0aW9uXG4gICAgICAgICAgICAvLyBiZXR3ZWVuLiBTZXQgdGhlIGN1cnJlbnQgaGVpZ2h0LCB3YWl0IGZvciB0aGUgYW4gYW5pbWF0aW9uIGZyYW1lXG4gICAgICAgICAgICAvLyBmcm9tIHRoZSBjaGFuZ2UgdG8gdGFrZSBlZmZlY3QgYW5kIHRoZW4gc2V0IHRoZSB0cmFuc2l0aW9uLlxuICAgICAgICAgICAgaGVpZ2h0OiBgJHtoZWlnaHR9cHhgLFxuICAgICAgICAgICAgYm94U2l6aW5nOiAnYm9yZGVyLWJveCcsXG4gICAgICAgICAgICB0cmFuc2l0aW9uOiAnbm9uZScsXG4gICAgICAgICAgfSxcbiAgICAgICAgICB7XG4gICAgICAgICAgICBoZWlnaHQ6ICcwcHgnLFxuICAgICAgICAgICAgcGFkZGluZ1RvcDogJzBweCcsXG4gICAgICAgICAgICBwYWRkaW5nQm90dG9tOiAnMHB4JyxcbiAgICAgICAgICAgIG92ZXJmbG93OiAnaGlkZGVuJyxcbiAgICAgICAgICB9LFxuICAgICAgICAgIChzdGF0ZSkgPT4ge1xuICAgICAgICAgICAgdGhpcy5zdHlsZS50cmFuc2l0aW9uID0gYGhlaWdodCAke2R1cmF0aW9ufSwgcGFkZGluZyAke2R1cmF0aW9ufWA7XG4gICAgICAgICAgICByZXR1cm4gc3RhdGU7XG4gICAgICAgICAgfVxuICAgICAgICApO1xuICAgICAgfVxuICAgIH0sXG5cbiAgICAvKipcbiAgICAgKiBFeHBhbmQgdGhlIGhlaWdodCBvZiB0aGUgZWxlbWVudCAoYW5pbWF0ZSBlbGVtZW50IG9wZW4gdmVydGljYWxseSkuXG4gICAgICpcbiAgICAgKiBAcGFyYW0ge2Zsb2F0fSBkdXJhdGlvbiBBIHRpbWUgaW4gc2Vjb25kcyBmb3IgdGhlIHRyYW5zaXRpb24gdG8gdGFrZSBwbGFjZS5cbiAgICAgKi9cbiAgICBleHBhbmQoZHVyYXRpb24gPSAnMC41cycpIHtcbiAgICAgIGlmICh0aGlzLmFuaW1hdGluZykgdGhpcy5fcXVldWVBbmltYXRpb24odGhpcy5leHBhbmQsIGR1cmF0aW9uKTtcbiAgICAgIGVsc2Uge1xuICAgICAgICB0aGlzLl9hbmltYXRlKFxuICAgICAgICAgIHtcbiAgICAgICAgICAgIGRpc3BsYXk6IG51bGwsXG4gICAgICAgICAgICBib3hTaXppbmc6ICdib3JkZXItYm94JyxcbiAgICAgICAgICAgIGhlaWdodDogJzBweCcsXG4gICAgICAgICAgICBwYWRkaW5nVG9wOiAnMHB4JyxcbiAgICAgICAgICAgIHBhZGRpbmdCb3R0b206ICcwcHgnLFxuICAgICAgICAgICAgdHJhbnNpdGlvbjogYGhlaWdodCAke2R1cmF0aW9ufSwgcGFkZGluZyAke2R1cmF0aW9ufWAsXG4gICAgICAgICAgICBvdmVyZmxvdzogJ2hpZGRlbicsXG4gICAgICAgICAgfSxcbiAgICAgICAgICB7XG4gICAgICAgICAgICBoZWlnaHQ6IGA1MHB4YCxcbiAgICAgICAgICAgIHBhZGRpbmdUb3A6IG51bGwsXG4gICAgICAgICAgICBwYWRkaW5nQm90dG9tOiBudWxsLFxuICAgICAgICAgIH0sXG4gICAgICAgICAgKHN0YXRlKSA9PiB7XG4gICAgICAgICAgICBzdGF0ZS5oZWlnaHQgPSBgJHt0aGlzLmVsLnNjcm9sbEhlaWdodHx8NTB9cHhgO1xuICAgICAgICAgICAgcmV0dXJuIHN0YXRlO1xuICAgICAgICAgIH1cbiAgICAgICAgKTtcbiAgICAgIH1cbiAgICB9LFxuXG4gICAgLyoqXG4gICAgICogQ29sbGFwc2UgdGhlIHdpZHRoIG9mIHRoZSBlbGVtZW50IChhbmltYXRlIGVsZW1lbnQgY2xvc2UgaG9yaXpvbnRhbGx5KS5cbiAgICAgKlxuICAgICAqIEBwYXJhbSB7ZmxvYXR9IGR1cmF0aW9uIEEgdGltZSBpbiBzZWNvbmRzIGZvciB0aGUgdHJhbnNpdGlvbiB0byB0YWtlIHBsYWNlLlxuICAgICAqL1xuICAgIHNsaWRlT3V0KGR1cmF0aW9uID0gJzAuNXMnKSB7XG4gICAgICBpZiAodGhpcy5hbmltYXRpbmcpIHRoaXMuX3F1ZXVlQW5pbWF0aW9uKHRoaXMuc2xpZGVPdXQsIGR1cmF0aW9uKTtcbiAgICAgIGVsc2Uge1xuICAgICAgICAvLyBDYXB0dXJlIHRoZSBlbGVtZW50IGhlaWdodCBiZWZvcmUgc3RhcnRpbmcgdGhlIGFuaW1hdGlvbi5cbiAgICAgICAgY29uc3Qgd2lkdGggPSB0aGlzLmVsLmNsaWVudFdpZHRoO1xuICAgICAgICB0aGlzLl9hbmltYXRlKFxuICAgICAgICAgIHtcbiAgICAgICAgICAgIC8vIEJyb3dzZXJzIGFyZSBub3QgYWJsZSB0byBwZXJmb3JtIHRyYW5zaXRpb25zIG9uIGVsZW1lbnRzIHdpdGhcbiAgICAgICAgICAgIC8vIGF1dG8td2lkdGgsIGJlY2F1c2UgdGhlIGJyb3dzZXIgZG9lc24ndCBoYXZlIGEgdmFsdWUgdG9cbiAgICAgICAgICAgIC8vIHRyYW5zaXRpb24gdG8uIFNldCB0aGUgY3VycmVudCBoZWlnaHQsIHdhaXQgZm9yIHRoZSBhbiBhbmltYXRpb25cbiAgICAgICAgICAgIC8vIGZyYW1lIGZyb20gdGhlIGNoYW5nZSB0byB0YWtlIGVmZmVjdCBhbmQgdGhlbiBzZXQgdGhlIHRyYW5zaXRpb24uXG4gICAgICAgICAgICB3aWR0aDogYCR7d2lkdGh9cHhgLFxuICAgICAgICAgICAgYm94U2l6aW5nOiAnYm9yZGVyLWJveCcsXG4gICAgICAgICAgICB0cmFuc2l0aW9uOiAnbm9uZScsXG4gICAgICAgICAgfSxcbiAgICAgICAgICB7XG4gICAgICAgICAgICB3aWR0aDogJzBweCcsXG4gICAgICAgICAgICBwYWRkaW5nTGVmdDogJzBweCcsXG4gICAgICAgICAgICBwYWRkaW5nUmlnaHQ6ICcwcHgnLFxuICAgICAgICAgICAgb3ZlcmZsb3c6ICdoaWRkZW4nLFxuICAgICAgICAgIH0sXG4gICAgICAgICAgKHN0YXRlKSA9PiB7XG4gICAgICAgICAgICB0aGlzLnN0eWxlLnRyYW5zaXRpb24gPSBgd2lkdGggJHtkdXJhdGlvbn0sIHBhZGRpbmcgJHtkdXJhdGlvbn1gO1xuICAgICAgICAgICAgcmV0dXJuIHN0YXRlO1xuICAgICAgICAgIH1cbiAgICAgICAgKTtcbiAgICAgIH1cbiAgICB9LFxuXG4gICAgLyoqXG4gICAgICogRXhwYW5kIHRoZSB3aWR0aCBvZiB0aGUgZWxlbWVudCAoYW5pbWF0ZSBlbGVtZW50IG9wZW4gaG9yaXpvbnRhbGx5KS5cbiAgICAgKlxuICAgICAqIEBwYXJhbSB7ZmxvYXR9IGR1cmF0aW9uIEEgdGltZSBpbiBzZWNvbmRzIGZvciB0aGUgdHJhbnNpdGlvbiB0byB0YWtlIHBsYWNlLlxuICAgICAqL1xuICAgIHNsaWRlSW4oZHVyYXRpb24gPSAnMC41cycpIHtcbiAgICAgIGlmICh0aGlzLmFuaW1hdGluZykgdGhpcy5fcXVldWVBbmltYXRpb24odGhpcy5zbGlkZUluLCBkdXJhdGlvbik7XG5cbiAgICAgIGVsc2Uge1xuICAgICAgICB0aGlzLl9hbmltYXRlKFxuICAgICAgICAgIHtcbiAgICAgICAgICAgIGRpc3BsYXk6IG51bGwsXG4gICAgICAgICAgICBib3hTaXppbmc6ICdib3JkZXItYm94JyxcbiAgICAgICAgICAgIHdpZHRoOiAnMHB4JyxcbiAgICAgICAgICAgIHBhZGRpbmdMZWZ0OiAnMHB4JyxcbiAgICAgICAgICAgIHBhZGRpbmdSaWdodDogJzBweCcsXG4gICAgICAgICAgICB0cmFuc2l0aW9uOiBgd2lkdGggJHtkdXJhdGlvbn0sIHBhZGRpbmcgJHtkdXJhdGlvbn1gLFxuICAgICAgICAgICAgb3ZlcmZsb3c6ICdoaWRkZW4nLFxuICAgICAgICAgIH0sXG4gICAgICAgICAge1xuICAgICAgICAgICAgd2lkdGg6IGA1MHB4YCxcbiAgICAgICAgICAgIHBhZGRpbmdMZWZ0OiAnJyxcbiAgICAgICAgICAgIHBhZGRpbmdSaWdodDogJycsXG4gICAgICAgICAgfSxcbiAgICAgICAgICAoc3RhdGUpID0+IHtcbiAgICAgICAgICAgIHN0YXRlLndpZHRoID0gYCR7dGhpcy5lbC5zY3JvbGxXaWR0aHx8NTB9cHhgO1xuICAgICAgICAgICAgcmV0dXJuIHN0YXRlO1xuICAgICAgICAgIH1cbiAgICAgICAgKTtcbiAgICAgIH1cbiAgICB9LFxuXG4gICAgLyoqXG4gICAgICogSW5pdGlhdGUgYW4gYW5pbWF0aW9uIHNlcXVlbmNlLlxuICAgICAqXG4gICAgICogQHBhcmFtIHtPYmplY3R9IGluaXRcbiAgICAgKiAgIEluaXRpYWwgc3RhdGVzIGFuZCBzdHlsZXMgdG8gYXBwbHkgYmVmb3JlIHN0YXJ0aW5nIHRoZSBhbmltYXRpb24gZnJhbWVzLlxuICAgICAqIEBwYXJhbSB7T2JqZWN0fSBlbmRcbiAgICAgKiAgIFRoZSBlbmRpbmcgc3RhdGVzIGFuZCBzdHlsZXMgd2hpY2ggY29tcGxldGUgdGhlIHRyYW5zaXRpb24uXG4gICAgICogQHBhcmFtIHtmdW5jdGlvbnxjbG9zdXJlPX0gdHJhbnNpdGlvblxuICAgICAqICAgQW4gb3B0aW9uYWwgaW50ZXJmcmFtZSBjYWxsYmFjayB0byBtYWtlIGFkanVzdG1lbnRzIHRvIHRoZSBlbmRpbmdcbiAgICAgKiAgIHRyYW5zaXRpb24gc3RhdGVzIGFuZCBhbHRlcmF0aW9ucy5cbiAgICAgKi9cbiAgICBfYW5pbWF0ZShpbml0LCBlbmQsIHRyYW5zaXRpb24pIHtcbiAgICAgIHRoaXMuaXNBbmltYXRpbmcgPSB0cnVlO1xuICAgICAgdGhpcy5faW5pdEFuaW1hdGlvbigpO1xuICAgICAgdGhpcy5zZXRTdHlsZXMoaW5pdCk7XG5cbiAgICAgIHJlcXVlc3RBbmltYXRpb25GcmFtZSgoKSA9PiB7XG4gICAgICAgIGlmICh0cmFuc2l0aW9uKSB7XG4gICAgICAgICAgZW5kID0gdHJhbnNpdGlvbihlbmQpO1xuICAgICAgICB9XG5cbiAgICAgICAgcmVxdWVzdEFuaW1hdGlvbkZyYW1lKCgpID0+IHRoaXMuc2V0U3R5bGVzKGVuZCkpO1xuICAgICAgfSk7XG4gICAgfSxcblxuICAgIC8qKlxuICAgICAqIEluaXRpYWxpemUgdGhlIGVsZW1lbnQgYmVmb3JlIHRoZSBmaXJzdCBhbmltYXRpb24gaXMgcnVuLlxuICAgICAqL1xuICAgIF9pbml0QW5pbWF0aW9uKCkge1xuICAgICAgaWYgKCF0aGlzLmFuaW1hdGVRdWV1ZSkge1xuICAgICAgICB0aGlzLmFuaW1hdGVRdWV1ZSA9IFtdO1xuICAgICAgICB0aGlzLm9uKCd0cmFuc2l0aW9uZW5kJywgb25UcmFuc2l0aW9uRW5kLmJpbmQodGhpcykpO1xuICAgICAgfVxuICAgIH0sXG5cbiAgICAvKipcbiAgICAgKiBRdWV1ZSBhbmltYXRpb25zIGlmIHBlbmRpbmcgYW5pbWF0aW9ucyBhcmUgYWxyZWFkeSBpbiBwcm9ncmVzcy5cbiAgICAgKlxuICAgICAqIEBwYXJhbSB7ZnVuY3Rpb258Y2xvc3VyZX0gbWV0aG9kXG4gICAgICogICBBbmltYXRpb24gbWV0aG9kIHRvIGFkZCB0byB0aGUgYW5pbWF0aW9uIHF1ZXVlLlxuICAgICAqIEBwYXJhbSB7c3RyaW5nfSBkdXJhdGlvblxuICAgICAqICAgVGhlIENTUyB0cmFuc2l0aW9uIHRpbWUgaW4gc2Vjb25kcyAoaWUgXCIwLjNzXCIpLlxuICAgICAqL1xuICAgIF9xdWV1ZUFuaW1hdGlvbihtZXRob2QsIGR1cmF0aW9uKSB7XG4gICAgICB0aGlzLl9pbml0QW5pbWF0aW9uKCk7XG4gICAgICB0aGlzLmFuaW1hdGVRdWV1ZS5wdXNoKFttZXRob2QsIGR1cmF0aW9uXSk7XG4gICAgfSxcbiAgfSk7XG59KShEcnVwYWwpO1xuIl0sIm1hcHBpbmdzIjoiOztBQUFBO0FBQ0E7QUFDQTtBQUNBLENBQUMsQ0FBQztFQUFFQSxRQUFRLEVBQUVDO0FBQUcsQ0FBQyxLQUFLO0VBQ3JCO0FBQ0Y7QUFDQTtBQUNBO0FBQ0E7QUFDQTtFQUNFLFNBQVNDLGVBQWUsQ0FBQ0MsQ0FBQyxFQUFFO0lBQzFCLE1BQU1DLElBQUksR0FBR0QsQ0FBQyxDQUFDRSxZQUFZO0lBQzNCLElBQUlELElBQUksS0FBSyxRQUFRLElBQUlBLElBQUksS0FBSyxPQUFPLEVBQUU7TUFDekMsTUFBTTtRQUFFRTtNQUFNLENBQUMsR0FBRyxJQUFJO01BRXRCLElBQUlBLEtBQUssQ0FBQ0YsSUFBSSxDQUFDLEtBQUssS0FBSyxFQUFFO1FBQ3pCRSxLQUFLLENBQUNDLE9BQU8sR0FBRyxNQUFNO01BQ3hCLENBQUMsTUFDSTtRQUNIRCxLQUFLLENBQUNDLE9BQU8sR0FBRyxFQUFFO1FBQ2xCRCxLQUFLLENBQUNFLFNBQVMsR0FBRyxFQUFFO1FBQ3BCRixLQUFLLENBQUNGLElBQUksQ0FBQyxHQUFHLEVBQUU7TUFDbEI7TUFFQUUsS0FBSyxDQUFDRyxRQUFRLEdBQUcsRUFBRTtNQUNuQkgsS0FBSyxDQUFDSSxVQUFVLEdBQUcsRUFBRTs7TUFFckI7TUFDQSxJQUFJLENBQUNDLFdBQVcsR0FBRyxLQUFLO01BQ3hCLE1BQU1DLElBQUksR0FBRyxJQUFJLENBQUNDLFlBQVksQ0FBQ0MsR0FBRyxFQUFFO01BQ3BDLElBQUlGLElBQUksRUFBRTtRQUNSQSxJQUFJLENBQUMsQ0FBQyxDQUFDLENBQUNHLElBQUksQ0FBQyxJQUFJLEVBQUVILElBQUksQ0FBQyxDQUFDLENBQUMsQ0FBQztNQUM3QjtJQUNGO0VBQ0Y7RUFBQzs7RUFFRDtFQUNBLE1BQU1JLE9BQU8sR0FBR2YsRUFBRSxDQUFDZ0IsT0FBTyxDQUFDQyxTQUFTLENBQUNGLE9BQU87O0VBRTVDO0VBQ0FHLE1BQU0sQ0FBQ0MsTUFBTSxDQUFDbkIsRUFBRSxDQUFDZ0IsT0FBTyxDQUFDQyxTQUFTLEVBQUU7SUFDbENGLE9BQU8sQ0FBQ0ssTUFBTSxFQUFFO01BQ2Q7TUFDQSxJQUFJLElBQUksQ0FBQ1YsV0FBVyxJQUFJLElBQUksQ0FBQ1csRUFBRSxDQUFDQyxTQUFTLEVBQUUsSUFBSSxDQUFDRCxFQUFFLENBQUNDLFNBQVMsQ0FBQ0MsTUFBTSxFQUFFO01BQ3JFLElBQUksSUFBSSxDQUFDWCxZQUFZLEVBQUUsT0FBTyxJQUFJLENBQUNBLFlBQVk7O01BRS9DO01BQ0FHLE9BQU8sQ0FBQ0QsSUFBSSxDQUFDLElBQUksRUFBRU0sTUFBTSxDQUFDO0lBQzVCLENBQUM7SUFFRDtBQUNKO0FBQ0E7QUFDQTtBQUNBO0lBQ0lJLFFBQVEsQ0FBQ0MsUUFBUSxHQUFHLE1BQU0sRUFBRTtNQUMxQixJQUFJLElBQUksQ0FBQ0MsU0FBUyxFQUFFLElBQUksQ0FBQ0MsZUFBZSxDQUFDLElBQUksQ0FBQ0gsUUFBUSxFQUFFQyxRQUFRLENBQUMsQ0FBQyxLQUM3RDtRQUNIO1FBQ0EsTUFBTUcsTUFBTSxHQUFHLElBQUksQ0FBQ1AsRUFBRSxDQUFDUSxZQUFZO1FBQ25DLElBQUksQ0FBQ0MsUUFBUSxDQUNYO1VBQ0U7VUFDQTtVQUNBO1VBQ0E7VUFDQUYsTUFBTSxFQUFHLEdBQUVBLE1BQU8sSUFBRztVQUNyQnJCLFNBQVMsRUFBRSxZQUFZO1VBQ3ZCRSxVQUFVLEVBQUU7UUFDZCxDQUFDLEVBQ0Q7VUFDRW1CLE1BQU0sRUFBRSxLQUFLO1VBQ2JHLFVBQVUsRUFBRSxLQUFLO1VBQ2pCQyxhQUFhLEVBQUUsS0FBSztVQUNwQnhCLFFBQVEsRUFBRTtRQUNaLENBQUMsRUFDQXlCLEtBQUssSUFBSztVQUNULElBQUksQ0FBQzVCLEtBQUssQ0FBQ0ksVUFBVSxHQUFJLFVBQVNnQixRQUFTLGFBQVlBLFFBQVMsRUFBQztVQUNqRSxPQUFPUSxLQUFLO1FBQ2QsQ0FBQyxDQUNGO01BQ0g7SUFDRixDQUFDO0lBRUQ7QUFDSjtBQUNBO0FBQ0E7QUFDQTtJQUNJQyxNQUFNLENBQUNULFFBQVEsR0FBRyxNQUFNLEVBQUU7TUFDeEIsSUFBSSxJQUFJLENBQUNDLFNBQVMsRUFBRSxJQUFJLENBQUNDLGVBQWUsQ0FBQyxJQUFJLENBQUNPLE1BQU0sRUFBRVQsUUFBUSxDQUFDLENBQUMsS0FDM0Q7UUFDSCxJQUFJLENBQUNLLFFBQVEsQ0FDWDtVQUNFeEIsT0FBTyxFQUFFLElBQUk7VUFDYkMsU0FBUyxFQUFFLFlBQVk7VUFDdkJxQixNQUFNLEVBQUUsS0FBSztVQUNiRyxVQUFVLEVBQUUsS0FBSztVQUNqQkMsYUFBYSxFQUFFLEtBQUs7VUFDcEJ2QixVQUFVLEVBQUcsVUFBU2dCLFFBQVMsYUFBWUEsUUFBUyxFQUFDO1VBQ3JEakIsUUFBUSxFQUFFO1FBQ1osQ0FBQyxFQUNEO1VBQ0VvQixNQUFNLEVBQUcsTUFBSztVQUNkRyxVQUFVLEVBQUUsSUFBSTtVQUNoQkMsYUFBYSxFQUFFO1FBQ2pCLENBQUMsRUFDQUMsS0FBSyxJQUFLO1VBQ1RBLEtBQUssQ0FBQ0wsTUFBTSxHQUFJLEdBQUUsSUFBSSxDQUFDUCxFQUFFLENBQUNjLFlBQVksSUFBRSxFQUFHLElBQUc7VUFDOUMsT0FBT0YsS0FBSztRQUNkLENBQUMsQ0FDRjtNQUNIO0lBQ0YsQ0FBQztJQUVEO0FBQ0o7QUFDQTtBQUNBO0FBQ0E7SUFDSUcsUUFBUSxDQUFDWCxRQUFRLEdBQUcsTUFBTSxFQUFFO01BQzFCLElBQUksSUFBSSxDQUFDQyxTQUFTLEVBQUUsSUFBSSxDQUFDQyxlQUFlLENBQUMsSUFBSSxDQUFDUyxRQUFRLEVBQUVYLFFBQVEsQ0FBQyxDQUFDLEtBQzdEO1FBQ0g7UUFDQSxNQUFNWSxLQUFLLEdBQUcsSUFBSSxDQUFDaEIsRUFBRSxDQUFDaUIsV0FBVztRQUNqQyxJQUFJLENBQUNSLFFBQVEsQ0FDWDtVQUNFO1VBQ0E7VUFDQTtVQUNBO1VBQ0FPLEtBQUssRUFBRyxHQUFFQSxLQUFNLElBQUc7VUFDbkI5QixTQUFTLEVBQUUsWUFBWTtVQUN2QkUsVUFBVSxFQUFFO1FBQ2QsQ0FBQyxFQUNEO1VBQ0U0QixLQUFLLEVBQUUsS0FBSztVQUNaRSxXQUFXLEVBQUUsS0FBSztVQUNsQkMsWUFBWSxFQUFFLEtBQUs7VUFDbkJoQyxRQUFRLEVBQUU7UUFDWixDQUFDLEVBQ0F5QixLQUFLLElBQUs7VUFDVCxJQUFJLENBQUM1QixLQUFLLENBQUNJLFVBQVUsR0FBSSxTQUFRZ0IsUUFBUyxhQUFZQSxRQUFTLEVBQUM7VUFDaEUsT0FBT1EsS0FBSztRQUNkLENBQUMsQ0FDRjtNQUNIO0lBQ0YsQ0FBQztJQUVEO0FBQ0o7QUFDQTtBQUNBO0FBQ0E7SUFDSVEsT0FBTyxDQUFDaEIsUUFBUSxHQUFHLE1BQU0sRUFBRTtNQUN6QixJQUFJLElBQUksQ0FBQ0MsU0FBUyxFQUFFLElBQUksQ0FBQ0MsZUFBZSxDQUFDLElBQUksQ0FBQ2MsT0FBTyxFQUFFaEIsUUFBUSxDQUFDLENBQUMsS0FFNUQ7UUFDSCxJQUFJLENBQUNLLFFBQVEsQ0FDWDtVQUNFeEIsT0FBTyxFQUFFLElBQUk7VUFDYkMsU0FBUyxFQUFFLFlBQVk7VUFDdkI4QixLQUFLLEVBQUUsS0FBSztVQUNaRSxXQUFXLEVBQUUsS0FBSztVQUNsQkMsWUFBWSxFQUFFLEtBQUs7VUFDbkIvQixVQUFVLEVBQUcsU0FBUWdCLFFBQVMsYUFBWUEsUUFBUyxFQUFDO1VBQ3BEakIsUUFBUSxFQUFFO1FBQ1osQ0FBQyxFQUNEO1VBQ0U2QixLQUFLLEVBQUcsTUFBSztVQUNiRSxXQUFXLEVBQUUsRUFBRTtVQUNmQyxZQUFZLEVBQUU7UUFDaEIsQ0FBQyxFQUNBUCxLQUFLLElBQUs7VUFDVEEsS0FBSyxDQUFDSSxLQUFLLEdBQUksR0FBRSxJQUFJLENBQUNoQixFQUFFLENBQUNxQixXQUFXLElBQUUsRUFBRyxJQUFHO1VBQzVDLE9BQU9ULEtBQUs7UUFDZCxDQUFDLENBQ0Y7TUFDSDtJQUNGLENBQUM7SUFFRDtBQUNKO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0lBQ0lILFFBQVEsQ0FBQ2EsSUFBSSxFQUFFQyxHQUFHLEVBQUVuQyxVQUFVLEVBQUU7TUFDOUIsSUFBSSxDQUFDQyxXQUFXLEdBQUcsSUFBSTtNQUN2QixJQUFJLENBQUNtQyxjQUFjLEVBQUU7TUFDckIsSUFBSSxDQUFDQyxTQUFTLENBQUNILElBQUksQ0FBQztNQUVwQkkscUJBQXFCLENBQUMsTUFBTTtRQUMxQixJQUFJdEMsVUFBVSxFQUFFO1VBQ2RtQyxHQUFHLEdBQUduQyxVQUFVLENBQUNtQyxHQUFHLENBQUM7UUFDdkI7UUFFQUcscUJBQXFCLENBQUMsTUFBTSxJQUFJLENBQUNELFNBQVMsQ0FBQ0YsR0FBRyxDQUFDLENBQUM7TUFDbEQsQ0FBQyxDQUFDO0lBQ0osQ0FBQztJQUVEO0FBQ0o7QUFDQTtJQUNJQyxjQUFjLEdBQUc7TUFDZixJQUFJLENBQUMsSUFBSSxDQUFDakMsWUFBWSxFQUFFO1FBQ3RCLElBQUksQ0FBQ0EsWUFBWSxHQUFHLEVBQUU7UUFDdEIsSUFBSSxDQUFDb0MsRUFBRSxDQUFDLGVBQWUsRUFBRS9DLGVBQWUsQ0FBQ2dELElBQUksQ0FBQyxJQUFJLENBQUMsQ0FBQztNQUN0RDtJQUNGLENBQUM7SUFFRDtBQUNKO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0lBQ0l0QixlQUFlLENBQUN1QixNQUFNLEVBQUV6QixRQUFRLEVBQUU7TUFDaEMsSUFBSSxDQUFDb0IsY0FBYyxFQUFFO01BQ3JCLElBQUksQ0FBQ2pDLFlBQVksQ0FBQ3VDLElBQUksQ0FBQyxDQUFDRCxNQUFNLEVBQUV6QixRQUFRLENBQUMsQ0FBQztJQUM1QztFQUNGLENBQUMsQ0FBQztBQUNKLENBQUMsRUFBRTJCLE1BQU0sQ0FBQyJ9
