(({ behaviors, Toolshed: ts }) => {
  behaviors.toolshedAccordions = {
    accordions: new Map(),

    attach(context, settings) {
      ts.walkByClass(context, 'use-accordion', (accordion) => {
        this.accordions.set(accordion, new ts.Accordion(accordion, {
          initOpen: false,
          exclusive: false,
          itemSelector: '.accordion-item',
          bodySelector: '.accordion-item__body',
          ...(settings.Toolshed || {}).accordion,
        }));
      }, 'accordion--processed');
    },

    detach(context, settings, trigger) {
      if (trigger !== 'unload') return;

      ts.walkByClass(context, 'accordion--processed', (accordion) => {
        const obj = this.map.get(accordion);
        if (obj) obj.destroy();
      });
    },
  };
})(Drupal);
