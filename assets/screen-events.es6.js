(({ Toolshed: ts }, { Toolshed: settings }) => {
  const defaultOpts = {
    autoListen: true, // Register and deregister the event automatically.
    debounce: settings.eventDebounce,
    passive: true,
  };

  // Initialize the Toolshed global listeners and window objects.
  ts.winOffset = { top: 0, left: 0 };
  ts.winRect = new ts.Geom.Rect(0, 0, window.innerWidth, window.innerHeight);
  ts.events = {};

  /**
   * Create a global event listener for window scroll events.
   *
   * Toolshed JS scroll event does additional calculations to maintain the
   * visibile window space still available to listener items. This helps for
   * layouts, dockers and other screen elements that need to know their
   * workable space taken by other Toolshed JS libraries.
   *
   * @type {EventListener}
   */
  ts.events.scroll = new ts.EventListener(window, 'scroll', defaultOpts);
  ts.events.scroll._run = function _run(e) {
    const rect = new ts.Geom.Rect(ts.winRect);
    const scroll = {
      left: document.documentElement.scrollLeft || document.body.scrollLeft,
      top: document.documentElement.scrollTop || document.body.scrollTop,
    };

    this.listeners.call(e, rect, scroll);
  };

  /**
   * Create a global event listener for window resize events.
   *
   * The global resize event listener is overridden to pass the available
   * window size and position to
   *
   * @type {EventListener}
   */
  ts.events.resize = new ts.EventListener(window, 'resize', defaultOpts);
  ts.events.resize._run = function _run(e, force = false) {
    const right = window.innerWidth || window.clientWidth;
    const bottom = window.innerHeight || window.clientHeight;

    // Only trigger if the size has changed in some way.
    if (ts.winRect.bottom !== bottom
      || ts.winRect.top !== ts.winOffset.top
      || ts.winRect.right !== right
      || ts.winRect.left !== ts.winOffset.left
      || force
    ) {
      ts.winRect.top = ts.winOffset.top;
      ts.winRect.left = ts.winOffset.left;
      ts.winRect.right = right;
      ts.winRect.bottom = bottom;

      const rect = new ts.Geom.Rect(ts.winRect);
      this.listeners.call(e, rect);
    }
  };

  // Defer creating and using this mql until after the document is ready.
  if (settings && settings.breakpoints) {
    ts.events.mediaQueries = new ts.MediaQueryListener(
      settings.breakpoints,
      defaultOpts,
    );

    ts.events.mediaQueries.listen();
  }

  /**
   * When the DOM is ready, start listening for the MediaQuery events
   * and keep track of the offset created by the admin too bar.
   */
  document.addEventListener('drupalViewportOffsetChange.toolbar', () => {
    const toolbar = document.getElementById('toolbar-bar');

    if (toolbar) {
      const tooltray = toolbar.querySelector('.toolbar-tray.is-active');

      ts.winOffset.top = document.body.classList.contains('toolbar-fixed') ? toolbar.clientHeight : 0;

      if (tooltray) {
        if (tooltray.classList.contains('toolbar-tray-horizontal')) ts.winOffset.top += tooltray.clientHeight;
        else if (tooltray.classList.contains('toolbar-tray-vertical')) ts.winOffset.left = tooltray.clientWidth;
      }
    }

    ts.events.resize.trigger();
  });

  // Run the initial resize callback if the toolbar has already loaded.
  const toolbar = document.getElementById('toolbar-bar');
  if (toolbar && toolbar.clientHeight) {
    document.dispatchEvent(new Event('drupalViewportOffsetChange.toolbar'));
  }
})(Drupal, drupalSettings);
