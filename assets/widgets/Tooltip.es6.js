(({ behaviors, Toolshed: ts }) => {
  /**
   * Show the tooltip content.
   *
   * @param {DOMElement} tip Tooltip content to show.
   */
  function showTooltip(tip) {
    tip.style.display = '';
  };

  /**
   * The hide the connected tooltip content.
   *
   * @param {DOMElement} tip Tooltip content to hide.
   */
  function hideTooltip(tip) {
    tip.style.display = 'none';
  };

  /**
   * Toggle the current tooltip display status.
   *
   * @param {DOMElement} tip Tooltip content to show or hide.
   */
  function toggleTooltip(tip) {
    tip.style.display = tip.style.display !== 'none' ? 'none' : '';
  }

  /**
   * Detect if hover functionality is available on the browser and switch
   * between displaying the tooltip on hover or click.
   */
  const attachTooltip = window.matchMedia('(hover: hover)').matches
    ? function(tool, tip) {
      tool.on('mouseover', (e) => showTooltip(tip));
      tool.on('mouseout', (e) => hideTooltip(tip));
      tool.on('focus', (e) => showTooltip(tip));
      tool.on('blur', (e) => hideTooltip(tip));
    } : function(tool, tip) {
      tool.on('click', (e) => toggleTooltip(tip));
      tool.on('focus', (e) =>showTooltip(tip));
      tool.on('blur', (e) => hideTooltip(tip));
    };

  /**
   * Find and attach all instances of the tooltip.
   */
  behaviors.tsTooltip = {
    attach(context) {
      ts.walkByClass(context, 'ts-tooltip', (el) => {
        const tool = new ts.Element(el);
        const tip = tool.findByClass('ts-tooltip__content').item(0);

        // If tooltip content was found, attach the tooltip behavior to the
        // tooltip trigger element.
        if (tip) attachTooltip(tool, tip);
      }, 'ts-tooltip-process');
    }
  }
})(Drupal);
