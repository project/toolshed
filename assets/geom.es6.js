/* eslint no-bitwise: ["error", { "allow": ["|"] }] */
(({ Toolshed: ts }) => {
  ts.Geom = {};

  /**
   * Defines a enumerations for different edges.
   *
   * @type {Object}
   */
  class Edges {
    static get TOP() {
      return 0x01;
    }

    static get BOTTOM() {
      return 0x02;
    }

    static get LEFT() {
      return 0x10;
    }

    static get RIGHT() {
      return 0x20;
    }
  }

  /**
   * Defines a directions enumerations or mask.
   *
   * @type {Object}
   */
  class Direction {
    static get VERTICAL() {
      return Edges.TOP | Edges.BOTTOM;
    }

    static get HORIZONTAL() {
      return Edges.LEFT | Edges.RIGHT;
    }

    static get ANY() {
      return Direction.HORIZONTAL | Direction.VERTICAL;
    }
  }

  /**
   * Coordinates to decribe a rectangular region in space.
   */
  class Rect {
    /**
     * Create a new Rect instance from the bounding area of a DOM element.
     *
     * @param {Element} el
     *   DOM element to create the Rectangle from.
     *
     * @return {Rect}
     *   A new Rect instance representing the bounding box of the element's
     *   client rectangular space.
     */
    static fromElement(el) {
      const b = el.getBoundingClientRect();
      return new Rect(b.left, b.top, b.right, b.bottom);
    }

    /**
     * @param {Toolshed.Rect|int} left
     *  Either a jQuery wrapper or Rect object to build the rectangle with.
     *  If just an integer, use this value as the left edge of the rectangle.
     * @param {int} top
     *  The coordinate of the top edge of the rectangle.
     * @param {int} right
     *  The coordinate of the right edge of the rectangle.
     * @param {int} bottom
     *  The coordinate of the bottom edge of the rectangle.
     */
    constructor(left, top, right, bottom) {
      if (left instanceof ts.Geom.Rect) {
        Object.assign(this, left);
      }
      else {
        if (top <= bottom) {
          this.top = top;
          this.bottom = bottom;
        }
        else {
          this.top = bottom;
          this.bottom = top;
        }

        if (left <= right) {
          this.left = left;
          this.right = right;
        }
        else {
          this.left = right;
          this.right = left;
        }
      }
    }

    /**
     * Get the a point indicating the top-left corner of the rectangle.
     *
     * @return {object}
     *   Object with a left and right property that present a point in space.
     */
    getPosition() {
      return { left: this.left, top: this.top };
    }

    /**
     * Get the width of the rectangle.
     *
     * @return {int}
     *   The width of the rectangular area.
     */
    getWidth() {
      return this.right - this.left;
    }

    /**
     * Get the height of the rectangle.
     *
     * @return {int}
     *   The height of the rectangular area.
     */
    getHeight() {
      return this.bottom - this.top;
    }

    /**
     * Check if a point in space is inside of the rectangle.
     *
     * @param {Object} pt
     *   An object with a left and top property to represent a point in space.
     *
     * @return {bool}
     *   If the point is inside of this rectangular area.
     */
    isInRect(pt) {
      const pos = this.getPosition();
      pt.left -= pos.left;
      pt.top -= pos.top;

      return (pt.left >= 0 && pt.left <= this.getWidth())
        && (pt.top >= 0 && pt.top <= this.getHeight());
    }

    /**
     * Get the volume / area represented by this rectangle.
     *
     * @return {int}
     *   The area represented by this rectangle.
     */
    getArea() {
      return this.getWidth() * this.getHeight();
    }

    /**
     * Offset the rectangle by a X and Y offset.
     *
     * @param {int} xOffset
     *   Number of units to move in the x axis. Can be negative.
     * @param {int} yOffset
     *   Number of units to move in the y axis. Can be negative.
     */
    offset(xOffset, yOffset) {
      this.top += yOffset;
      this.bottom += yOffset;
      this.left += xOffset;
      this.right += xOffset;
    }

    /**
     * Get a Rect object representing the intersection of the two rectangles.
     *
     * @param {Rect} rect
     *   The rectangle to find and intersection of.
     *
     * @return {Rect|null}
     *   The resulting intersection if there was one, otherwise NULL.
     */
    getIntersection(rect) {
      const o1 = rect.getPosition();
      const o2 = this.getPosition();

      const x = Math.max(o1.left, o2.left);
      const y = Math.max(o1.top, o2.top);
      const r = Math.min(o1.left + rect.getWidth(), o2.left + this.getWidth());
      const b = Math.min(o1.top + rect.getHeight(), o2.top + this.getHeight());

      // Check that this point is in the rectangle.
      return (x > r || y > b) ? null : new Rect(x, y, r, b);
    }

    /**
     * Test if a rectangle is completely inside this rectangle.
     *
     * @param {Rect} rect
     *   Rectangle to test if it was contained inside of.
     *
     * @return {bool}
     *   True IFF the rect parameter represents and area that is completely
     *   inside this Rect instance.
     */
    contains(rect) {
      return rect.left >= this.left && rect.right <= this.right
        && rect.top >= this.top && rect.bottom <= this.bottom;
    }
  }

  // Export the classes to the Toolshed namespace.
  ts.Geom.Edges = Edges;
  ts.Geom.Direction = Direction;
  ts.Geom.Rect = Rect;
})(Drupal);
