"use strict";

(({
  behaviors,
  Toolshed: ts
}) => {
  /**
   * Form submission callback to combine search filters.
   *
   * @param {SubmitEvent} e
   *   Submit event for the form submission event.
   */
  function onSubmit(e) {
    const form = e.target;
    const grp = form.dataset.targetView ? this.groups.get(form.dataset.targetView) : null;
    if (grp && grp.length) {
      const data = new FormData(form);
      grp.forEach(f => {
        // Skip the current form values. Use the base form so these
        // existing values will have priority.
        if (f === form) return;
        const add = new FormData(f);
        for (const [k, v] of add.entries()) {
          if (k && v) data.append(k, v);
        }
      });

      // Submit the form with URL query string.
      const params = new URLSearchParams(data);
      window.location = ts.buildUrl(form.action, params.toString());
      e.preventDefault();
    }
  }
  ;

  /**
   * Setup the Drupal behaviors for connecting Toolshed search filter forms.
   */
  behaviors.tsSearchFilterForm = {
    // Manage forms that are grouped together and combine filter.
    groups: new Map(),
    /**
     * Attach Toolshed filter forms into groups of forms with the same targets.
     */
    attach(context) {
      // Bind the form submit callback to this behavior.
      this.onSubmit = this.onSubmit || onSubmit.bind(this);
      ts.walkBySelector(context, 'form.toolshed-search-filter-form', form => {
        // The target view data if this form is connected to other forms.
        const tv = form.dataset.targetView;
        if (!(tv && 'GET' === form.method.toUpperCase())) return;
        let list = this.groups.get(tv);
        if (!list) {
          this.groups.set(tv, list = []);
        }
        list.push(form);

        // Intercept the form submission to allow the combining of grouped filters.
        form.addEventListener('submit', this.onSubmit);
      }, 'ts-search-form-processed');
    },
    /**
     * Find instances of the grouped filter forms and remove them from groups.
     */
    detach(context) {
      ts.walkByClass(context, 'ts-search-form-processed', form => {
        form.classList.removeClass('ts-search-form-processed');
        const tv = form.dataset.targetView;
        if (tv && this.onSubmit) {
          form.removeEventListener('submit', this.onSubmit);
          const list = this.groups.get(tv);
          const i = list ? list.indexOf(form) : -1;
          if (i > -1) list.splice(i, 1);
        }
      });
    }
  };
})(Drupal);
//# sourceMappingURL=data:application/json;charset=utf8;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoic2VhcmNoLWZpbHRlcnMuanMiLCJuYW1lcyI6WyJiZWhhdmlvcnMiLCJUb29sc2hlZCIsInRzIiwib25TdWJtaXQiLCJlIiwiZm9ybSIsInRhcmdldCIsImdycCIsImRhdGFzZXQiLCJ0YXJnZXRWaWV3IiwiZ3JvdXBzIiwiZ2V0IiwibGVuZ3RoIiwiZGF0YSIsIkZvcm1EYXRhIiwiZm9yRWFjaCIsImYiLCJhZGQiLCJrIiwidiIsImVudHJpZXMiLCJhcHBlbmQiLCJwYXJhbXMiLCJVUkxTZWFyY2hQYXJhbXMiLCJ3aW5kb3ciLCJsb2NhdGlvbiIsImJ1aWxkVXJsIiwiYWN0aW9uIiwidG9TdHJpbmciLCJwcmV2ZW50RGVmYXVsdCIsInRzU2VhcmNoRmlsdGVyRm9ybSIsIk1hcCIsImF0dGFjaCIsImNvbnRleHQiLCJiaW5kIiwid2Fsa0J5U2VsZWN0b3IiLCJ0diIsIm1ldGhvZCIsInRvVXBwZXJDYXNlIiwibGlzdCIsInNldCIsInB1c2giLCJhZGRFdmVudExpc3RlbmVyIiwiZGV0YWNoIiwid2Fsa0J5Q2xhc3MiLCJjbGFzc0xpc3QiLCJyZW1vdmVDbGFzcyIsInJlbW92ZUV2ZW50TGlzdGVuZXIiLCJpIiwiaW5kZXhPZiIsInNwbGljZSIsIkRydXBhbCJdLCJzb3VyY2VzIjpbInNlYXJjaC1maWx0ZXJzLmVzNi5qcyJdLCJzb3VyY2VzQ29udGVudCI6WyIoKHsgYmVoYXZpb3JzLCBUb29sc2hlZDogdHMgfSkgPT4ge1xuICAvKipcbiAgICogRm9ybSBzdWJtaXNzaW9uIGNhbGxiYWNrIHRvIGNvbWJpbmUgc2VhcmNoIGZpbHRlcnMuXG4gICAqXG4gICAqIEBwYXJhbSB7U3VibWl0RXZlbnR9IGVcbiAgICogICBTdWJtaXQgZXZlbnQgZm9yIHRoZSBmb3JtIHN1Ym1pc3Npb24gZXZlbnQuXG4gICAqL1xuICBmdW5jdGlvbiBvblN1Ym1pdChlKSB7XG4gICAgY29uc3QgZm9ybSA9IGUudGFyZ2V0O1xuICAgIGNvbnN0IGdycCA9IGZvcm0uZGF0YXNldC50YXJnZXRWaWV3ID8gdGhpcy5ncm91cHMuZ2V0KGZvcm0uZGF0YXNldC50YXJnZXRWaWV3KSA6IG51bGw7XG5cbiAgICBpZiAoZ3JwICYmIGdycC5sZW5ndGgpIHtcbiAgICAgIGNvbnN0IGRhdGEgPSBuZXcgRm9ybURhdGEoZm9ybSk7XG4gICAgICBncnAuZm9yRWFjaCgoZikgPT4ge1xuICAgICAgICAvLyBTa2lwIHRoZSBjdXJyZW50IGZvcm0gdmFsdWVzLiBVc2UgdGhlIGJhc2UgZm9ybSBzbyB0aGVzZVxuICAgICAgICAvLyBleGlzdGluZyB2YWx1ZXMgd2lsbCBoYXZlIHByaW9yaXR5LlxuICAgICAgICBpZiAoZiA9PT0gZm9ybSkgcmV0dXJuO1xuXG4gICAgICAgIGNvbnN0IGFkZCA9IG5ldyBGb3JtRGF0YShmKTtcbiAgICAgICAgZm9yIChjb25zdCBbaywgdl0gb2YgYWRkLmVudHJpZXMoKSkge1xuICAgICAgICAgIGlmIChrICYmIHYpIGRhdGEuYXBwZW5kKGssIHYpO1xuICAgICAgICB9XG4gICAgICB9KTtcblxuICAgICAgLy8gU3VibWl0IHRoZSBmb3JtIHdpdGggVVJMIHF1ZXJ5IHN0cmluZy5cbiAgICAgIGNvbnN0IHBhcmFtcyA9IG5ldyBVUkxTZWFyY2hQYXJhbXMoZGF0YSk7XG4gICAgICB3aW5kb3cubG9jYXRpb24gPSB0cy5idWlsZFVybChmb3JtLmFjdGlvbiwgcGFyYW1zLnRvU3RyaW5nKCkpO1xuICAgICAgZS5wcmV2ZW50RGVmYXVsdCgpO1xuICAgIH1cbiAgfTtcblxuICAvKipcbiAgICogU2V0dXAgdGhlIERydXBhbCBiZWhhdmlvcnMgZm9yIGNvbm5lY3RpbmcgVG9vbHNoZWQgc2VhcmNoIGZpbHRlciBmb3Jtcy5cbiAgICovXG4gIGJlaGF2aW9ycy50c1NlYXJjaEZpbHRlckZvcm0gPSB7XG4gICAgLy8gTWFuYWdlIGZvcm1zIHRoYXQgYXJlIGdyb3VwZWQgdG9nZXRoZXIgYW5kIGNvbWJpbmUgZmlsdGVyLlxuICAgIGdyb3VwczogbmV3IE1hcCgpLFxuXG4gICAgLyoqXG4gICAgICogQXR0YWNoIFRvb2xzaGVkIGZpbHRlciBmb3JtcyBpbnRvIGdyb3VwcyBvZiBmb3JtcyB3aXRoIHRoZSBzYW1lIHRhcmdldHMuXG4gICAgICovXG4gICAgYXR0YWNoKGNvbnRleHQpIHtcbiAgICAgIC8vIEJpbmQgdGhlIGZvcm0gc3VibWl0IGNhbGxiYWNrIHRvIHRoaXMgYmVoYXZpb3IuXG4gICAgICB0aGlzLm9uU3VibWl0ID0gdGhpcy5vblN1Ym1pdCB8fCBvblN1Ym1pdC5iaW5kKHRoaXMpO1xuXG4gICAgICB0cy53YWxrQnlTZWxlY3Rvcihjb250ZXh0LCAnZm9ybS50b29sc2hlZC1zZWFyY2gtZmlsdGVyLWZvcm0nLCAoZm9ybSkgPT4ge1xuICAgICAgICAvLyBUaGUgdGFyZ2V0IHZpZXcgZGF0YSBpZiB0aGlzIGZvcm0gaXMgY29ubmVjdGVkIHRvIG90aGVyIGZvcm1zLlxuICAgICAgICBjb25zdCB0diA9IGZvcm0uZGF0YXNldC50YXJnZXRWaWV3O1xuXG4gICAgICAgIGlmICghKHR2ICYmICdHRVQnID09PSBmb3JtLm1ldGhvZC50b1VwcGVyQ2FzZSgpKSkgcmV0dXJuO1xuXG4gICAgICAgIGxldCBsaXN0ID0gdGhpcy5ncm91cHMuZ2V0KHR2KTtcbiAgICAgICAgaWYgKCFsaXN0KSB7XG4gICAgICAgICAgdGhpcy5ncm91cHMuc2V0KHR2LCBsaXN0ID0gW10pO1xuICAgICAgICB9XG4gICAgICAgIGxpc3QucHVzaChmb3JtKTtcblxuICAgICAgICAvLyBJbnRlcmNlcHQgdGhlIGZvcm0gc3VibWlzc2lvbiB0byBhbGxvdyB0aGUgY29tYmluaW5nIG9mIGdyb3VwZWQgZmlsdGVycy5cbiAgICAgICAgZm9ybS5hZGRFdmVudExpc3RlbmVyKCdzdWJtaXQnLCB0aGlzLm9uU3VibWl0KTtcbiAgICAgIH0sICd0cy1zZWFyY2gtZm9ybS1wcm9jZXNzZWQnKTtcbiAgICB9LFxuXG4gICAgLyoqXG4gICAgICogRmluZCBpbnN0YW5jZXMgb2YgdGhlIGdyb3VwZWQgZmlsdGVyIGZvcm1zIGFuZCByZW1vdmUgdGhlbSBmcm9tIGdyb3Vwcy5cbiAgICAgKi9cbiAgICBkZXRhY2goY29udGV4dCkge1xuICAgICAgdHMud2Fsa0J5Q2xhc3MoY29udGV4dCwgJ3RzLXNlYXJjaC1mb3JtLXByb2Nlc3NlZCcsIChmb3JtKSA9PiB7XG4gICAgICAgIGZvcm0uY2xhc3NMaXN0LnJlbW92ZUNsYXNzKCd0cy1zZWFyY2gtZm9ybS1wcm9jZXNzZWQnKTtcblxuICAgICAgICBjb25zdCB0diA9IGZvcm0uZGF0YXNldC50YXJnZXRWaWV3O1xuICAgICAgICBpZiAodHYgJiYgdGhpcy5vblN1Ym1pdCkge1xuICAgICAgICAgIGZvcm0ucmVtb3ZlRXZlbnRMaXN0ZW5lcignc3VibWl0JywgdGhpcy5vblN1Ym1pdCk7XG5cbiAgICAgICAgICBjb25zdCBsaXN0ID0gdGhpcy5ncm91cHMuZ2V0KHR2KTtcbiAgICAgICAgICBjb25zdCBpID0gbGlzdCA/IGxpc3QuaW5kZXhPZihmb3JtKSA6IC0xO1xuICAgICAgICAgIGlmIChpID4gLTEpIGxpc3Quc3BsaWNlKGksIDEpO1xuICAgICAgICB9XG4gICAgICB9KTtcbiAgICB9LFxuICB9O1xufSkoRHJ1cGFsKTtcbiJdLCJtYXBwaW5ncyI6Ijs7QUFBQSxDQUFDLENBQUM7RUFBRUEsU0FBUztFQUFFQyxRQUFRLEVBQUVDO0FBQUcsQ0FBQyxLQUFLO0VBQ2hDO0FBQ0Y7QUFDQTtBQUNBO0FBQ0E7QUFDQTtFQUNFLFNBQVNDLFFBQVFBLENBQUNDLENBQUMsRUFBRTtJQUNuQixNQUFNQyxJQUFJLEdBQUdELENBQUMsQ0FBQ0UsTUFBTTtJQUNyQixNQUFNQyxHQUFHLEdBQUdGLElBQUksQ0FBQ0csT0FBTyxDQUFDQyxVQUFVLEdBQUcsSUFBSSxDQUFDQyxNQUFNLENBQUNDLEdBQUcsQ0FBQ04sSUFBSSxDQUFDRyxPQUFPLENBQUNDLFVBQVUsQ0FBQyxHQUFHLElBQUk7SUFFckYsSUFBSUYsR0FBRyxJQUFJQSxHQUFHLENBQUNLLE1BQU0sRUFBRTtNQUNyQixNQUFNQyxJQUFJLEdBQUcsSUFBSUMsUUFBUSxDQUFDVCxJQUFJLENBQUM7TUFDL0JFLEdBQUcsQ0FBQ1EsT0FBTyxDQUFFQyxDQUFDLElBQUs7UUFDakI7UUFDQTtRQUNBLElBQUlBLENBQUMsS0FBS1gsSUFBSSxFQUFFO1FBRWhCLE1BQU1ZLEdBQUcsR0FBRyxJQUFJSCxRQUFRLENBQUNFLENBQUMsQ0FBQztRQUMzQixLQUFLLE1BQU0sQ0FBQ0UsQ0FBQyxFQUFFQyxDQUFDLENBQUMsSUFBSUYsR0FBRyxDQUFDRyxPQUFPLENBQUMsQ0FBQyxFQUFFO1VBQ2xDLElBQUlGLENBQUMsSUFBSUMsQ0FBQyxFQUFFTixJQUFJLENBQUNRLE1BQU0sQ0FBQ0gsQ0FBQyxFQUFFQyxDQUFDLENBQUM7UUFDL0I7TUFDRixDQUFDLENBQUM7O01BRUY7TUFDQSxNQUFNRyxNQUFNLEdBQUcsSUFBSUMsZUFBZSxDQUFDVixJQUFJLENBQUM7TUFDeENXLE1BQU0sQ0FBQ0MsUUFBUSxHQUFHdkIsRUFBRSxDQUFDd0IsUUFBUSxDQUFDckIsSUFBSSxDQUFDc0IsTUFBTSxFQUFFTCxNQUFNLENBQUNNLFFBQVEsQ0FBQyxDQUFDLENBQUM7TUFDN0R4QixDQUFDLENBQUN5QixjQUFjLENBQUMsQ0FBQztJQUNwQjtFQUNGO0VBQUM7O0VBRUQ7QUFDRjtBQUNBO0VBQ0U3QixTQUFTLENBQUM4QixrQkFBa0IsR0FBRztJQUM3QjtJQUNBcEIsTUFBTSxFQUFFLElBQUlxQixHQUFHLENBQUMsQ0FBQztJQUVqQjtBQUNKO0FBQ0E7SUFDSUMsTUFBTUEsQ0FBQ0MsT0FBTyxFQUFFO01BQ2Q7TUFDQSxJQUFJLENBQUM5QixRQUFRLEdBQUcsSUFBSSxDQUFDQSxRQUFRLElBQUlBLFFBQVEsQ0FBQytCLElBQUksQ0FBQyxJQUFJLENBQUM7TUFFcERoQyxFQUFFLENBQUNpQyxjQUFjLENBQUNGLE9BQU8sRUFBRSxrQ0FBa0MsRUFBRzVCLElBQUksSUFBSztRQUN2RTtRQUNBLE1BQU0rQixFQUFFLEdBQUcvQixJQUFJLENBQUNHLE9BQU8sQ0FBQ0MsVUFBVTtRQUVsQyxJQUFJLEVBQUUyQixFQUFFLElBQUksS0FBSyxLQUFLL0IsSUFBSSxDQUFDZ0MsTUFBTSxDQUFDQyxXQUFXLENBQUMsQ0FBQyxDQUFDLEVBQUU7UUFFbEQsSUFBSUMsSUFBSSxHQUFHLElBQUksQ0FBQzdCLE1BQU0sQ0FBQ0MsR0FBRyxDQUFDeUIsRUFBRSxDQUFDO1FBQzlCLElBQUksQ0FBQ0csSUFBSSxFQUFFO1VBQ1QsSUFBSSxDQUFDN0IsTUFBTSxDQUFDOEIsR0FBRyxDQUFDSixFQUFFLEVBQUVHLElBQUksR0FBRyxFQUFFLENBQUM7UUFDaEM7UUFDQUEsSUFBSSxDQUFDRSxJQUFJLENBQUNwQyxJQUFJLENBQUM7O1FBRWY7UUFDQUEsSUFBSSxDQUFDcUMsZ0JBQWdCLENBQUMsUUFBUSxFQUFFLElBQUksQ0FBQ3ZDLFFBQVEsQ0FBQztNQUNoRCxDQUFDLEVBQUUsMEJBQTBCLENBQUM7SUFDaEMsQ0FBQztJQUVEO0FBQ0o7QUFDQTtJQUNJd0MsTUFBTUEsQ0FBQ1YsT0FBTyxFQUFFO01BQ2QvQixFQUFFLENBQUMwQyxXQUFXLENBQUNYLE9BQU8sRUFBRSwwQkFBMEIsRUFBRzVCLElBQUksSUFBSztRQUM1REEsSUFBSSxDQUFDd0MsU0FBUyxDQUFDQyxXQUFXLENBQUMsMEJBQTBCLENBQUM7UUFFdEQsTUFBTVYsRUFBRSxHQUFHL0IsSUFBSSxDQUFDRyxPQUFPLENBQUNDLFVBQVU7UUFDbEMsSUFBSTJCLEVBQUUsSUFBSSxJQUFJLENBQUNqQyxRQUFRLEVBQUU7VUFDdkJFLElBQUksQ0FBQzBDLG1CQUFtQixDQUFDLFFBQVEsRUFBRSxJQUFJLENBQUM1QyxRQUFRLENBQUM7VUFFakQsTUFBTW9DLElBQUksR0FBRyxJQUFJLENBQUM3QixNQUFNLENBQUNDLEdBQUcsQ0FBQ3lCLEVBQUUsQ0FBQztVQUNoQyxNQUFNWSxDQUFDLEdBQUdULElBQUksR0FBR0EsSUFBSSxDQUFDVSxPQUFPLENBQUM1QyxJQUFJLENBQUMsR0FBRyxDQUFDLENBQUM7VUFDeEMsSUFBSTJDLENBQUMsR0FBRyxDQUFDLENBQUMsRUFBRVQsSUFBSSxDQUFDVyxNQUFNLENBQUNGLENBQUMsRUFBRSxDQUFDLENBQUM7UUFDL0I7TUFDRixDQUFDLENBQUM7SUFDSjtFQUNGLENBQUM7QUFDSCxDQUFDLEVBQUVHLE1BQU0sQ0FBQyIsImlnbm9yZUxpc3QiOltdfQ==
