<?php

namespace Drupal\toolshed_search\Plugin;

use Drupal\Component\Plugin\PluginInspectionInterface;
use Drupal\toolshed_search\Plugin\Block\ToolshedSearchBlock;
use Drupal\views\ViewExecutable;

/**
 * The interface for the Toolshed search autocomplete provider plugins.
 */
interface ToolshedSearchAutocompleteInterface extends PluginInspectionInterface {

  /**
   * Is this autocomplete plugin compatible with the autocomplete plugin.
   *
   * @param \Drupal\views\ViewExecutable $view
   *   The view which executes the search.
   *
   * @return bool
   *   TRUE if this autocomplete plugin can be use with the provided view.
   */
  public function isApplicable(ViewExecutable $view): bool;

  /**
   * Applies the search autocomplete to the relevent search form elements.
   *
   * @param array $form
   *   Reference to the complete search form.
   * @param array $filters
   *   Reference to the form filter elements.
   * @param \Drupal\toolshed_search\Plugin\Block\ToolshedSearchBlock $search_block
   *   The search block to apply the autocomplete to.
   */
  public function applyAutocomplete(array &$form, array &$filters, ToolshedSearchBlock $search_block): void;

}
