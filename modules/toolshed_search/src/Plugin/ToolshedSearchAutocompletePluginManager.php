<?php

namespace Drupal\toolshed_search\Plugin;

use Drupal\Core\Cache\CacheBackendInterface;
use Drupal\Core\Extension\ModuleHandlerInterface;
use Drupal\Core\Plugin\DefaultPluginManager;
use Drupal\toolshed_search\Annotation\ToolshedSearchAutocomplete;
use Drupal\toolshed_search\Attribute\ToolshedSearchAutocomplete as ToolshedSearchAutocompleteAttribute;

/**
 * The plugin manager for search autocomplete provider plugins.
 */
class ToolshedSearchAutocompletePluginManager extends DefaultPluginManager {

  /**
   * Creates a new ToolshedSearchAutocompletePluginManager class.
   *
   * @param \Traversable $namespaces
   *   Namespaces to search for autocomplete provider plugins.
   * @param \Drupal\Core\Cache\CacheBackendInterface $cache_backend
   *   The cache backend to store plugin definitions.
   * @param \Drupal\Core\Extension\ModuleHandlerInterface $module_handler
   *   The module handler.
   */
  public function __construct(\Traversable $namespaces, CacheBackendInterface $cache_backend, ModuleHandlerInterface $module_handler) {
    parent::__construct(
      'Plugin/ToolshedSearch/Autocomplete',
      $namespaces,
      $module_handler,
      ToolshedSearchAutocompleteInterface::class,
      ToolshedSearchAutocompleteAttribute::class,
      ToolshedSearchAutocomplete::class
    );

    $this->alterInfo('toolshed_search_autocomplete');
    $this->setCacheBackend($cache_backend, 'toolshed_search:autocomplete');
  }

}
