<?php

namespace Drupal\toolshed_search\Annotation;

use Drupal\Component\Annotation\Plugin;

/**
 * Defines a search autocomplete provider plugin for toolshed search blocks.
 *
 * Plugin Namespace: Plugin\ToolshedSearch\Autocomplete.
 *
 * @see \Drupal\Component\Annotation\Plugin
 * @see \Drupal\toolshed_search\Plugin\ToolshedSearchAutocompleteInterface
 * @see \Drupal\toolshed_search\Plugin\ToolshedSearchAutocompletePluginManager
 *
 * @Annotation
 */
class ToolshedSearchAutocomplete extends Plugin {

  /**
   * The machine name for the plugin.
   *
   * @var string
   */
  public $id;

  /**
   * Label for the autocomplete plugin.
   *
   * @var \Drupal\Core\StringTranslation\TranslatableMarkup|string
   */
  public $label;

}
