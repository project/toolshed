<?php

namespace Drupal\toolshed_search\Attribute;

use Drupal\Component\Plugin\Attribute\Plugin;

/**
 * Toolshed autocomplete plugin.
 *
 * Attribute for providing the plugin definitions for autocomplete plugins. The
 * autocomplete plugins provide autocomplete functionality for toolshed search
 * forms.
 */
#[\Attribute(\Attribute::TARGET_CLASS)]
class ToolshedSearchAutocomplete extends Plugin {

  /**
   * The machine name for the plugin.
   *
   * @param string $id
   *   The plugin unique plugin identifier.
   * @param \Stringable|string $label
   *   The friendly human label for the plugin.
   */
  public function __construct(
    public readonly string $id,
    public readonly \Stringable|string $label,
  ) {}

}
