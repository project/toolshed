<?php

/**
 * @file
 * Toolshed search Hooks and callback for views.
 */

use Drupal\Core\Entity\TypedData\EntityDataDefinitionInterface;
use Drupal\Core\Field\Plugin\Field\FieldType\EntityReferenceItem;
use Drupal\Core\TypedData\DataReferenceDefinitionInterface;

/**
 * Implements hook_views_data_alter().
 */
function toolshed_search_views_data_alter(&$data): void {
  $indexStorage = \Drupal::entityTypeManager()->getStorage('search_api_index');

  /** @var \Drupal\search_api\IndexInterface $index */
  foreach ($indexStorage->loadMultiple() as $index) {
    $entity_types = $index->getEntityTypes();
    if (!$entity_types) {
      continue;
    }

    $table = &$data['search_api_index_' . $index->id()];
    foreach ($index->getFields() as $name => $field) {
      if (empty($table[$name])) {
        continue;
      }

      $dataDef = $field->getDataDefinition();
      if ('entity_bundle' === $field->getPropertyPath() && NULL === $field->getDatasourceId()) {
        $table[$name]['filter']['id'] = 'toolshed_entity_bundle_filter';
      }
      elseif (is_a($dataDef->getClass(), EntityReferenceItem::class, TRUE)) {
        /** @var \Drupal\Core\Field\TypedData\FieldItemDataDefinitionInterface $dataDef */
        $propertyDef = $dataDef->getPropertyDefinition('entity');

        if ($propertyDef instanceof DataReferenceDefinitionInterface) {
          $entityDataDef = $propertyDef->getTargetDefinition();
          $entityTypeId = $entityDataDef instanceof EntityDataDefinitionInterface ? $entityDataDef->getEntityTypeId() : NULL;

          // @todo Taoxnomy terms are skipped for now, since they already have
          // reasonable handling from the taxonomy module and Search API.
          // Consider replacing when hierarchy and dropdown select supported.
          if ($entityTypeId && 'taxonomy_term' !== $entityTypeId && in_array($entityTypeId, $entity_types)) {
            $table[$name]['filter']['id'] = 'toolshed_entity_selection';
          }
        }
      }
    }

    // Add a special handler for filtering including or excluding entity types.
    // The search_api_datasource is always available and always set to this key.
    $table['toolshed_include_type_filter'] = [
      'title' => t('Toggle datasource'),
      'help' => t('Creates an exposed checkbox to toggle a datasource.'),
      'filter' => [
        'id' => 'toolshed_datasource_filter',
        'real field' => 'search_api_datasource',
      ],
    ];
  }
}
