(({ behaviors, Toolshed: ts }) => {
  /**
   * Form submission callback to combine search filters.
   *
   * @param {SubmitEvent} e
   *   Submit event for the form submission event.
   */
  function onSubmit(e) {
    const form = e.target;
    const grp = form.dataset.targetView ? this.groups.get(form.dataset.targetView) : null;

    if (grp && grp.length) {
      const data = new FormData(form);
      grp.forEach((f) => {
        // Skip the current form values. Use the base form so these
        // existing values will have priority.
        if (f === form) return;

        const add = new FormData(f);
        for (const [k, v] of add.entries()) {
          if (k && v) data.append(k, v);
        }
      });

      // Submit the form with URL query string.
      const params = new URLSearchParams(data);
      window.location = ts.buildUrl(form.action, params.toString());
      e.preventDefault();
    }
  };

  /**
   * Setup the Drupal behaviors for connecting Toolshed search filter forms.
   */
  behaviors.tsSearchFilterForm = {
    // Manage forms that are grouped together and combine filter.
    groups: new Map(),

    /**
     * Attach Toolshed filter forms into groups of forms with the same targets.
     */
    attach(context) {
      // Bind the form submit callback to this behavior.
      this.onSubmit = this.onSubmit || onSubmit.bind(this);

      ts.walkBySelector(context, 'form.toolshed-search-filter-form', (form) => {
        // The target view data if this form is connected to other forms.
        const tv = form.dataset.targetView;

        if (!(tv && 'GET' === form.method.toUpperCase())) return;

        let list = this.groups.get(tv);
        if (!list) {
          this.groups.set(tv, list = []);
        }
        list.push(form);

        // Intercept the form submission to allow the combining of grouped filters.
        form.addEventListener('submit', this.onSubmit);
      }, 'ts-search-form-processed');
    },

    /**
     * Find instances of the grouped filter forms and remove them from groups.
     */
    detach(context) {
      ts.walkByClass(context, 'ts-search-form-processed', (form) => {
        form.classList.removeClass('ts-search-form-processed');

        const tv = form.dataset.targetView;
        if (tv && this.onSubmit) {
          form.removeEventListener('submit', this.onSubmit);

          const list = this.groups.get(tv);
          const i = list ? list.indexOf(form) : -1;
          if (i > -1) list.splice(i, 1);
        }
      });
    },
  };
})(Drupal);
