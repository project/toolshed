<?php

namespace Drupal\toolshed_media\Plugin\Toolshed\ThirdPartyConfig;

use Drupal\Core\Config\ConfigFactoryInterface;
use Drupal\Core\Config\Entity\ConfigEntityInterface;
use Drupal\Core\Config\ImmutableConfig;
use Drupal\Core\Extension\ModuleHandlerInterface;
use Drupal\Core\Form\FormStateInterface;
use Drupal\Core\Plugin\ContainerFactoryPluginInterface;
use Drupal\Core\StringTranslation\TranslatableMarkup;
use Drupal\Core\Url;
use Drupal\toolshed\Attribute\ToolshedThirdPartyConfig;
use Drupal\toolshed\Plugin\ThirdPartyConfigBase;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * Add toolshed media third party settings to the media_type edit form.
 */
#[ToolshedThirdPartyConfig(
  id: 'toolshed_media_behaviors',
  label: new TranslatableMarkup('Page display behaviors'),
  entity_types: [
    'media_type',
  ],
)]
class MediaThirdPartyConfig extends ThirdPartyConfigBase implements ContainerFactoryPluginInterface {

  /**
   * The current media settings.
   *
   * @var \Drupal\Core\Config\ImmutableConfig
   */
  protected ImmutableConfig $mediaSettings;

  /**
   * Indicates if the media_entity_download module is available.
   *
   * @var bool
   */
  protected bool $hasMediaDownload;

  /**
   * Create a new instance of the MediaThirdPartyConfig plugin.
   *
   * @param array $configuration
   *   The plugin configuration.
   * @param string $plugin_id
   *   The plugin ID.
   * @param mixed $plugin_definition
   *   The plugin definition.
   * @param \Drupal\Core\Config\ConfigFactoryInterface $config_factory
   *   The config factory.
   * @param \Drupal\Core\Extension\ModuleHandlerInterface $module_handler
   *   The module handler service.
   */
  public function __construct(array $configuration, $plugin_id, $plugin_definition, ConfigFactoryInterface $config_factory, ModuleHandlerInterface $module_handler) {
    parent::__construct($configuration, $plugin_id, $plugin_definition);

    $this->mediaSettings = $config_factory->get('media.settings');
    $this->hasMediaDownload = $module_handler->moduleExists('media_entity_download');
  }

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container, array $configuration, $plugin_id, $plugin_definition) {
    return new static(
      $configuration,
      $plugin_id,
      $plugin_definition,
      $container->get('config.factory'),
      $container->get('module_handler')
    );
  }

  /**
   * {@inheritdoc}
   */
  public function getDefaultSettings(): array {
    return [
      'redirect_to_file' => FALSE,
      'use_download' => TRUE,
    ];
  }

  /**
   * {@inheritdoc}
   */
  protected function formElements(ConfigEntityInterface $entity, array $settings, FormStateInterface $form_state): array {
    $elements = [];
    $elements['redirect_to_file'] = [
      '#type' => 'checkbox',
      '#title' => $this->t('Redirect non-admins to file'),
      '#default_value' => $settings['redirect_to_file'],
    ];

    // If media does not have standalone URL settings enabled, disable
    // the checkbox and add some help text point out that this option relies
    // on this media configuration option.
    if (!$this->mediaSettings->get('standalone_url')) {
      $elements['redirect_to_file']['#disabled'] = TRUE;
      $elements['redirect_to_file']['#description'] = $this->t('Media standalone URLs are required for file redirects to occur. Enable this functionality <a href="@url">here</a>.', [
        '@url' => Url::fromRoute('media.settings')->toString(),
      ]);
    }

    if ($this->hasMediaDownload) {
      $provider = $this->getProvider();
      $elements['use_download'] = [
        '#type' => 'checkbox',
        '#title' => $this->t('Use media download link when redirecting non-admins'),
        '#default_value' => $settings['use_download'],
      ];
      $elements['use_download']['#states']['visible'] = [
        ":input[name='{$provider}[redirect_to_file]']" => ['checked' => TRUE],
      ];
    }
    else {
      $elements['#description'] = $this->t('Consider using the <a href="@module_url" target="_blank">Media Entity Download</a> helps to make download links more consistent and applies better HTTP headers to the download routes.', [
        '@module_url' => 'https://www.drupal.org/project/media_entity_download',
      ]);
    }

    return $elements;
  }

}
