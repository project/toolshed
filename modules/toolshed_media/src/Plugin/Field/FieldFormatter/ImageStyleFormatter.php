<?php

namespace Drupal\toolshed_media\Plugin\Field\FieldFormatter;

use Drupal\Core\Config\ConfigFactoryInterface;
use Drupal\Core\Entity\EntityTypeManagerInterface;
use Drupal\Core\Field\Attribute\FieldFormatter;
use Drupal\Core\Field\FieldItemListInterface;
use Drupal\Core\File\FileUrlGeneratorInterface;
use Drupal\Core\Plugin\ContainerFactoryPluginInterface;
use Drupal\Core\StringTranslation\TranslatableMarkup;
use Drupal\image\Plugin\Field\FieldFormatter\ImageFormatterBase;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * A Field formatter for displaying image fields with more styling options.
 *
 * @FieldFormatter(
 *   id = "toolshed_image_styler",
 *   label = @Translation("Toolshed: image styler"),
 *   field_types = {
 *     "image",
 *   }
 * );
 */
#[FieldFormatter(
  id: 'toolshed_image_styler',
  label: new TranslatableMarkup('Toolshed: image styler'),
  field_types: [
    'image',
  ],
)]
class ImageStyleFormatter extends ImageFormatterBase implements ContainerFactoryPluginInterface {

  use ImageStyleTrait;

  /**
   * The file URL generator service.
   *
   * @var \Drupal\Core\File\FileUrlGeneratorInterface
   */
  protected FileUrlGeneratorInterface $fileUrlGenerator;

  /**
   * Media image style formatter object.
   *
   * @param array $configuration
   *   Array of plugin configuration options which includes the field formatter
   *   definition information:
   *     - $field_definition
   *     - $settings
   *     - $label
   *     - $view_mode
   *     - $third_party_settings
   *   These are the usual values for constructing a standard field formatter.
   * @param string $plugin_id
   *   The plugin_id for the formatter.
   * @param mixed $plugin_definition
   *   The plugin implementation definition.
   * @param \Drupal\Core\Config\ConfigFactoryInterface $config_factory
   *   Service for retrieving and maintain Drupal system configurations.
   * @param \Drupal\Core\Entity\EntityTypeManagerInterface $entity_type_manager
   *   Entity manager service, for getting entity definitions and handlers.
   * @param \Drupal\Core\File\FileUrlGeneratorInterface $file_url_generator
   *   The file URL generator service.
   */
  public function __construct(array $configuration, $plugin_id, $plugin_definition, ConfigFactoryInterface $config_factory, EntityTypeManagerInterface $entity_type_manager, FileUrlGeneratorInterface $file_url_generator) {
    parent::__construct(
      $plugin_id,
      $plugin_definition,
      $configuration['field_definition'],
      $configuration['settings'],
      $configuration['label'],
      $configuration['view_mode'],
      $configuration['third_party_settings']
    );

    $this->configFactory = $config_factory;
    $this->entityTypeManager = $entity_type_manager;
    $this->fileUrlGenerator = $file_url_generator;
  }

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container, array $configuration, $plugin_id, $plugin_definition): static {
    return new static(
      $configuration,
      $plugin_id,
      $plugin_definition,
      $container->get('config.factory'),
      $container->get('entity_type.manager'),
      $container->get('file_url_generator')
    );
  }

  /**
   * {@inheritdoc}
   *
   * @param \Drupal\Core\Field\EntityReferenceFieldItemListInterface $items
   *   The field entity reference items.
   * @param string $langcode
   *   The language that should be used to render the field.
   */
  public function viewElements(FieldItemListInterface $items, $langcode): array {
    $elements = [];
    $display = $this->getDisplayRenderable($items);
    $linkToImg = 'file' === $this->getSetting('link_to');

    /** @var \Drupal\file\FileInterface $file */
    foreach ($this->getEntitiesToView($items, $langcode) as $delta => $file) {
      $elements[$delta] = $display;
      // @phpstan-ignore-next-line property.notFound
      $elements[$delta]['#item'] = $file->_referringItem;

      if ($linkToImg) {
        $elements[$delta]['#url'] = $this->fileUrlGenerator->generate($file->getFileUri());
      }
    }

    return $elements;
  }

  /**
   * Get the current options for linking the image to.
   *
   * @return array<string, string|\Stringable>
   *   An array of 'link to' options with the labels as the value.
   */
  public function getLinkOptions(): array {
    return [
      'entity' => $this->t('Content entity'),
      'file' => $this->t('Image file'),
    ];
  }

}
