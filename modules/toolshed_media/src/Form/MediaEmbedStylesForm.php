<?php

namespace Drupal\toolshed_media\Form;

use Drupal\Core\Config\ConfigFactoryInterface;
use Drupal\Core\Config\Entity\ConfigEntityStorageInterface;
use Drupal\Core\Config\TypedConfigManagerInterface;
use Drupal\Core\Entity\EntityTypeManagerInterface;
use Drupal\Core\Form\ConfigFormBase;
use Drupal\Core\Form\FormStateInterface;
use Drupal\image\ImageStyleStorageInterface;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * Manage the image styles available for embedded media.
 *
 * @todo add configurations for responsive image styles.
 */
class MediaEmbedStylesForm extends ConfigFormBase {

  /**
   * Entity storage handler for image styles.
   *
   * @var \Drupal\image\ImageStyleStorageInterface
   */
  protected ImageStyleStorageInterface $imageStyleStorage;

  /**
   * Responsive image style storage handler if responsive image is installed.
   *
   * @var \Drupal\Core\Config\Entity\ConfigEntityStorageInterface|null
   */
  protected ?ConfigEntityStorageInterface $responsiveStyleStorage;

  /**
   * Create a new instance of the MediaEmbedStylesForm class.
   *
   * @param \Drupal\Core\Config\ConfigFactoryInterface $config_factory
   *   The config factory.
   * @param \Drupal\Core\Config\TypedConfigManagerInterface $typed_config_manager
   * @param \Drupal\Core\Entity\EntityTypeManagerInterface $entity_type_manager
   *   The entity type manager.
   */
  public function __construct(ConfigFactoryInterface $config_factory, TypedConfigManagerInterface $typed_config_manager, EntityTypeManagerInterface $entity_type_manager) {
    parent::__construct($config_factory, $typed_config_manager);

    $this->imageStyleStorage = $entity_type_manager->getStorage('image_style');
    $this->responsiveStyleStorage = $entity_type_manager->hasDefinition('responsive_image_style')
      ? $entity_type_manager->getStorage('responsive_image_style')
      : NULL;
  }

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container): static {
    return new static(
      $container->get('config.factory'),
      $container->get('config.typed'),
      $container->get('entity_type.manager')
    );
  }

  /**
   * {@inheritdoc}
   */
  public function getFormId(): string {
    return 'toolshed_media_embed_styles_settings';
  }

  /**
   * {@inheritdoc}
   */
  protected function getEditableConfigNames(): array {
    return ['toolshed.media.embed_styles'];
  }

  /**
   * {@inheritdoc}
   */
  public function buildForm(array $form, FormStateInterface $form_state): array {
    $defaults = $this->config('toolshed.media.embed_styles');

    $form['filter_method'] = [
      '#type' => 'radios',
      '#title' => $this->t('Styles filtering'),
      '#required' => TRUE,
      '#options' => [
        'only_listed' => $this->t('Include only the selected styles'),
        'exclude_listed' => $this->t('Exclude the selected styles'),
      ],
      '#default_value' => $defaults->get('filter_method') ?: 'only_listed',
    ];

    $styleOpts = ['none' => $this->t('Original')];
    foreach ($this->imageStyleStorage->loadMultiple() as $style) {
      $styleOpts[$style->id()] = $style->label();
    }

    $form['styles'] = [
      '#type' => 'checkboxes',
      '#title' => $this->t('Image styles available'),
      '#options' => $styleOpts,
      '#default_value' => $defaults->get('styles') ?: [],
    ];

    if ($this->responsiveStyleStorage) {
      $responsiveOpts = [];
      /** @var \Drupal\responsive_image\ResponsiveImageStyleInterface $responsiveStyle */
      foreach ($this->responsiveStyleStorage->loadMultiple() as $responsiveId => $responsiveStyle) {
        if ($responsiveStyle->hasImageStyleMappings()) {
          $responsiveOpts[$responsiveId] = $responsiveStyle->label();
        }
      }

      if ($responsiveOpts) {
        $form['responsiveStyles'] = [
          '#type' => 'checkboxes',
          '#title' => $this->t('Responsive image styles'),
          '#options' => $responsiveOpts,
          '#default_value' => $defaults->get('responsiveStyles') ?: [],
        ];
      }
    }

    $form['actions'] = [
      '#type' => 'actions',

      'submit' => [
        '#type' => 'submit',
        '#value' => $this->t('Save Settings'),
      ],
    ];

    return $form;
  }

  /**
   * {@inheritdoc}
   */
  public function validateForm(array &$form, FormStateInterface $form_state): void {
  }

  /**
   * {@inheritdoc}
   */
  public function submitForm(array &$form, FormStateInterface $form_state): void {
    $values = $form_state->getValues();
    $this->config('toolshed.media.embed_styles')->setData([
      'filter_method' => $values['filter_method'],
      'styles' => array_filter($values['styles']),
      'responsiveStyles' => array_filter($values['responsiveStyles'] ?? []),
    ])->save();

    $this->messenger()->addStatus($this->t('The configuration options have been saved.'));
  }

}
