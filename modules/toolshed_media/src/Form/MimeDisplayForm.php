<?php

namespace Drupal\toolshed_media\Form;

use Drupal\Core\Form\ConfigFormBase;
use Drupal\Core\Form\FormStateInterface;

/**
 * Create configuration form for mapping mime types to friendly display names.
 */
class MimeDisplayForm extends ConfigFormBase {

  /**
   * {@inheritdoc}
   */
  protected function getEditableConfigNames(): array {
    return ['toolshed.media.mime_display'];
  }

  /**
   * {@inheritdoc}
   */
  public function getFormId(): string {
    return 'toolshed_media_file_display_label_form';
  }

  /**
   * {@inheritdoc}
   */
  public function buildForm(array $form, FormStateInterface $form_state): array {
    $form = parent::buildForm($form, $form_state);
    $mimeMap = $this->config('toolshed.media.mime_display')->get('mime_map');

    $form['mime_map'] = [
      '#type' => 'fieldset',
      '#title' => $this->t('File Extension & Display Names'),
      '#tree' => TRUE,
    ];

    foreach ($mimeMap as $info) {
      $form['mime_map'][] = [
        '#type' => 'container',
        '#attributes' => [
          'class' => [
            'form-wrapper',
            'form--inline',
            'clearfix',
          ],
        ],

        'name' => [
          '#type' => 'textfield',
          '#title' => $this->t('Display name'),
          '#default_value' => $info['name'],
          '#size' => 16,
        ],
        'extensions' => [
          '#type' => 'textfield',
          '#title' => $this->t('Extensions'),
          '#placeholder' => $this->t('File extensions'),
          '#default_value' => implode(' ', $info['extensions']),
        ],
      ];
    }

    return $form;
  }

  /**
   * {@inheritdoc}
   */
  public function validateForm(array &$form, FormStateInterface $form_state): void {
  }

  /**
   * {@inheritdoc}
   */
  public function submitForm(array &$form, FormStateInterface $form_state): void {
    $mimeData = $form_state->getValue('mime_map');

    $data = [];
    foreach ($mimeData as $values) {
      $data[] = [
        'name' => $values['name'],
        'extensions' => preg_split('#\s+#', $values['extensions'], 0, PREG_SPLIT_NO_EMPTY),
      ];
    }

    $this
      ->config('toolshed.media.mime_display')
      ->setData(['mime_map' => $data])
      ->save();

    $this->messenger()->addStatus($this->t('The configuration options have been saved.'));
  }

}
