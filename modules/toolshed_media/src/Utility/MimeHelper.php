<?php

namespace Drupal\toolshed_media\Utility;

use Drupal\Core\Config\ConfigFactoryInterface;

/**
 * Translate file extensions to friendly display names.
 */
class MimeHelper implements MimeHelperInterface {

  /**
   * A map of file extensions to the friendly display name.
   *
   * @var string[]
   */
  protected array $mimeMap = [];

  /**
   * Create a new instance of the Toolshed MimeHelper class.
   *
   * @param \Drupal\Core\Config\ConfigFactoryInterface $config_factory
   *   The plugin factory.
   */
  public function __construct(ConfigFactoryInterface $config_factory) {
    $config = $config_factory
      ->get('toolshed.media.mime_display')
      ->get('mime_map');

    // If mime map isn't empty, populate the mime mapping with data from the
    // configurations.
    if ($config) {
      foreach ($config as $info) {
        if (!empty($info['extensions'])) {
          $this->mimeMap += array_fill_keys($info['extensions'], $info['name']);
        }
      }
    }
  }

  /**
   * {@inheritdoc}
   */
  public function getDisplayName($extension): string|false {
    return $this->mimeMap[$extension] ?? FALSE;
  }

}
