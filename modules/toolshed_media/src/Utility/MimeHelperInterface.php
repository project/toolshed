<?php

namespace Drupal\toolshed_media\Utility;

/**
 * Interface for file mime helper services.
 */
interface MimeHelperInterface {

  /**
   * Get a the friendly display name for a file extension if one is available.
   *
   * @param string $extension
   *   The file extension to match with a friendly display name.
   *
   * @return string|false
   *   If a friendly name is found for the extension, then return the display
   *   name, otherwise return FALSE.
   */
  public function getDisplayName($extension): string|false;

}
